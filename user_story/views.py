from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import UserStory, Usuario
from proyecto.models import Proyecto
from product_backlog.models import ProductBacklog
from .forms import CrearUserStoryForm, EditarUserStoryForm, CancelarUserStoryForm, AsignarUsuarioUserStoryForm, CamposPersonalizadosForm, AsignarFlujoForm
from django.contrib.auth.models import User
from guardian.shortcuts import assign_perm
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.shortcuts import redirect
from tipo_us.models import *
from flujo.models import Flujo, Fase
from product_backlog.models import ProductBacklog
from actividad.models import Actividad
from sprint_backlog.models import SprintBacklog

TO_DO=1
DOING=2
DONE=3

PENDIENTE = 'P'
ACTIVO = 'A'
FINALIZADO = 'F'
CANCELADO = 'C'


def editar_user_story(request, us_id, proyecto_id):
    """
    Recibe una solicitud de la pagina de editar User Story, el codigo
    del User Story que se quiere editar, y el codigo del proyecto al
    cual pertenece el User Story, verifica que el User Story que se
    desea editar esté en estado No Iniciado, si se encuentra en otro
    estado no se puede editar, tambien se verifica que solo el encargado
    del User Story pueda editarlo, luego se carga el template y se renderiza
    la pagina de edicion, una vez editado se guarda en la base de datos los
    cambios realizados sobre el User Story y redirecciona a la pagina del
    proyecto al cual pertenece el User Story

    :param us_id: El codigo del User Story a editar
    :param proyecto_id: El codigo del Proyecto al cual pertenece el User Story
    :return: Dependiendo del caso devuelve la pagina de edicion de User Stories o
    la pagina de visualizacion del User Story
    """
    user = request.user
    usuario = Usuario.objects.get(user=user)
    us = UserStory.objects.get(pk=us_id)
    proy = Proyecto.objects.get(id=proyecto_id)
    if not request.session.is_empty() and proy.scrum == usuario and us.estado_proyecto == 'N':
        if request.method == 'POST':
            form = EditarUserStoryForm(request.POST, instance=us)
            if form.is_valid():
                us_form = form.save(commit=False)
                calculo_prioridad = (us_form.prioridad+2*(us.valor_negocio)+2*(us.valor_tecnico))/5
                us_form.prioridad_final = calculo_prioridad
                us_form.save()
                return redirect('ver_user_story', proyecto_id=proyecto_id, us_id=us.id)
        form_editar_user_story = EditarUserStoryForm(instance=us)
        return render(request, 'user_story/formularios_user_story.html', {'form': form_editar_user_story, 'operacion': 4, 'proyecto_id':proyecto_id, 'us_id':us.id})
    elif request.session.is_empty():
        return redirect('login')
    else:
        return redirect('ver_user_story', proyecto_id=proyecto_id, us_id=us.id)


def ver_user_story(request, us_id, proyecto_id):
    """
    Recibe una solicitud de la pagina para visualizar el User Story, el codigo
    del User Story que se quiere ver, y el codigo del proyecto al cual pertenece
    el User Story, se verifica que se haya iniciado sesion y que el usuario de
    la sesion actual tenga el permiso para visualizar el User Story, luego se
    devuelve el template para visualizar dicho User Story, en caso contrario
    se devuelve a la pagina de inicio de sesion

    :param us_id: El codigo del User Story a visualizar
    :param proyecto_id: El codigo del Proyecto al cual pertenece el User Story
    :return: Dependiendo del caso devuelve la pagina de visualizacion del User Stories o
    la pagina de inicio de sesion
    """
    user = request.user
    usuario = Usuario.objects.get(user=user)
    us = UserStory.objects.get(id=us_id)
    actividades = Actividad.objects.filter(user_story=us)
    atras = request.session['atras_us']
    sprint_id = None
    if atras == 3:
        sprint_id = request.session['atras_us_sprint_id']
    if request.session.is_empty():
        return render(request, 'proyecto/error_inicio_session.html')
    else:
        us = get_object_or_404(UserStory, pk=us_id)
        proy = Proyecto.objects.get(pk=proyecto_id)
        if request.GET.get('cambio_fase'):
            us.fase = Fase.objects.filter(id_flujo=us.flujo.id).get(indice=us.fase.indice-1)
            us.estado_fase = TO_DO
            us.save()

        if request.GET.get('cambio_estado'):
            if us.estado_fase == TO_DO:
                us.estado_fase = DOING
            else:
                us.estado_fase = DONE
                if us.flujo.cant_fases > us.fase.indice:
                    fase_sigte = Fase.objects.filter(id_flujo=us.flujo.id).get(indice=us.fase.indice + 1)
                    us.fase = fase_sigte
                    us.estado_fase = TO_DO
            us.save()
        fase_anterior = None
        if us.fase.indice > 1:
            fase_anterior = Fase.objects.filter(id_flujo=us.flujo.id).get(indice=us.fase.indice-1)
        else:
            fase_anterior = us.fase
        return render(request, 'user_story/ver_user_story.html', {'us': us, 'usuario': usuario, 'proy': proy,
                                                                  'us_id': us_id, 'fase_anterior': fase_anterior,
                                                                  'actividades': actividades, 'atras': atras,
                                                                  'sprint_id': sprint_id})


def crear_user_story(request, proyecto_id):
    """
    Recibe la solicitud de creacion de User Story y el codigo del Proyecto al cual pertenecera el nuevo User Story a
    crear, se verifica que se haya iniciado sesion, que quien este intentando crear un nuevo User Story sea el Scrum
    Master del Proyecto y se vincula el User Story al Product Backlog del Proyecto al cual pertenece, luego se devuelve
    la pagina de visualizacion del nuevo User Story si se creo correctamente, la pagina de inicio de sesion si no esta
    logeado y la pagina de visualizar el proyecto si el usuario no es Scrum Master

    :param proyecto_id: El codigo del Proyecto al cual pertenece el User Story
    :return Dependiendo del caso devuelve la pagina de edicion de User Stories o la pagina de visualizacion del User
    Story
    """
    user = request.user
    usuario = Usuario.objects.get(user=user)
    proy = Proyecto.objects.get(pk=proyecto_id)
    if not request.session.is_empty() and proy.is_scrum(usuario):
        if request.method == 'POST':
            form = CrearUserStoryForm(request.POST, proyecto=proy)
            if form.is_valid():
                us = form.save(commit=False)
                us.estado_proyecto = 'N'
                us.product_backlog = ProductBacklog.objects.get(proyecto_id=proy.id)
                calculo_prioridad = (us.prioridad+2*(us.valor_negocio)+2*(us.valor_tecnico))/5
                us.prioridad_final = calculo_prioridad
                us.trabajo_realizado = 0
                us.save()

                return HttpResponseRedirect(reverse('campos_pers', kwargs={'proyecto_id': proyecto_id, 'us_id':us.id } ))
        form_crear_user_story = CrearUserStoryForm(proyecto=proy)
        return render(request, 'user_story/formularios_user_story.html', {'form': form_crear_user_story, 'operacion': 1, 'proyecto_id':proyecto_id})
    elif request.session.is_empty():
        return redirect('login')
    else:
        return redirect('ver_proyecto', proyecto_id=proyecto_id)


def cancelar_user_story(request, us_id, proyecto_id):
    """
    Recibe una solicitud de cancelacion de User Story, el codigo
    del User Story que se desea cancelar y el codigo del proyecto
    al cual pertenece dicho User Story, corrobora que no este en
    estado finalizado ni cancelado, pide una justificacion para
    la cancelacion y pone en estado cancelado al User Story, luego
    de acuerdo al caso redirecciona a la pantalla de inicio de
    sesion si no se inicio sesion, a la pantalla de visualizacion
    del User Story si algo salio mal y a la pantalla del proyecto
    al cual pertenece si salio bien

    :param us_id: El codigo del User Story a cancelar
    :param proyecto_id: El codigo del Proyecto al cual pertenece el User Story
    :return: Dependiendo del caso devuelve la pagina de visualizacion de User Stories,
    la pagina de inicio de sesion o la pagina de visualizacion del proyecto
    """
    user = request.user
    usuario = Usuario.objects.get(user=user)
    us = UserStory.objects.get(pk=us_id)
    proy = Proyecto.objects.get(pk=proyecto_id)
    if not request.session.is_empty() and proy.scrum == usuario and us.estado_proyecto != 'F' and us.estado_proyecto != 'C':
        if request.method == 'POST':
            form = CancelarUserStoryForm(request.POST, instance=us)
            if form.is_valid():
                us = form.save(commit=False)
                us.estado_proyecto = 'C'
                us.save()
                return redirect('ver_proyecto', proyecto_id=proyecto_id)
        form_cancelar_user_story = CancelarUserStoryForm(instance=us)
        return render(request, 'user_story/formularios_user_story.html', {'form': form_cancelar_user_story, 'operacion': 2, 'proyecto_id': proyecto_id, 'us_id': us.id})
    elif request.session.is_empty():
        return redirect('login')
    else:
        return redirect('ver_user_story', proyecto_id=proyecto_id, us_id=us.id)


def asignar_encargado_user_story(request, us_id, proyecto_id):
    """
    Recibe la solicitud de asignacion de un encargado para el User
    Story, verifica que el usuario quien quiera asignar un nuevo
    encargado sea el Scrum Master y que el User Story no se encuentre
    finalizado ni cancelado, luego redirecciona a la pantalla de asignacion
    de encargado si fue correcto a la pantalla de inicio de sesion si no se
    ha iniciado sesion, a la pantalla de visualizacion del User Story si se
    asigno correctamente o si no se tiene el permiso de realizar esta
    operacion

    :param us_id: El codigo del User Story al cual se quiere asignar un encargado
    :param proyecto_id: El codigo del Proyecto al cual pertenece el User Story
    """
    user = request.user
    usuario = Usuario.objects.get(user=user)
    us = UserStory.objects.get(pk=us_id)
    proy = Proyecto.objects.get(pk=proyecto_id)
    if not request.session.is_empty() and (proy.is_scrum(usuario) or us.usuario_asignado == usuario) and us.estado_proyecto != 'F' and us.estado_proyecto != 'C':
        if request.method == 'POST':
            form = AsignarUsuarioUserStoryForm(request.POST, instance=us, proyecto=proy)
            if form.is_valid():
                us = form.save(commit=False)
                us.save()
                return redirect('ver_user_story', proyecto_id=proyecto_id, us_id=us.id)
        form_cancelar_user_story = AsignarUsuarioUserStoryForm(instance=us, proyecto=proy)
        return render(request, 'user_story/formularios_user_story.html', {'form': form_cancelar_user_story, 'operacion': 3, 'proyecto_id':proyecto_id, 'us_id':us.id})
    elif request.session.is_empty():
        return redirect('login')
    else:
        return redirect('ver_user_story', proyecto_id=proyecto_id, us_id=us.id)


def CamposPersonalizadosView(request,proyecto_id , us_id):
    """
    Vista para cargar el valor de un campo personalizado dentro de un User Story
    """
    us = UserStory.objects.get(pk=us_id)
    tipo = us.tipo
    if request.method == 'POST':

        form = CamposPersonalizadosForm(request.POST, instance = tipo)
        form_flujo = AsignarFlujoForm(request.POST, instance = us, flujos=tipo.flujo_set.all())
        if form.is_valid():
            # lista de tcv del tipo de us actual
            lista_tc = Tipo_Campo.objects.filter(tipo = tipo )

            #query = CampoPersonalizado.objects.filter(tipo_us=tipo_us)
            # print(lista_tc)

            if lista_tc != None:

                for tc in lista_tc:
                    # cargar datos en la tabla campo
                    utcv = US_Tipo_Campo_Valor.objects.create(us = us, tipo_campo= tc)
                    utcv.valor = form.cleaned_data.get(tc.campo.nombre_campo)

                    #guardar datos
                    utcv.save()

            form.save()
            us = form_flujo.save()
            us = UserStory.objects.get(id=us.id)
            fase = Fase.objects.filter(id_flujo=us.flujo.id).get(indice=1)
            us.fase = fase
            us.estado_fase = TO_DO
            us.save()
            return redirect('ver_user_story', us_id=us.id, proyecto_id=proyecto_id)
    else:
        form = CamposPersonalizadosForm(instance = tipo)
        form_flujo = AsignarFlujoForm(instance = us, flujos=tipo.flujo_set.all())
    return render(request, "user_story/editar_campos.html", {'form': form, 'form_flujo': form_flujo, 'proyecto_id':proyecto_id })


def listar_us(request, sprint_id):
    """
    Genera una lista de user stories que están asignados a un proyecto determinado para luego importarlo a un sprint determinado.

    :param sprint_id: id del sprint al cual se importará el user story.
    :return vista con user stories.
    """
    if request.session.is_empty() or request.user.is_superuser:
        return render(request, 'proyecto/error_inicio_session.html', {})
    else:
        sprint = Sprint.objects.get(pk=sprint_id)
        pBacklog = ProductBacklog.objects.get(proyecto_id__exact=sprint.proyecto_id)

        us_iniciados = UserStory.objects.filter(product_backlog=pBacklog).filter(sprint_backlog=None).filter(iniciado=True).filter(estado_proyecto='N').order_by("-prioridad_final")
        us_no_iniciados = UserStory.objects.filter(product_backlog=pBacklog).filter(sprint_backlog=None).filter(
            iniciado=False).filter(estado_proyecto='N').order_by("-prioridad_final")

        return render(request, 'user_story/listar_us.html', {'sprint': sprint, 'us_iniciados': us_iniciados, 'us_no_iniciados': us_no_iniciados })


def listar_usuario_sprint(request, sprint_id):
    """
    Genera dos listas de usuarios que pertenecen a un sprint determinado.
    Una lista con usuarios con US y otro de usuarios sin US.

    :param sprint_id: id del sprint al cual pertenecen los usuarios.
    :return Vista con una lista de usuarios. Desde esta lista se pueden asignar user stories a los usuarios.
    """
    if request.session.is_empty() or request.user.is_superuser:
        return render(request, 'proyecto/error_inicio_session.html', {})
    else:
        sprint = Sprint.objects.get(pk=sprint_id)
        sprint_backlog = SprintBacklog.objects.get(sprint_id__exact=sprint_id)

        uss = UserStory.objects.all().filter(sprint_backlog_id=sprint_backlog.id)

        us_libres = 0
        for us in uss:
            if not us.usuario_asignado:
                us_libres += 1

        usuarios = sprint.miembros.all().exclude(pk=sprint.proyecto.scrum.id)

        usuarios_us_id = []

        usuarios_id = []

        for usuario in usuarios:
            aux = 0
            for us in uss:
                if usuario == us.usuario_asignado:
                    aux += 1

            if aux > 0:
                usuarios_us_id.append(usuario.id)
            else:
                usuarios_id.append(usuario.id)

        usuarios_con_us = Usuario.objects.all().filter(id__in=usuarios_us_id)
        usuarios_sin_us = Usuario.objects.all().filter(id__in=usuarios_id)

        return render(request, 'user_story/agregar_us_usuarios.html', {'sprint': sprint,
                                                                       'usuarios_con_us': usuarios_con_us,
                                                                       'usuarios_sin_us': usuarios_sin_us,
                                                                       'uss': uss,
                                                                       'us_libres': us_libres})


def listar_usuario_us(request, sprint_id, username):
    """
    Genera una lista con US libres para asignarle a un usuario determinado.

    :param sprint_id: ID del sprint al cual pertenece el usuario.
    :param username: Usuario al cual será asignado el US.
    :return Vista con una lista de US libres.
    """
    if request.session.is_empty() or request.user.is_superuser:
        return render(request, 'proyecto/error_inicio_session.html', {})
    else:
        sprint = Sprint.objects.get(pk=sprint_id)
        sprint_backlog = SprintBacklog.objects.get(sprint_id__exact=sprint_id)

        uss = UserStory.objects.all().filter(sprint_backlog_id=sprint_backlog.id)

        libres = []

        for us in uss:
            if not us.usuario_asignado:
                libres.append(us.id)

        us = UserStory.objects.all().filter(id__in=libres)

        return render(request, 'user_story/listar_us_libres.html', {'sprint': sprint,
                                                                    'user_stories': us,
                                                                    'username': username})


def listar_us_sprint(request, sprint_id):
    """
    Genera una lista con User Stories que no estan asignados a un usuario.
    La lista sirve para asignarle un usuario a dichos US.

    :param sprint_id: sprint en el cual están los user stories.
    :return vista con una lista de US disponibles.
    """

    if request.session.is_empty() or request.user.is_superuser:
        return render(request, 'proyecto/error_inicio_session.html', {})
    else:
        sprint = Sprint.objects.get(pk=sprint_id)
        sprint_backlog = SprintBacklog.objects.get(sprint_id__exact=sprint_id)

        uss = UserStory.objects.all().filter(sprint_backlog_id=sprint_backlog.id)

        libres = []

        for us in uss:
            if not us.usuario_asignado:
                libres.append(us.id)

        us = UserStory.objects.all().filter(id__in=libres)

        return render(request, 'user_story/agregar_us_us.html', {'sprint': sprint, 'user_stories': us})


def listar_us_usuario(request, sprint_id, us_id):
    """
    Genera una lista de los usuarios que pertenecen a un sprint para luego poder asignarle un US determinado.

    :param sprint_id: ID del sprint.
    :param us_id: ID del US que será asignado al usuario seleccionado.
    :return Llama a la funcion que se encarga de asignar el US al usuario seleccionado.
    """
    if request.session.is_empty() or request.user.is_superuser:
        return render(request, 'proyecto/error_inicio_session.html', {})
    else:
        sprint = Sprint.objects.get(pk=sprint_id)
        sprint_backlog = SprintBacklog.objects.get(sprint_id__exact=sprint_id)

        uss = UserStory.objects.all().filter(sprint_backlog_id=sprint_backlog.id)

        usuarios = sprint.miembros.all().exclude(pk=sprint.proyecto.scrum.id)

        usuarios_us_id = []

        usuarios_id = []

        for usuario in usuarios:
            aux = 0
            for us in uss:
                if usuario == us.usuario_asignado:
                    aux += 1

            if aux > 0:
                usuarios_us_id.append(usuario.id)
            else:
                usuarios_id.append(usuario.id)

        usuarios_con_us = Usuario.objects.all().filter(id__in=usuarios_us_id)
        usuarios_sin_us = Usuario.objects.all().filter(id__in=usuarios_id)

        return render(request, 'user_story/listar_us_usuarios.html', {'sprint': sprint,
                                                                      'us_id': us_id,
                                                                      'usuarios_con_us': usuarios_con_us,
                                                                      'usuarios_sin_us': usuarios_sin_us,
                                                                      'uss': uss})


def asignar_usuario_us(request, sprint_id, username, us_id):
    """
    Asigna un US a un usuario. Ambos pertenecen a un sprint determinado

    :param sprint_id: ID del sprint al cual pertenecen el US y el usuario.
    :param username: Username del usuario.
    :param us_id: ID del US.
    :return Ventana de asignacion de US. Ya con el US asignado al usuario.
    """
    sprint = Sprint.objects.get(pk=sprint_id)
    sprint_bl = sprint.sprintbacklog_set.all()[0]
    us = UserStory.objects.get(pk=us_id)
    if request.session.is_empty() or request.user.is_superuser:
        return render(request, 'proyecto/error_inicio_session.html', {})
    elif not us.sprint_backlog == sprint_bl:
        return redirect('ver_sprint', sprint_id)
    else:
        usuario = Usuario.objects.get(user__username=username)
        sprint = Sprint.objects.get(pk=sprint_id)
        us = UserStory.objects.get(pk=us_id)

        us.asignar_usuario(usuario)
        usuario.restar_horas_disponibles(us.trabajo_por_realizar)

        return redirect('listar_usuario_sprint', sprint_id)


def desasignar_usuario_us(request, sprint_id, username, us_id, quitar_sprint):
    """
    Desasigna un US a un usuario. Ambos pertenecen a un sprint determinado

    :param sprint_id: ID del sprint al cual pertenecen el US y el usuario.
    :param username: Username del usuario.
    :param us_id: ID del US.
    :param quitar_sprint: Bandera. Indica si se debe quitar el US del sprint backlog o solo desasignar el usuario.
    :return Ventana de asignacion de US. Ya con el US asignado al usuario.
    """

    sprint = Sprint.objects.get(pk=sprint_id)
    sprint_bl = sprint.sprintbacklog_set.all()[0]
    us = UserStory.objects.get(pk=us_id)
    if request.session.is_empty() or request.user.is_superuser:
        return render(request, 'proyecto/error_inicio_session.html', {})
    elif not us.sprint_backlog == sprint_bl:
        return redirect('ver_sprint', sprint_id)
    else:
        usuario = Usuario.objects.get(user__username=username)
        us = UserStory.objects.get(pk=us_id)

        us.desasignar_usuario()

        usuario.add_horas_disponibles(us.trabajo_por_realizar)

        if quitar_sprint == 1:
            return redirect('quitar_us', sprint_id, us_id)
        else:
            return redirect('ver_sprint', sprint_id)

