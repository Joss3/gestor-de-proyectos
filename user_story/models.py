from django.db import models
from usuarios.models import Usuario
from product_backlog.models import ProductBacklog
from sprint_backlog.models import SprintBacklog
from flujo.models import Flujo, Fase
from tipo_us.models import Tipo_US
from django.core.validators import MaxValueValidator, MinValueValidator


class UserStory(models.Model):

    ESTADO_SPRINT = (
        ('N', 'No Iniciado'),
        ('P', 'En Proceso'),
        ('F', 'Finalizado'),
        ('C', 'Cancelado'),
    )
    """Estados en los que se encuentra un sprint"""

    ESTADO_FASE = (
        ('1', 'TO DO'),
        ('2', 'DOING'),
        ('3', 'DONE')
    )
    """Estados en los que se encuentra una fase"""

    nombre = models.CharField(max_length=500)
    """Nombre del User Story"""
    descripcion = models.CharField(max_length=500)
    """Descripcion del User Story"""
    criterios_aceptacion = models.CharField(max_length=2000)
    """Los requisitos necesarios para que un User Story se pueda dar como finalizado"""
    trabajo_por_realizar = models.IntegerField(validators=[MinValueValidator(1)], help_text="Horas Hombre necesarias para cerrar el User Story")
    """La cantidad de horas total estimada que debe trabajar sobre un User Story"""
    trabajo_realizado = models.IntegerField(null=True, blank=True)
    """La cantidad de horas total que se trabajó sobre un User Story"""
    flujo = models.ForeignKey(Flujo, on_delete=models.CASCADE, null=True)
    """El flujo al cual pertenece un User Story"""
    tipo = models.ForeignKey(Tipo_US, on_delete=models.CASCADE, null=True)
    """El tipo del User Story"""
    prioridad = models.IntegerField(null=True, blank=True, validators=[MaxValueValidator(10), MinValueValidator(1)], help_text="Del 1 (minima) al 10 (maxima)")
    """Es la prioridad final que se calcula con el valor tecnico y el valor de negocio de un User Story"""
    prioridad_final = models.IntegerField(null=True, blank=True)
    """Prioridad que se le asigna al US dentro del product backlog"""
    valor_negocio = models.IntegerField(validators=[MaxValueValidator(10), MinValueValidator(1)], help_text="Del 1 (minimo) al 10 (maximo)")
    """El valor que tiene el User Story para la empresa asignado por el Scrum Master"""
    valor_tecnico = models.IntegerField(validators=[MaxValueValidator(10), MinValueValidator(1)], help_text="Del 1 (minimo) al 10 (maximo)")
    """El valor que tiene el User Story para el desarrollador"""
    estado_proyecto = models.CharField(max_length=1, choices=ESTADO_SPRINT, default='N', null=True, blank=True)
    """El estado del User Story en el Proyecto"""
    usuario_asignado = models.ForeignKey(Usuario, on_delete=models.SET_NULL, null=True, blank=True)
    """El desarrollador al cual fue asignado el User Story"""
    fase = models.ForeignKey(Fase, on_delete=models.SET_NULL, null=True, blank=True)
    """La fase dentro del flujo en la cual se encuentra el User Story"""
    estado_fase = models.IntegerField(choices=ESTADO_FASE, null=True, blank=True)
    """El estado del User Story dentro de una fase"""
    justificacion = models.CharField(max_length=5000, null=True, blank=True)
    """En caso de cancelar un User Story se debe agregar una justificacion, en caso contrario se mantiene en nulo"""
    product_backlog = models.ForeignKey(ProductBacklog, on_delete=models.CASCADE, null=True)
    """El Product Backlog del Proyecto al cual pertenece este User Story"""
    sprint_backlog = models.ForeignKey(SprintBacklog, on_delete=models.CASCADE, null=True, blank=True, default=None)
    """El Sprint Backlog del Sprint al cual se asigna este User Story"""
    iniciado = models.BooleanField(default=False)
    """Bandera que indica si el US ya se inició en algun momento"""
    fin = models.BooleanField(default=False)
    """Bandera que indica si el US ya está terminado"""
    actividad_agregada = models.BooleanField(default=False)
    """Bandera que indica si ya se ha agregado una actividad al US después de haber ingresado a una fase"""

    class Meta:

        permissions = (
            ("encagado_us", "El encargado del User Story"),
        )
        """
        Se agregan permisos nuevos relacionados con los encargados de los User Stories
        """

    def __str__(self):
        return f' {self.nombre}'

    def set_sprint_backlog(self, backlog):
        """
        :type backlog: SprintBacklog
        :param backlog: Objeto sprint backlog, al cual será asignado el US
        Establece el sprint backlog de US
        """
        self.sprint_backlog = backlog
        self.save()
        return

    def set_product_backlog(self, backlog):
        """
        :type backlog: ProductBacklog
        :param backlog: Objeto product backlog, al cual será asignado el US
        Establece el product backlog del US
        """
        self.product_backlog = backlog
        self.save()
        return

    def asignar_usuario(self, usuario):
        """
        :type usuario: Usuario
        :param usuario: Objeto usuario
        Asigna un usuario al US
        """
        self.usuario_asignado = usuario
        self.save()
        return

    def desasignar_usuario(self):
        """
        Desasigna el usuario asignado al US
        """
        self.usuario_asignado = None
        self.save()
        return

    def reset_sprint_backlog(self):
        """Desasigna el US del sprint backlog"""
        self.sprint_backlog = None
        self.save()
        return

    def set_prioridad_final(self, prioridad):
        """
        :type prioridad: int
        :param prioridad: Prioridad final que se le asigna al US
        Establece la prioridad final del US
        """
        self.prioridad_final = prioridad
        self.save()
        return

    def reset_trabajo_realizado(self):
        """Establece el trabajo realizado de un US a cero"""
        self.trabajo_realizado = 0
        self.save()
        return

    def set_flujo(self, flujo):
        """
        :type flujo: Flujo
        :param flujo: flujo al cual pertenecerá el US
        Asigna el US a un flujo
        """
        self.flujo = flujo
        self.save()
        return

    def set_fase(self, fase):
        """
        :type fase: Fase
        :param fase: fase que se le asigna al US
        Devuelve un US a la primera fase del flujo
        """
        self.fase = fase
        self.save()
        return

    def add_trabajo_realizado(self, horas):
        """
        :type horas: int
        :param horas: cantidad de horas que se trabajo en el US
        Incrementa el trabajo realizado en un US una cantidad de horas especificadas como parámetro
        """
        self.trabajo_realizado += horas
        self.trabajo_por_realizar -= horas
        self.save()
        return

    def reset_estado_fase(self):
        """Devuelve un US a la primera fase del flujo"""
        self.estado_fase = 1
        self.save()
        return

