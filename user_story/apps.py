from django.apps import AppConfig


class UserStoryConfig(AppConfig):
    name = 'user_story'
