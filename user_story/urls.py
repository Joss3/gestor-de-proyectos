from django.urls import path, include
from . import views

urlpatterns = [
    path('<int:us_id>/', views.ver_user_story, name='ver_user_story'),
    path('<int:us_id>/editar/', views.editar_user_story, name='editar_user_story'),
    path('<int:us_id>/cancelar/', views.cancelar_user_story, name='cancelar_user_story'),
    path('<int:us_id>/asignar-encargado/', views.asignar_encargado_user_story, name='asignar_encargado_user_story'),
    path('crear/', views.crear_user_story, name='crear_user_story'),
    path('crear/<int:us_id>', views.CamposPersonalizadosView, name='campos_pers'),
    path('<int:us_id>/actividad/', include('actividad.urls')),

]
