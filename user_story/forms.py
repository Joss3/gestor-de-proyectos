from django import forms
from .models import UserStory
from proyecto.models import Proyecto
from usuarios.models import Usuario
from django.conf import settings
from tipo_us.models import *
from sprint_backlog.models import SprintBacklog


class DateInput(forms.DateInput):

    input_type = 'date'


class CrearUserStoryForm(forms.ModelForm):
    """
        Formulario para creacion de un nuevo User Story, teniendo
        como campos su nombre, la descripcion corta, la descripcion
        larga, los criterios de aceptacion, el trabajo por realizar
        en horas hombre, el valor del negocio, el valor tecnico y el
        usuario encargado del User Story en creacion
    """

    def __init__(self, *args, **kwargs):
        """
        Inicializa el formulario para crear un User Story listando a todos
        los usuarios del proyecto para que puedan ser asignados al User Story
        :param kwargs: Atravez de args se pasa el objeto del encargado y el
        id del proyecto al cual pertenece el User Story
        """
        self.proyecto = kwargs.pop('proyecto')
        super(CrearUserStoryForm, self).__init__(*args, **kwargs)
        scrum = Usuario.objects.filter(user=self.proyecto.scrum.user)
        tipo = Tipo_US.objects.filter(proyecto__id=self.proyecto.id)
        self.proyecto = Usuario.objects.filter(proyecto__id=self.proyecto.id).union(scrum)

        if not self.is_bound:
            self.fields["tipo"].queryset = tipo

    class Meta:
        """
            Se define el formato y la estructura del formulario para crear User Story
        """
        model = UserStory
        fields = {'nombre', 'descripcion', 'criterios_aceptacion', 'trabajo_por_realizar',
                  'prioridad', 'valor_negocio', 'valor_tecnico', 'tipo'}
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'col-sm-6'}),
            'descripcion': forms.Textarea(attrs={'rows': 2,'cols': 20}),
            'criterios_aceptacion': forms.Textarea(attrs={'rows': 2,'cols': 20}),
            'trabajo_por_realizar': forms.NumberInput(attrs={'min': 1}),
            'prioridad': forms.NumberInput(attrs={'min': 1, 'max': 10}),
            'valor_negocio': forms.NumberInput(attrs={'min': 1, 'max': 10}),
            'valor_tecnico': forms.NumberInput(attrs={'min': 1, 'max': 10}),
            'tipo': forms.Select()
        }

    field_order = ['nombre', 'descripcion', 'criterios_aceptacion', 'trabajo_por_realizar',
                   'prioridad', 'valor_negocio', 'valor_tecnico', 'tipo']


class EditarUserStoryForm(forms.ModelForm):

    """
        Formulario para edicion de un User Story, teniendo
        como campos su nombre, la descripcion corta, la descripcion
        larga y los criterios de aceptacion
    """

    class Meta:
        """
            Se define el formato y la estructura del formulario para editar User Story
        """
        model = UserStory
        fields = {'nombre', 'descripcion', 'criterios_aceptacion'}
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'col-sm-6'}),
            'descripcion': forms.Textarea(attrs={'rows': 2,'cols': 20}),
            'criterios_aceptacion': forms.Textarea(attrs={'rows': 2,'cols': 20}),
        }

    field_order = ['nombre', 'descripcion', 'criterios_aceptacion']


class CancelarUserStoryForm(forms.ModelForm):
    """
        Formulario para cancelacion de un User Story, el unico
        campo que se tiene es la justificacion de la cancelacion
        del User Story a cancelar
    """

    class Meta:
        """
            Se define el formato y la estructura del formulario para cancelar un User Story
        """
        model = UserStory
        fields = {'justificacion'}
        widgets = {
            'justificacion': forms.Textarea(attrs={'rows': 2,'cols': 20}),
        }


class AsignarUsuarioUserStoryForm(forms.ModelForm):
    """
        Formulario para asignar un encargado de un User Story,
        teniendo solamente el campo de usuario_asignado en el
        cual se carga el usuario asignado al User Story
    """

    def __init__(self, *args, **kwargs):
        """
        Se inicializa el formulario listando a los integrantes del proyecto
        al cual pertenece el User Story
        :param kwargs: Atravez de args se pasa el objeto del scrum master
        """
        self.proyecto = kwargs.pop('proyecto')
        super(AsignarUsuarioUserStoryForm, self).__init__(*args, **kwargs)
        scrum = Usuario.objects.filter(user=self.proyecto.scrum.user)
        self.proyecto = Usuario.objects.filter(proyecto__id=self.proyecto.id).union(scrum)
        if not self.is_bound:
            self.fields["usuario_asignado"].queryset = self.proyecto

    class Meta:
        """
            Se define el formato y la estructura del formulario para asignar un Usuario a un User Story
        """
        model = UserStory
        fields = {'usuario_asignado'}
        widgets = {'usuario_asignado': forms.RadioSelect()}


class CamposPersonalizadosForm(forms.ModelForm):

    """
    Formulario de campos personalizados
    modelo: Tipo_US
    fields = nombre
    """

    class Meta:
        model = Tipo_US
        fields = ['nombre']
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control', 'readonly':'readonly'}, ),
        }

    def __init__(self, *args, **kwargs):
        """
        constructor del formulario, en el se construyen los campos adicionales dinamicamente obteniendo un
        queryset de CampoPersonalizado
        :param args: argumentos pasados al constructor
        :param kwargs: se le pasan datos tipo clave : valor como un diccionario, en el recibe la instancia
        """
        super(CamposPersonalizadosForm, self).__init__(*args, **kwargs)
        # self.fields['proyecto'].initial =

        campos_por_tipo = Tipo_Campo.objects.filter(tipo=kwargs['instance'])

        #campos_por_us = US_Tipo_Campo_Valor.objects.filter(us = kwargs['us'])
        # obtener el objeto que tiene el valor para la
        # valores =  tipo_campo_valor.object.filter(tipo_us = kwargs['instance'], campo = kwargs[''] )
        print(campos_por_tipo)

        if campos_por_tipo != None:

            for utcv in campos_por_tipo:
                # genera los campos extras
                campo = utcv.campo  # trae el campo de la lista de campos por tipo de user story
                index = '%s' % campo.nombre_campo
                # valor = campo.imput_campo

                if campo.tipo_dato == 'TEXT':
                    self.fields[index] = forms.CharField()
                elif campo.tipo_dato == 'INT':
                    self.fields[index] = forms.IntegerField()
                elif campo.tipo_dato == 'BOOL':
                    self.fields[index] = forms.BooleanField()

        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'


class AsignarFlujoForm(forms.ModelForm):
    """
        Formulario para asignar un flujo al User Story
    """
    def __init__(self, *args, **kwargs):
        """
        Se inicializa el formulario listando las fases del flujo
        :param kwargs: Atravez de args se pasa el objeto del scrum master
        """
        self.flujos = kwargs.pop('flujos')
        super(AsignarFlujoForm, self).__init__(*args, **kwargs)
        if not self.is_bound:
            self.fields["flujo"].queryset = self.flujos

    class Meta:
        """
            Se define el formato y la estructura del formulario para asignar flujo al User Story
        """
        model = UserStory
        fields = {'flujo'}
        widgets = {
            'flujo': forms.Select(),
        }


class AsignarUsuarioForm(forms.ModelForm):
    """Formulario de asignación de usuario a US"""

    def __init__(self, *args, **kwargs):
        self.sprint = kwargs.pop('sprint')
        super(AsignarUsuarioForm, self).__init__(*args, **kwargs)
        if not self.is_bound:
            sprint_backlog = SprintBacklog.objects.get(sprint_id__exact=self.sprint.id)
            uss = UserStory.objects.all().filter(sprint_backlog_id=sprint_backlog.id)
            usuarios = self.sprint.miembros.all().exclude(pk=self.sprint.proyecto.scrum.id)
            usuarios_us_id = []
            usuarios_id = []
            for usuario in usuarios:
                aux = 0
                for us in uss:
                    if usuario == us.usuario_asignado:
                        aux += 1
                if aux > 0:
                    usuarios_us_id.append(usuario.id)
                else:
                    usuarios_id.append(usuario.id)
            usuarios_sin_us = Usuario.objects.all().filter(id__in=usuarios_id)
            self.fields["usuario_asignado"].queryset = usuarios_sin_us

    class Meta:
        model = UserStory
        fields = {'usuario_asignado'}
        widgets = {
            'usuario_asignado': forms.Select(),
        }


class ReasignarTrabajoForm(forms.ModelForm):
    """
    Formulario para reasignar el tiempo por trabajar de un US del cual sus Horas Trabajadas
    es negativo, lo cual indica que se tenia mas trabajo de lo esperado y se lo tiene que
    reevaluar
    """

    class Meta:
        model = UserStory
        fields = {'trabajo_por_realizar', }
        widgets = {
            'trabajo_por_realizar': forms.NumberInput(attrs={'min': 1}),
        }
        labels = {
            "descripcion": "Cantidad de Trabajo por Realizar",
        }
