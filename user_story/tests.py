from django.test import TestCase, Client
from user_story.models import UserStory
from django.contrib.auth.models import User
from usuarios.models import Usuario
from proyecto.models import Proyecto
from tipo_us.models import Tipo_US
from user_story.forms import CrearUserStoryForm, EditarUserStoryForm, AsignarUsuarioUserStoryForm, CancelarUserStoryForm
from django.forms import ModelChoiceField

class UserStoryTestCase(TestCase):
    def setUp(self):
        user = User.objects.create(username='pedro', email='pedro@test.com')
        user.set_password('00000000')
        user.save()
        User.objects.create(username='pablo', email='pablo@test.com')
        User.objects.create(username='juan', email='juan@test.com')
        usuarios = Usuario.objects.all().exclude(user__username='AnonymousUser').exclude(user=user)
        proy = Proyecto.objects.create(nombre='Proyecto 1', fecha_inicio='2018-01-01', estado='P', scrum=Usuario.objects.get(user=user))
        proy.usuarios.set(usuarios)
        Tipo_US.objects.create(nombre='tipo', proyecto=proy).save()
        proy.save()
        encargado = Usuario.objects.get(user=user)
        UserStory.objects.create(nombre='US2', descripcion='PRUEBA',
                                 criterios_aceptacion='PRUEBA', trabajo_por_realizar=10,
                                 valor_negocio=10, valor_tecnico=10, usuario_asignado=encargado)

    def test_form_crear_user_story(self):
        c = Client()
        c.login(username='pedro', password='00000000')
        proy = Proyecto.objects.get(nombre='Proyecto 1')
        usuario = User.objects.filter(username='juan')
        # Falta campo tipo en data
        data = {'nombre': 'US1', 'descripcion': 'PRUEBA',
                'criterios_aceptacion': 'PRUEBA', 'trabajo_por_realizar': 8,
                'valor_negocio': 8, 'valor_tecnico': 8}
        form = CrearUserStoryForm(data=data, proyecto=proy)
        self.assertTrue(form.is_valid())
        us = form.save()
        self.assertTrue(UserStory.objects.get(nombre='US1'))

    def test_form_editar_user_story(self):
        c = Client()
        c.login(username='pedro', password='00000000')
        proy = Proyecto.objects.get(nombre='Proyecto 1')
        usuario = User.objects.get(username='pedro', email='pedro@test.com')
        us = UserStory.objects.get(nombre='US2')
        data = {'nombre': 'USER STORY 2', 'descripcion': 'PRUEBA DE USER STORY 2',
                'criterios_aceptacion': 'PRUEBA 3 DE USER STORY 2'}
        form = EditarUserStoryForm(data=data, instance=us)
        self.assertTrue(form.is_valid())
        form.save()
        self.assertEqual(us.nombre, 'USER STORY 2')
        self.assertEqual(us.descripcion, 'PRUEBA DE USER STORY 2')
        self.assertEqual(us.criterios_aceptacion, 'PRUEBA 3 DE USER STORY 2')

    def test_form_cancelar_user_story(self):
        c = Client()
        c.login(username='pedro', password='00000000')
        us = UserStory.objects.get(nombre='US2')
        data = {'justificacion': 'SE CANCELA PARA PROBAR'}
        form = CancelarUserStoryForm(data=data, instance=us)
        self.assertTrue(form.is_valid())
        form.save()
        self.assertEqual(us.justificacion, 'SE CANCELA PARA PROBAR')

    def test_form_asignar_user_story_encargado(self):
        c = Client()
        c.login(username='pedro', password='00000000')
        us = UserStory.objects.get(nombre='US2')
        proy = Proyecto.objects.get(nombre='Proyecto 1')
        usuario = Usuario.objects.get(user__username='pablo')
        usuario_id = usuario.pk
        data = {'usuario_asignado': usuario_id}
        form = AsignarUsuarioUserStoryForm(instance=us, proyecto=proy, data=data)
        self.assertTrue(form.is_valid())
        form.save()
        self.assertEqual(us.usuario_asignado, usuario)

    def test_visualizar_user_story(self):
        c = Client()
        c.login(username='pedro', password='00000000')
        us = UserStory.objects.get(nombre='US2')
        proy = Proyecto.objects.get(nombre='Proyecto 1')
        usuario = Usuario.objects.get(user__username='pedro')
        link = '/proyecto/'+str(proy.id)+'/user-stories/'+str(us.id)+'/'
        # Error
        '''response = c.get(link)
        self.assertContains(response, 'US2')
        self.assertContains(response, 'PRUEBA')
        self.assertContains(response, usuario.user)'''
