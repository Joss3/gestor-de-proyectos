from django import forms


class loginform(forms.Form):
	"""
	Formulario del login
	"""

	username = forms.CharField(label='username', max_length=100)
	"""Nombre de usuario"""
	password = forms.CharField(label='password', max_length=100)
	"""Contraseña"""
