from django.test import TestCase
from django.test import Client
from django.contrib.auth.models import User
from .views import hacer_login


class TestVistaLogin(TestCase):

    def setUp(self):
        User.objects.create_user(username='usuario1', password='contraseña')

    def test_hacer_login(self):

        response = self.client.post('', {'username': 'usuario1', 'password': 'contraseña'})

        self.assertEqual(response.status_code, 302)
        self.client.login(username='usuario1',password='contraseña')
