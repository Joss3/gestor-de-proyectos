from django.urls import path, include
from django.contrib import admin
from django.contrib.auth.context_processors import auth
from django.contrib.auth import views as auth_views
from django.contrib.auth.decorators import login_required
from . import views

urlpatterns = [
    path('', views.hacer_login, name='login'),
    path('logout/', views.hacer_logout, name='logout_url'),
    path('home/', login_required(views.mostrar_interfaz), name='index'),
]