from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth import *
from django.contrib.auth.models import *
from .formulario import loginform
from django.urls import reverse_lazy
from proyecto.models import Proyecto
from usuarios.models import Usuario


@login_required()
def hacer_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse_lazy('login'))


def hacer_login(request):
    """
    Implementación de login que en el caso de estar logueado redirecciona a /home/

    :param request: peticion http de la url en el form
    :return: formulario de inicio de sesion
    """
    if request.method == 'GET':
        if request.user.is_authenticated:
            return HttpResponseRedirect('/home/')
        else:
            return render(request, 'login/formulario.html', {})
    else:
        if request.method == 'POST':  # Si el formulario se envio
            form = loginform(request.POST)  # Un formulario virtual enlazado a los datos post
            if form.is_valid():  # All validation rules pass
                username = form.cleaned_data['username']
                password = form.cleaned_data['password']
                user = authenticate(username=username, password=password)
                if user is not None:
                    objeto_permiso = User.objects.get(username=username).get_all_permissions()
                    login(request, user)
                    return HttpResponseRedirect('/home/')

                else:
                    return render(request, 'login/error.html', {}, status=404)


@login_required()
def mostrar_interfaz(request):
    """
    vista que muestra la interfaz principal al loguearse un usuario, el home, con todos los proyectos
    a los cuales pertenece el usuario

    :param request: contiene todos los datos de la solicitud Http
    :return: plantilla de menu principal que lista los proyectos relacionados
    """
    user = request.user
    usuario = Usuario.objects.get(user=user)
    objeto_permiso = User.objects.get(username=user).get_all_permissions()  # tendra roles predeterminados
    roluserproyecto = None
    request.session['atras_sprint'] = 2
    if request.session.is_empty():
        return render(request, 'proyecto/error_inicio_session.html', {})
    else:
        # if usuario.is_admin():
            # proyectos = Proyecto.objects.all().order_by('nombre')
        # else:
        miembro = user.usuario_set.all()
        proyectos = miembro[0].proyecto_set.all()
        proyecto_scrum = miembro[0].scrum.all()
        proyectos = proyectos.union(proyecto_scrum)

        proyectos = proyectos.order_by('nombre')

        page = request.GET.get('page', 1)
        paginator = Paginator(proyectos, 5)
        try:
            proyectos = paginator.page(page)
        except PageNotAnInteger:
            proyectos = paginator.page(1)
        except EmptyPage:
            proyectos = paginator.page(paginator.num_pages)
    return render(request, 'login/logeado.html', {"objeto_permiso": objeto_permiso, "usuario": usuario,
                                                  "roluserproyecto": roluserproyecto, "proyectos": proyectos})
