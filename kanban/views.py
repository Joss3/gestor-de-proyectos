from django.shortcuts import render
from proyecto.models import Proyecto
from user_story.models import UserStory
from tipo_us.models import Tipo_US
from django.shortcuts import redirect
from usuarios.models import Usuario
from sprint.models import Sprint
from django.core.mail import send_mail
from gestorProyectos.email_info import EMAIL_HOST_USER

from flujo.models import Flujo, Fase
# Create your views here.

# Estados del US
TO_DO = 1
DOING = 2
DONE = 3

# Estados del Proyecto
PENDIENTE = 'P'
ACTIVO = 'A'
PAUSADO = 'S'
FINALIZADO = 'F'
CANCELADO = 'C'

# Estados del US en Proyecto
NO_INICIADO = 'N'
EN_PROCESO = 'P'


def ver_kanban(request, proyecto_id):
    """
    Muestra el tablero kanban los US finalizados del proyecto y los del Sprint en curso, cargando
    todos los US,los flujos y las fases, tambien permite a los encargados de los US moverlos mediantes flechas
    que se muestran en el tablero a los estados siguientes y anteriores.
    Solo se puede mover de DOING a DONE si se agrego almenos una actividad al US.
    Una vez que se llego el US al ultimo estado de la ultima fase de un flujo se debe esperar a
    que el Scrum Master del Proyecto acepte o rechace para poder moverlo de nuevo.

    :param request:
    :param proyecto_id: El id del proyecto al cual pertenece el kanban
    :return: Una respuesta HTTP con el template del kanban y sus flujos con los US's ubicados
    """

    proy = Proyecto.objects.get(pk=proyecto_id)
    us_sprint = UserStory.objects.filter(product_backlog=proy.productbacklog_set.all()[0]).exclude(
        estado_proyecto=CANCELADO).exclude(estado_proyecto=NO_INICIADO)
    if (proy.estado == ACTIVO or proy.estado == FINALIZADO or proy.is_pausado()) and us_sprint:
        flujos = []
        for us in us_sprint:
            if us.flujo not in flujos:
                flujos.append(us.flujo)

        if flujos:
            flujo_actual = flujos[0]
            fases = Fase.objects.filter(id_flujo=flujo_actual.id)
            fase_actual = fases[0]

        if request.GET.get('cambio_flujo'):
            flujo_id = request.GET.get('cambio_flujo')
            flujo_actual = Flujo.objects.get(id=int(flujo_id))
            fases = Fase.objects.filter(id_flujo=int(flujo_id))
            fase_actual = fases[0]

        if request.GET.get('cambio_fase'):
            fase_id = request.GET.get('us')
            fase_actual = Fase.objects.get(id=fase_id)

        if request.GET.get('estado_anterior'):
            us = UserStory.objects.get(id=int(request.GET.get('us')))
            flujo_actual = Flujo.objects.get(id=us.flujo.id)
            fases = Fase.objects.filter(id_flujo=flujo_actual.id)
            if us.estado_fase == DOING:
                us.estado_fase = TO_DO
            elif us.estado_fase == TO_DO and not us.fin:
                if us.fase.indice > 1:
                    us.fase = Fase.objects.filter(id_flujo=us.flujo_id).get(indice=us.fase.indice - 1)
                    us.estado_fase = DOING
            else:
                us.estado_fase = DOING
            us.save()

        if request.GET.get('sigte_estado'):
            us = UserStory.objects.get(id=int(request.GET.get('us')))
            flujo_actual = Flujo.objects.get(id=us.flujo.id)
            fases = Fase.objects.filter(id_flujo=flujo_actual.id)
            if us.estado_fase == TO_DO:
                us.estado_fase = DOING
                fase_actual = us.fase
            elif us.estado_fase == DOING and us.actividad_agregada:
                us.estado_fase = DONE
                us.actividad_agregada = False
                fase_actual = us.fase
                if us.flujo.cant_fases == us.fase.indice:
                    us.fin = True
                    email_scrum = Proyecto.objects.get(id=proyecto_id).scrum.user.email
                    send_mail(f'User Story {us.nombre} ha finalizado las fases',
                              f'El user story {us.nombre} necesita ser revisado para poder finalizar el flujo',
                              EMAIL_HOST_USER,
                              [email_scrum],
                              fail_silently=True,
                              )
            elif not us.estado_fase == DOING:
                if us.flujo.cant_fases > us.fase.indice:
                    fase_sigte = Fase.objects.filter(id_flujo=us.flujo.id).get(indice=us.fase.indice + 1)
                    us.fase = fase_sigte
                    us.estado_fase = TO_DO
                fase_actual = us.fase
            us.save()

        fase = Fase.objects.filter(id_flujo=flujo_actual.id)
        fase = fase.get(indice=1)
        us_fase = UserStory.objects.filter(fase=fase).exclude(estado_proyecto=NO_INICIADO).exclude(estado_proyecto=CANCELADO)
        us_todo = us_fase.filter(estado_fase=1)
        us_doing = us_fase.filter(estado_fase=2)
        us_done = us_fase.filter(estado_fase=3)
        for fase in Fase.objects.filter(id_flujo=flujo_actual.id):
            us_fase = UserStory.objects.filter(fase=fase).exclude(estado_proyecto=NO_INICIADO).exclude(estado_proyecto=CANCELADO)
            us_todo = us_todo.union(us_fase.filter(estado_fase=1))
            us_doing = us_doing.union(us_fase.filter(estado_fase=2))
            us_done = us_done.union(us_fase.filter(estado_fase=3))

        request.session['atras_us'] = 2
        return render(request, 'ver_kanban.html', {'proy': proy, 'flujos': flujos, 'flujo_actual': flujo_actual,
                                                   'fases': fases, 'fase_actual': fase_actual,
                                                   'us_todo': us_todo, 'us_doing': us_doing, 'us_done': us_done,
                                                   'usuario_actual': Usuario.objects.get(user=request.user)
                                                   })
    else:
        return redirect('ver_proyecto', proyecto_id)
