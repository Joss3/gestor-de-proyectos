from django.shortcuts import render
from usuarios.models import Usuario
from user_story.models import UserStory
from .forms import ActividadForm, ArchivoForm
from django.shortcuts import redirect, HttpResponse
from .models import Actividad, Archivo
from proyecto.models import Proyecto
from sprint.models import Sprint, TrabajoDiario
import os
import hashlib
from gestorProyectos.settings import BASE_DIR
import datetime


def crear_actividad(request, proyecto_id, us_id):
    """
    Vista que permite la creacion de actividades, manejando los tiempos trabajados en
    ese US y permitiendo adjuntarle archivos

    :param request:
    :param proyecto_id: El id del proyecto al cual pertenece el US de la actividad
    :param us_id: El id del US al cual se va a cargar la actividad
    :return:
    """
    user = request.user
    usuario = Usuario.objects.get(user=user)
    us = UserStory.objects.get(id=us_id)
    if not request.session.is_empty() and us.usuario_asignado == usuario and us.estado_fase == 2:
        if request.method == 'POST':
            form = ActividadForm(request.POST)
            if form.is_valid():
                act = form.save(commit=False)
                act.user_story = us
                act.fecha_cargado = datetime.date.today()
                act.autor = usuario
                act.save()
                act = Actividad.objects.get(id=act.id)
                # us.trabajo_por_realizar = us.trabajo_por_realizar - act.trabajo
                # us.trabajo_realizado = us.trabajo_realizado + act.trabajo
                us.actividad_agregada = True
                us.add_trabajo_realizado(act.trabajo)
                horas = act.trabajo
                us.iniciado = True
                sprint = Sprint.objects.get(pk=us.sprint_backlog.sprint.id)

                # Lista de horas trabajadas por dia, para el burndownchart
                trabajos = TrabajoDiario.objects.all().filter(sprint=sprint).order_by('id')
                trabajoss = []
                for trabajo in trabajos:
                    trabajoss.append(trabajo)

                dia = (act.fecha_cargado - sprint.fecha_inicio).days

                trabajo = trabajoss[dia]

                trabajo.add_trabajo_realizado(horas)

                id_inicial = trabajoss[0].id
                dia_actual = sprint.get_cant_dias()

                id_actual = id_inicial + dia_actual

                for trabajo in trabajoss:
                    if trabajo.id > id_actual:
                        trabajo.add_trabajo_realizado(horas)

                us.save()
                return redirect('adjuntar_archivo', proyecto_id=proyecto_id, us_id=us_id, actividad_id=act.id)
        form = ActividadForm()
        return render(request, 'actividad/formularios_actividad.html', {'form': form, 'operacion': 1, 'proyecto_id': proyecto_id, 'us_id': us_id})
    elif request.session.is_empty():
        return redirect('login')
    else:
        return redirect('ver_user_story', proyecto_id=proyecto_id, us_id=us_id)


def editar_actividad(request, proyecto_id, us_id, actividad_id):
    """
    Vista para edicion de una actividad, pudiendose editar su descripcion y las horas hombres trabajadas

    :param request:
    :param proyecto_id: El id del proyecto al cual pertenece el US de la actividad
    :param us_id: El id del US al cual pertenece la actividad
    :param actividad_id: El id de la actividad a editar
    :return:
    """
    user = request.user
    usuario = Usuario.objects.get(user=user)
    us = UserStory.objects.get(id=us_id)
    act = Actividad.objects.get(id=actividad_id)
    if not request.session.is_empty() and us.usuario_asignado == usuario and act.user_story.id == us.id and us.product_backlog.proyecto.id == proyecto_id and us.estado_proyecto != 'F' and us.estado_proyecto != 'C':
        if request.method == 'POST':
            form = ActividadForm(request.POST, instance=act)
            if form.is_valid():
                us.trabajo_por_realizar = us.trabajo_por_realizar + act.trabajo
                us.trabajo_realizado = us.trabajo_realizado - act.trabajo
                form.save()
                act = Actividad.objects.get(id=act.id)
                us.trabajo_por_realizar = us.trabajo_por_realizar - act.trabajo
                us.trabajo_realizado = us.trabajo_realizado + act.trabajo
                us.save()
                return redirect('ver_actividad', proyecto_id=proyecto_id, us_id=us_id, actividad_id=actividad_id)
        form = ActividadForm(instance=act)
        return render(request, 'actividad/formularios_actividad.html', {'form': form, 'operacion': 4, 'proyecto_id': proyecto_id, 'us_id': us_id, 'actividad_id': actividad_id, 'actividad': act})
    elif request.session.is_empty():
        return redirect('login')
    else:
        return redirect('ver_actividad', proyecto_id=proyecto_id, us_id=us_id, actividad_id=actividad_id)


def ver_actividad(request, proyecto_id, us_id, actividad_id):
    """
    Vista para visualizar la actividad

    :param request:
    :param proyecto_id: El id del proyecto al cual pertenece el US de la actividad
    :param us_id: El id del US al cual pertenece la actividad
    :param actividad_id: El id de la actividad a visualizar
    :return:
    """
    user = request.user
    usuario = Usuario.objects.get(user=user)
    proy = Proyecto.objects.get(id=proyecto_id)
    us = UserStory.objects.get(id=us_id)
    act = Actividad.objects.get(id=actividad_id)
    hay_archivo = False
    if not act.archivo_set.all().count() == 0 and not act.archivo_set.all()[0].es_vacio:
        hay_archivo = True
    return render(request, 'actividad/ver_actividad.html',
                  {'hay_archivo': hay_archivo, 'actividad': act,'proy': proy, 'usuario': usuario, 'us_id': us_id, 'actividad_id': actividad_id })


def adjuntar_archivo(request, proyecto_id, us_id, actividad_id):
    """
    Vista para adjuntar archivos a la actividad mientras se esta creando, cubriendo los casos en que
    se adjunte y no se adjunten los archivos

    :param request:
    :param proyecto_id: El id del proyecto al cual pertenece el US de la actividad
    :param us_id: El id del US al cual pertenece la actividad
    :param actividad_id: El id de la actividad a la cual se le va a adjuntar archivos
    :return:
    """
    user = request.user
    usuario = Usuario.objects.get(user=user)
    us = UserStory.objects.get(id=us_id)
    act = Actividad.objects.get(id=actividad_id)
    if not request.session.is_empty() and us.usuario_asignado == usuario and us.estado_fase == 2:
        if request.method == 'POST':
            form = ArchivoForm(request.POST, request.FILES)
            if form.is_valid():
                archivo = form.save()
                archivo.actividad = act
                archivo.save()
                archivo = Archivo.objects.get(actividad=act)
                if not request.FILES.__len__() == 0:
                    archivo.set_data(request.FILES['archivo_adjunto'])
                    archivo.nombre = archivo.archivo_adjunto.name
                    archivo.archivo_adjunto = None
                    archivo.es_vacio = True
                archivo.save()
                return redirect('ver_user_story', proyecto_id=proyecto_id, us_id=us_id)
        form = ArchivoForm()
        return render(request, 'actividad/adjuntar_archivo.html',
                      {'form': form, 'operacion': 3, 'actividad': act, 'proyecto_id': proyecto_id,
                       'us_id': us_id, 'actividad_id': actividad_id})
    elif request.session.is_empty():
        return redirect('login')
    else:
        return redirect('ver_actividad', proyecto_id=proyecto_id, us_id=us_id, actividad_id=actividad_id)


def ver_archivo(request, proyecto_id, us_id, actividad_id):
    """
    Vista para obtener el archivo adjunto de la actividad que se esta visualizando

    :param request:
    :param proyecto_id: El id del proyecto al cual pertenece el US de la actividad
    :param us_id: El id del US al cual pertenece la actividad
    :param actividad_id: El id de la actividad de la cual se quiere obtener el archivo adjunto
    :return: El archivo adjunto de la actividad
    """
    act = Actividad.objects.get(id=actividad_id)
    archivo = act.archivo_set.all()[0]
    archivo_a_devolver = archivo.get_data()
    respuesta = HttpResponse(content=archivo_a_devolver)
    respuesta['Content-Type'] = 'application/octet-stream'
    respuesta['Content-Disposition'] = 'attachment; filename="%s"' % archivo.nombre
    return respuesta
