from django.db import models
from user_story.models import UserStory
from django.core.validators import MinValueValidator
import pickle
from usuarios.models import Usuario


class Actividad(models.Model):
    """
    Registro del trabajo que se invierte en un User Story

    """

    user_story = models.ForeignKey(UserStory, on_delete=models.CASCADE,  null=True, blank=True)
    """User Story al cual pertenece la actividad"""
    descripcion = models.CharField(max_length=500, null=True, blank=True)
    """Una descripcion de lo que se realizo en la actividad"""
    fecha_cargado = models.DateField(null=True, blank=True, help_text="En formato DD/MM/AAAA")
    """Fecha en que se cargo esta actividad al sistema"""
    trabajo = models.IntegerField(null=True, blank=True, validators=[MinValueValidator(1)], help_text="Horas dedicadas en esta actividad")
    """Cantidad de horas trabajadas en la actividad"""
    autor = models.ForeignKey(Usuario, on_delete=models.DO_NOTHING, null=True, blank=True)
    """Usuario que cargo la actividad al sistema"""

    def __str__(self):
        return f' { self.id }'


class Archivo(models.Model):

    nombre = models.TextField(null=True, blank=True)
    """El nombre del archivo que se adjunto a la actividad"""
    archivo_adjunto = models.FileField(null=True, blank=True)
    """El archivo el cual se quiere adjuntar a la actividad"""
    archivo_binario = models.BinaryField(null=True, blank=True)
    """El mismo archivo a adjuntar pero en binario para almacenarlo en la base de datos"""
    actividad = models.ForeignKey(Actividad, on_delete=models.CASCADE, null=True, blank=True)
    """Una referencia a la actividad al cual pertenece"""
    es_vacio = models.BooleanField(default=True)
    """Una bandera que indica si el archivo creado esta en blanco o no"""

    def set_data(self, data):
        """
        :type data: File
        :param data: Archivo que sera guardado en la base de datos
        Transforma un archivo normal a un archivo binario para guardarlo en la base de datos
        """
        self.archivo_binario = pickle.dumps(data)

    def get_data(self):
        """
        :return: retorna el archivo al usuario
        Convierte un archivo binario a su formato original
        """
        return pickle.loads(self.archivo_binario)
