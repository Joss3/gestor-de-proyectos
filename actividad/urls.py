from django.urls import path
from . import views

urlpatterns = [
    path('<int:actividad_id>/', views.ver_actividad, name='ver_actividad'),
    path('crear/', views.crear_actividad, name='crear_actividad'),
    path('<int:actividad_id>/editar/', views.editar_actividad, name='editar_actividad'),
    path('<int:actividad_id>/adjuntar-archivo/', views.adjuntar_archivo, name='adjuntar_archivo'),
    path('<int:actividad_id>/ver-archivo/', views.ver_archivo, name='ver_archivo'),
]
