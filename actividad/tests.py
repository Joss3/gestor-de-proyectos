from django.test import TestCase, Client
from user_story.models import UserStory
from django.contrib.auth.models import User
from actividad.models import Actividad, Archivo
from usuarios.models import Usuario
from proyecto.models import Proyecto
from tipo_us.models import Tipo_US, CampoPersonalizado, Tipo_Campo
from flujo.models import Flujo, Fase
from product_backlog.models import ProductBacklog
from sprint_backlog.models import SprintBacklog
from sprint.models import Sprint
from django.urls import reverse_lazy, reverse
import tempfile

# Estados del US
TO_DO = 1
DOING = 2
DONE = 3

# Constantes de estado de proyecto y sprint
PENDIENTE = 'P'
ACTIVO = 'A'
FINALIZADO = 'F'
CANCELADO = 'C'
PAUSADO = 'S'

# Constantes de estado de un US
NO_INICIADO ='N'
EN_PROCESO = 'P'


class ActividadTestCase(TestCase):
    def setUp(self):
        user = User.objects.create(username='pedro', email='pedro@test.com')
        user.set_password('00000000')
        user.save()
        User.objects.create(username='pablo', email='pablo@test.com')
        User.objects.create(username='juan', email='juan@test.com')
        usuarios = Usuario.objects.all().exclude(user__username='AnonymousUser').exclude(user=user)
        proy = Proyecto.objects.create(nombre='Proyecto 1', fecha_inicio='2018-01-01', estado='P',
                                       scrum=Usuario.objects.get(user=user))
        proy.usuarios.set(usuarios)
        proy.save()
        product_backlog = ProductBacklog.objects.create(proyecto=proy)
        product_backlog.save()
        sprint = Sprint.objects.create(duracion=14, fecha_inicio='2018-01-02', fecha_fin="2018-01-16",
                                       estado='P', proyecto=proy)
        for miembro in usuarios:
            miembro.set_en_sprint(sprint.id)
            sprint.miembros.add(miembro)

        sprint_backlog = SprintBacklog.objects.create(sprint=sprint)
        Tipo_US.objects.create(nombre='tipo', proyecto=proy).save()
        tipo = Tipo_US.objects.get(nombre='tipo')
        campo = CampoPersonalizado.objects.create(nombre_campo='campo1', tipo_dato='TEXT', proyecto_or=proy.id)
        campo.save()
        flujo = Flujo.objects.create(nombre='flujo', tipo=tipo)
        flujo.save()
        tipo_campo = Tipo_Campo.objects.create(tipo=tipo, campo=campo)
        fase = Fase.objects.create(nombre='fase1', id_flujo=flujo)
        fase.save()
        Fase.objects.create(nombre='fase2', id_flujo=flujo).save()
        Fase.objects.create(nombre='fase3', id_flujo=flujo).save()

        encargado = Usuario.objects.get(user=user)
        UserStory.objects.create(nombre='US1', descripcion='PRUEBA',criterios_aceptacion='PRUEBA',
                                 trabajo_por_realizar=120,valor_negocio=10, valor_tecnico=10, usuario_asignado=encargado,
                                 trabajo_realizado=0, flujo=flujo, tipo=tipo, prioridad=10, prioridad_final=7,
                                 estado_proyecto='P', fase=fase, estado_fase=1,product_backlog=product_backlog,
                                 sprint_backlog=sprint_backlog, iniciado=False)


    def test_view_crear_actividad(self):
        self.assertTrue(self.client.login(username='pedro', password='00000000'))
        proy = Proyecto.objects.get(nombre='Proyecto 1')
        usuario = User.objects.get(username='pedro', email='pedro@test.com')
        us = UserStory.objects.get(nombre='US1')
        sprint = Sprint.objects.get(proyecto=proy)
        trabajo_por_realizar = us.trabajo_por_realizar
        us.estado_proyecto = EN_PROCESO
        us.estado_fase = DOING
        us.save()

        self.client.get(reverse_lazy('cambiar_estado_sprint', kwargs={'sprint_id': sprint.id, 'estado': ACTIVO}),
                                   follow=True)
        response = self.client.get(reverse_lazy('crear_actividad', kwargs={'proyecto_id': proy.id, 'us_id': us.id}), follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'actividad/formularios_actividad.html')

        response = self.client.post(reverse_lazy('crear_actividad', kwargs={'proyecto_id': proy.id, 'us_id': us.id}),
                                    {'descripcion': 'Se realizo una funcion', 'trabajo': 15}, follow=True)

        self.assertEqual(response.status_code, 200, msg="El estado de la llamada HTTP no fue correcto")
        self.assertTemplateUsed(response, 'actividad/adjuntar_archivo.html', msg_prefix="No llevo al template de adjuntar archivos")
        actividad = Actividad.objects.get(descripcion='Se realizo una funcion')
        us = actividad.user_story
        self.assertEquals(actividad.descripcion, 'Se realizo una funcion', msg="No asigno correctamente la descripcion de la actividad")
        self.assertEquals(actividad.trabajo, 15, msg="No asigno correctamente la cantidad de horas trabajadas en la actividad")
        self.assertEquals(us.trabajo_realizado, 15, msg="No resto correctamente la cantidad de trabajo realizado")
        self.assertEquals(us.trabajo_por_realizar+15, trabajo_por_realizar, msg="No resto correctamente la cantidad de trabajo realizado")
        self.assertEqual(us.actividad_agregada, True, msg="No aviso que si se agrego una actividad al US")
        self.assertRedirects(response, reverse('adjuntar_archivo', kwargs={'proyecto_id': proy.id, 'us_id': us.id, 'actividad_id': actividad.id}))


    def test_view_ver_actividad(self):
        self.assertTrue(self.client.login(username='pedro', password='00000000'))
        proy = Proyecto.objects.get(nombre='Proyecto 1')
        usuario = User.objects.get(username='pedro', email='pedro@test.com')
        us = UserStory.objects.get(nombre='US1')
        sprint = Sprint.objects.get(proyecto=proy)
        trabajo_por_realizar = us.trabajo_por_realizar
        us.estado_proyecto = EN_PROCESO
        us.estado_fase = DOING
        us.save()
        self.client.get(reverse_lazy('cambiar_estado_sprint', kwargs={'sprint_id': sprint.id, 'estado': ACTIVO}),
                                   follow=True)
        self.client.post(reverse_lazy('crear_actividad', kwargs={'proyecto_id': proy.id, 'us_id': us.id}),
                                    {'descripcion': 'Se realizo una funcion', 'trabajo': 15}, follow=True)
        actividad = Actividad.objects.get(descripcion='Se realizo una funcion')
        response = self.client.get(reverse_lazy('ver_actividad', kwargs={'proyecto_id': proy.id, 'us_id': us.id, 'actividad_id': actividad.id}),
                        follow=True)
        self.assertEqual(response.status_code, 200, msg="El estado de la llamada HTTP no fue correcto")
        self.assertContains(response, 'Se realizo una funcion', msg_prefix='No se devolvio la vista correcta para visualizar el US')

    def test_view_adjuntar_archivos(self):
        self.assertTrue(self.client.login(username='pedro', password='00000000'))
        proy = Proyecto.objects.get(nombre='Proyecto 1')
        usuario = User.objects.get(username='pedro', email='pedro@test.com')
        us = UserStory.objects.get(nombre='US1')
        sprint = Sprint.objects.get(proyecto=proy)
        trabajo_por_realizar = us.trabajo_por_realizar
        us.estado_proyecto = EN_PROCESO
        us.estado_fase = DOING
        us.save()
        self.client.get(reverse_lazy('cambiar_estado_sprint', kwargs={'sprint_id': sprint.id, 'estado': ACTIVO}),
                                   follow=True)
        self.client.post(reverse_lazy('crear_actividad', kwargs={'proyecto_id': proy.id, 'us_id': us.id}),
                                    {'descripcion': 'Se realizo una funcion', 'trabajo': 15}, follow=True)
        actividad = Actividad.objects.get(descripcion='Se realizo una funcion')
        response = self.client.post(reverse_lazy('adjuntar_archivo', kwargs={'proyecto_id': proy.id, 'us_id': us.id, 'actividad_id': actividad.id}),
                                    {'archivo_adjunto': tempfile.NamedTemporaryFile().name, 'actividad': actividad}, follow=True)
        archivo = Archivo.objects.get(archivo_adjunto=tempfile.NamedTemporaryFile().name)
        self.request.session['atras_us'] = 1
        self.assertIsNone(archivo.archivo_adjunto, msg='Hay archivos adjuntos en directorio')
        self.assertContains(response.get('Content-Disposition'), "attachment", msg_prefix='No responde con el archivo adjunto')


