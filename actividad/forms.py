from django import forms
from .models import Actividad, Archivo


class ActividadForm(forms.ModelForm):
    """
    Formulario para crear una actividad
    """
    def __init__(self, *args, **kwargs):
        super(ActividadForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Actividad
        fields = {'descripcion', 'trabajo', }
        widgets = {
            'descripcion': forms.Textarea(attrs={'rows': 2,'cols': 20}),
            'trabajo': forms.NumberInput(attrs={'min': 1}),
        }

    field_order = ['descripcion', 'trabajo', ]


class ArchivoForm(forms.ModelForm):
    """
    Formulario para adjuntar un archivo a una actividad, teniendo un solo campo
    el de archivo
    """
    class Meta:
        model = Archivo
        fields = ('archivo_adjunto', )