from django.contrib import admin
from .models import Rol, Permiso

# Register your models here.
admin.site.register([Rol, Permiso])
