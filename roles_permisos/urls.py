from django.urls import path
from . import views
from .views import crear_roles_view, listar_roles_view, editar_rol

urlpatterns = [
    path('crear/', crear_roles_view, name='crearrol'),
    path('listar/', listar_roles_view, name='listarrol'),
    path('editar/<str:nombre_rol>/', editar_rol, name='editarrol'),
]
