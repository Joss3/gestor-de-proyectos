from django.shortcuts import render
from .forms import CrearRolesForm
from django.shortcuts import redirect
from django.contrib.auth.models import Group
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth.models import User
from proyecto.models import Proyecto
from guardian.shortcuts import assign_perm
from .forms import FiltrarRol
from roles_permisos.models import Rol


def crear_roles_view(request):
    """
    Crea un objeto rol que contiene una lista de los permisos seleccionados por el usuario en el formulario

    :param request: Usuario que invoca a la funcion crear_roles_view
    :return: Formulario de creación de roles o Retorna al listado de proyectos
    """
    if request.session.is_empty() or request.user.is_superuser:
        return render(request, 'proyecto/error_inicio_session.html', {})
    else:
        if request.method == 'POST':

            form = CrearRolesForm(request.POST)

            if form.is_valid():

                form.save()

            return redirect('listarrol')

        else:
            form = CrearRolesForm()
            return render(request, 'usuario/rol.html', {"form": form})


def editar_rol(request, nombre_rol):
    """
    Permite editar un rol ya creado. Tanto el nombre del mismo como su lista de permisos

    :param request:
    :param nombre_rol: Nombre del rol que sera editado
    :return:
    """
    if request.session.is_empty() or request.user.is_superuser:
        return render(request, 'proyecto/error_inicio_session.html', {})
    else:
        rol = Rol.objects.get(nombre=nombre_rol)

        if request.method == 'POST':
            form = CrearRolesForm(request.POST, instance=rol)
            if form.is_valid():
                form.save()
            return redirect('listarrol')
        form_editar_rol = CrearRolesForm(None, instance=rol)
        return render(request, 'usuario/rol.html', {'form': form_editar_rol})


def listar_roles_view(request):
    """
    Lista con buscador de los roles del sistema

    :param request:
    :return:
    """
    if request.session.is_empty() or request.user.is_superuser:
        return render(request, 'proyecto/error_inicio_session.html', {})
    else:
        user_list = Rol.objects.all()
        user_filter = FiltrarRol(request.GET, queryset=user_list)
        return render(request, 'roles_permisos/listar_rol.html', {'filter': user_filter})


def search_rol_proyecto(request, proyecto_id, username):
    """
    Lista con buscador de roles de sistema. Se utiliza al momento de agregar nuevos miembros a un proyecto.
    Permite asignar un rol a un usuario dentro de un proyecto.

    :param request:
    :param proyecto_id: ID del proyecto al cual será agregado el usuario seleccionado
    :param username: Nombre del usuario
    :return:
    """
    if request.session.is_empty() or request.user.is_superuser:
        return render(request, 'proyecto/error_inicio_session.html', {})
    else:
        rol_list = Rol.objects.all().exclude(nombre='scrum_master')

        roles_libres = []

        for rol in rol_list:
            nombre_grupo = 'proyecto' + str(proyecto_id) + str(username) + str(rol.nombre)
            if not Group.objects.filter(name=nombre_grupo).exists():
                roles_libres.append(rol.id)

        rol_list = Rol.objects.filter(id__in=roles_libres)

        user_filter = FiltrarRol(request.GET, queryset=rol_list)
        return render(request, 'roles_permisos/asignar_rol_proyecto.html', {'filter': user_filter,
                                                                            'proyecto_id': proyecto_id,
                                                                            'usuario': username})


'''def search_rol_sprint(request, sprint_id, username):
    """
    Lista con buscador de usuarios del sistema. Se utiliza al momento de agregar nuevos miembros a un sprint.
    Permite asignar un rol a un usuario dentro de un sprint.
    :param request:
    :param sprint_id: ID del sprint al cual será agregado el usuario seleccionado
    :param username: Nombre del usuario
    :return:
    """
    if request.session.is_empty() or request.user.is_superuser:
        return render(request, 'proyecto/error_inicio_session.html', {})
    else:
        user_list = Rol.objects.all()
        user_filter = FiltrarRol(request.GET, queryset=user_list)
        return render(request, 'roles_permisos/asignar_rol_sprint.html', {'filter': user_filter,
                                                                          'sprint_id': sprint_id,
                                                                          'usuario': username})'''
