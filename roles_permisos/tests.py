from django.test import TestCase
from roles_permisos.forms import CrearRolesForm, FiltrarRol
from roles_permisos.models import Permiso, Rol
from django.contrib.auth.models import User
from django.test import Client

# Create your tests here.


class SetUpClass(TestCase):

    def setUp(self):
        self.user = User.objects.create_user(username='prueba', email='prueba@gmail.com', password='a1b2c3d4*')
        self.permiso1 = Permiso.objects.create(nombre='view_user', descripcion='Ver users')
        self.permiso2 = Permiso.objects.create(nombre='change_user', descripcion='Editar users')
        self.rol1 = Rol.objects.create(nombre='rol1', permisos=[self.permiso1])
        self.rol2 = Rol.objects.create(nombre='rol2', permisos=[self.permiso1, self.permiso2])


# class PermisoTestCase(TestCase):



# Form valido
'''
def test_CrearRolesForm_valido(self):
        form = CrearRolesForm(data={'nombre': "view_user", 'descripcion': {'view_user'}})
        self.assertTrue(form.is_valid())

    def test_CrearRolesForm_invalido(self):
        form = CrearRolesForm(data={'nombre': "", 'descripcion': {""}})
        self.assertTrue(form.is_valid())


class RolesViewTest(SetUpClass):

    def test_home_view(self):
        user_login = self.client.login(email="prueba@gmail.com", password="a1b2c3d4*")
        self.assertTrue(user_login)
        response = self.client.get("/")
        self.assertEqual(response.status_code, 302)


class User_Views_Test(SetUp_Class):

    def test_home_view(self):
        user_login = self.client.login(email="user@mp.com", password="user")
        self.assertTrue(user_login)
        response = self.client.get("/")
        self.assertEqual(response.status_code, 302)

    def test_add_user_view(self):
        response = self.client.get("include url for add user view")
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "include template name to render the response")

    # Invalid Data
    def test_add_user_invalidform_view(self):
        response = self.client.post("include url to post the data given", {'email': "admin@mp.com", 'password': "", 'first_name': "mp", 'phone': 12345678})
        self.assertTrue('"error": true' in response.content)

    # Valid Data
    def test_add_admin_form_view(self):
        user_count = User.objects.count()
        response = self.client.post("include url to post the data given", {'email': "user@mp.com", 'password': "user", 'first_name': "user"})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(User.objects.count(), user_count+1)
        self.assertTrue('"error": false' in response.content)'''

