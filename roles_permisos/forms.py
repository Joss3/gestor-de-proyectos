from django import forms
from roles_permisos.models import Rol, Permiso
import django_filters


class CrearRolesForm(forms.ModelForm):
    """
    Formulario de creacion de roles.
    Contiene un campo de nombre y una lista seleccionable de los permisos disponibles
    """

    PERM_SET = Permiso.objects.all()
    permisos = forms.ModelMultipleChoiceField(
        widget=forms.CheckboxSelectMultiple(attrs={'width': 30}), queryset=PERM_SET
    )

    class Meta:
        model = Rol
        fields = ('nombre', 'permisos')


class FiltrarRol(django_filters.FilterSet):
    """Campo y filtro para realizar búsquedas de roles"""

    nombre = django_filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = Rol
        fields = ['nombre']
