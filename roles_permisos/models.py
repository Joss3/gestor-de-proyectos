from django.db import models
from django.contrib.auth.models import Group
from proyecto.models import Proyecto


class Permiso(models.Model):

    nombre = models.CharField(max_length=30, unique=True)
    """Indica el nombre de un objeto Permiso especifico"""
    descripcion = models.CharField(max_length=60, null=True)
    """Titulo formal para el permiso, se utiliza como identificador en los formularios"""

    def __str__(self):
        return f' {self.descripcion}'


class Rol(models.Model):

    nombre = models.CharField(max_length=100, null=False, unique=True)
    """nombre del modelo, debe ser único"""
    permisos = models.ManyToManyField('Permiso')
    """lista de permisos que contiene el rol"""

    def __str__(self):
        return f' {self.nombre}'



