from django.apps import AppConfig


class RolesPermisosConfig(AppConfig):
    name = 'roles_permisos'
