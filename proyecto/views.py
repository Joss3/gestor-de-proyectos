import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.models import User, Group
from django.http import HttpResponseRedirect
from django.shortcuts import redirect, get_object_or_404, render
from django.urls import reverse
from guardian.shortcuts import assign_perm

from .models import Proyecto
from .forms import EditarProyectoForm, ProyectoForm, AddMiembroForm, FiltrarUsuarioProyecto, CancelarProyectoForm, RechazarUserStory
from actividad.models import Actividad
from flujo.models import Flujo, Fase
from product_backlog.models import ProductBacklog
from roles_permisos.models import Rol
from sprint.models import Sprint, TrabajoDiario
from sprint_backlog.models import SprintBacklog
from user_story.models import UserStory
from usuarios.models import Usuario
from user_story.models import UserStory
from product_backlog.models import ProductBacklog

from django.core.mail import send_mail
from gestorProyectos.email_info import EMAIL_HOST_USER

from .render import Render
from django.views.generic import View
"""
Constantes:
PENDIENTE: Valor que indica el estado inicial de un proyecto. 
SCRUM: Indica el nombre del rol del cual se copiaran los permisos para asignarle al usuario seleccionado como scrum master
"""
PENDIENTE = 'P'
ACTIVO = 'A'
FINALIZADO = 'F'
CANCELADO = 'C'
PAUSADO = 'S'

# Constantes de estado de un US
NO_INICIADO ='N'
EN_PROCESO = 'P'

SCRUM = 'scrum_master'


def editar_proyecto(request, proyecto_id):
    """
    Recibe un solicitud de la pagina de editar proyectos y el
    codigo del proyecto del cual se quiere editar, retorna el template
    de la pagina solicitada y cuando se finalice la edicion guardan los
    nuevos datos del proyecto editado en la base de datos
    """
    user = request.user
    usuario = Usuario.objects.get(user=user)
    objeto_permiso = User.objects.get(username=user).get_all_permissions() # tendra roles predeterminados
    proy = get_object_or_404(Proyecto, pk=proyecto_id)

    if request.session.is_empty() or request.user.is_superuser:
        return render(request, 'proyecto/error_inicio_session.html', {})
    elif not proy.is_scrum(usuario):
        return redirect('ver_proyecto', proyecto_id=proyecto_id)
    else:
        scrum = proy.scrum
        if request.method == 'POST':
            form = EditarProyectoForm(request.POST, instance=proy, scrum=scrum)
            if form.is_valid():
                proy = form.save(commit=False)
                busca_scrum = proy.usuarios.all().filter(user=scrum.user)
                if busca_scrum:
                    proy_sin_scrum = list(proy.usuarios.all())
                    proy_sin_scrum.remove(scrum)
                    proy.usuarios.set(proy_sin_scrum)
                proy.save()
                form.save_m2m()
                return redirect('ver_proyecto', proyecto_id)
            return HttpResponseRedirect(reverse('ver_proyecto', kwargs={'proyecto_id': proyecto_id}))
        form_editar_proyecto = EditarProyectoForm(None, scrum=scrum, instance=proy)
        return render(request, 'proyecto/editar_proyecto.html', {'form_editar': form_editar_proyecto, 'proy': proy,
                                                                 'objeto_permiso': objeto_permiso, 'usuario_actual': request.user})


def ver_proyecto(request, proyecto_id):
    """
    Recibe un solicitud de la pagina de ver proyectos y el
    codigo del proyecto del cual se quiere ver, retorna el template
    de la pagina ver proyecto del proyecto solicitado pasandole
    al template el proyecto solicitado.
    Muestra todos los detalles del proyecto, permitiendo ver el Product Backlog con la prioridad y el estado
    de cada User Story en el proyecto.
    Al Scrum Master le muestra la lista de User Stories pendientes de verificar para aceptar o rechazar.

    :param proyecto_id El codigo del proyecto que se desea ver
    """

    from cliente.models import Cliente
    request.session['atras_us'] = 1
    request.session['atras_sprint'] = 1
    user = request.user
    if request.session.is_empty() or request.user.is_superuser:
        return render(request, 'proyecto/error_inicio_session.html', {})
    else:
        proy = get_object_or_404(Proyecto, pk=proyecto_id)
        usuario_actual = Usuario.objects.get(user=user)

        if Cliente.objects.filter(proyecto_id__exact=proyecto_id).exists():
            cliente = Cliente.objects.get(proyecto_id__exact=proyecto_id)
        else:
            cliente = []

        usuarios = []
        nombre_rol = 'scrum' + str(proy.nombre) + str(user.username)
        user_stories = ProductBacklog.objects.get(proyecto_id__exact=proyecto_id).userstory_set.all().order_by("-prioridad_final").order_by("-iniciado")
        user_stories_no_terminados = ProductBacklog.objects.get(proyecto_id__exact=proyecto_id).userstory_set.filter(iniciado=True).filter(estado_proyecto='N').order_by("-prioridad_final")
        user_stories_no_iniciados = ProductBacklog.objects.get(proyecto_id__exact=proyecto_id).userstory_set.filter(
            iniciado=False).filter(estado_proyecto='N').order_by("-prioridad_final")
        user_stories_terminados_cancelados = ProductBacklog.objects.get(proyecto_id__exact=proyecto_id).userstory_set.exclude(estado_proyecto='N').order_by("-prioridad_final")
        page = request.GET.get('page', 1)
        paginator = Paginator(user_stories, 5)
        sprint_id = proy.sprint_activo
        us_pendientes = UserStory.objects.filter(fin=True, sprint_backlog__sprint_id=sprint_id, estado_proyecto='P')
        try:
            user_stories = paginator.page(page)
        except PageNotAnInteger:
            user_stories = paginator.page(1)
        except EmptyPage:
            user_stories = paginator.page(paginator.num_pages)

        for users in proy.usuarios.all():
            usuarios.append(users)

        alert = 0
        #No hay sprint
        if proy.sprint_activo == -1:
            alert = '1'
            proy.set_sprint(0)
        #Hay mas de 1 sprint pendiente
        elif proy.sprint_activo == -2:
            alert = '2'
            proy.set_sprint(0)
        kanban = False
        us_kanban = UserStory.objects.filter(product_backlog=proy.productbacklog_set.all()[0]).exclude(estado_proyecto=CANCELADO).exclude(estado_proyecto=NO_INICIADO)
        if not us_kanban.count() == 0:
            kanban = True

        sprint = Sprint.objects.filter(proyecto_id=proyecto_id).filter(estado=ACTIVO)
        sprint = Sprint.objects.filter(proyecto_id=proyecto_id).filter(estado=PENDIENTE).union(sprint)
        us_libres = 0
        if sprint:
            sprint = sprint[0]
            us_sprint = SprintBacklog.objects.get(sprint_id=sprint.id).userstory_set.all()
            for us in us_sprint:
                if not us.usuario_asignado:
                    us_libres += 1
        else:
            sprint = None

        return render(request, 'proyecto/ver_proyecto.html', {'proy': proy,
                                                              "usuarios": usuarios,
                                                              'usuario_actual': usuario_actual,
                                                              'user_stories': user_stories,
                                                              'user_stories_no_iniciados': user_stories_no_iniciados,
                                                              'user_stories_no_terminados': user_stories_no_terminados,
                                                              'user_stories_terminados_cancelados': user_stories_terminados_cancelados,
                                                              'cliente': cliente,
                                                              'us_pendientes': us_pendientes,
                                                              'alert': alert, 'kanban': kanban,
                                                              'sprint': sprint, 'us_libres': us_libres})


def proyecto_new(request):
    """
    Crea una nueva instancia de proyecto, luego asigna los permisos del rol scrum_master al
    usuario designado con ese rol en el nuevo proyecto e instancia un Product Backlog para el
    Proyecto.
    """
    if request.session.is_empty() or request.user.is_superuser:
        return render(request, 'proyecto/error_inicio_session.html', {})
    else:
        usuario = request.user
        objeto_permiso = User.objects.get(username=usuario).get_all_permissions()  # ????????????????????????????????
        if request.method == "POST":
            print(request.POST)
            if "cancel" in request.POST:
                return redirect('index')

            form1 = ProyectoForm(request.POST)
            if form1.is_valid():
                pro = form1.save(commit=False)
                pro.estado = PENDIENTE

                nombre = pro.nombre

                pro.save()
                form1.save()

                # Asignación de permisos de scrum master

                proyecto = Proyecto.objects.get(nombre=nombre)

                usuario = proyecto.scrum

                # Se trae el rol scrum_master
                rol = Rol.objects.get(nombre=SCRUM)

                # Se crea un nuevo grupo con un nombre determinado por el usuario y el proyecto
                nombre_rol = 'scrum' + str(proyecto.id) + str(usuario.user.username)

                try:
                    nuevogrupo = Group.objects.create(name=nombre_rol)

                    # Asignacion de permisos

                    permisos = rol.permisos.all()

                    for perm in permisos:
                        assign_perm(perm.nombre, nuevogrupo, proyecto)

                    usuario.user.groups.add(nuevogrupo)

                    proyecto.usuarios.add(usuario)


                except:
                    print("Error al asignar los permisos")
                    Group.objects.get(name=nombre_rol).delete()

                # Creacion y asignacion de Product Backlog
                ProductBacklog.objects.create(proyecto_id=proyecto.id)

                return redirect('index')
        else:
            form1 = ProyectoForm()
        return render(request, 'proyecto/proyecto_edit.html', {'form': form1, 'objeto_permiso': objeto_permiso})


def search_proy_miembro(request, proyecto_id):
    """
    Buscador de usuarios. Permite buscar un usuario para agregarlo como miembro de un proyecto.

    :param proyecto_id: ID del proyecto al cual se quiere agregar un nuevo miembro
    """
    if request.session.is_empty() or request.user.is_superuser:
        return render(request, 'proyecto/error_inicio_session.html', {})
    else:
        user_list = User.objects.all().exclude(username=request.user.username).exclude(username='AnonymousUser').exclude(is_superuser=True)
        cant_rol = Rol.objects.count() - 1  # no se cuenta rol scrum_master
        roles = Rol.objects.all().exclude(nombre='scrum_master')

        usuarios = []

        for user in user_list:
            cant = 0
            for rol in roles:
                nombre_grupo = 'proyecto' + str(proyecto_id) + str(user.username) + str(rol.nombre)
                if Group.objects.filter(name=nombre_grupo).exists():
                    cant += 1

            if cant < cant_rol:
                usuarios.append(user.id)

        user_list = User.objects.filter(id__in=usuarios)

        user_filter = FiltrarUsuarioProyecto(request.GET, queryset=user_list)
        return render(request, 'proyecto/miembros.html', {'filter': user_filter,
                                                          'proyecto_id': proyecto_id})


def agregar_miembro_proyecto(request, proyecto_id, username, rolname):
    """
    Agrega un usuario a un proyecto. Asigna al usuario los permisos que se encuentran dentro de un rol especifico.
    Los permisos se aplican sobre una instancia específica de proyecto.

    :param request:
    :param proyecto_id: ID del proyecto al cual será agregado el usuario
    :param username: username del Usuario que será agregado al proyecto
    :param rolname: nombre del rol que se le designa al usuario dentro del proyecto
    """

    if request.session.is_empty() or request.user.is_superuser:
        return render(request, 'proyecto/error_inicio_session.html', {})
    else:
        proyecto = Proyecto.objects.get(id=proyecto_id)
        user = User.objects.get(username=username)
        usuario = Usuario.objects.get(user=user)
        rol = Rol.objects.get(nombre=rolname)

        # Se crea un nuevo grupo con un nombre determinado por los nombres del proyecto, del usuario y del rol
        nombre_rol = 'proyecto' + str(proyecto_id) + str(username) + str(rolname)
        nuevogrupo = Group.objects.create(name=nombre_rol)

        # Asignacion de permisos

        permisos = rol.permisos.all()

        for perm in permisos:
            assign_perm(perm.nombre, nuevogrupo, proyecto)

        user.groups.add(nuevogrupo)

        proyecto.usuarios.add(usuario)

        # Asignar horas_laborales
        # horas = proyecto.duracion*8  # 8 horas por dia

        # usuario.add_horas_laborales(horas)

        return redirect('ver_proyecto', proyecto_id)


def cambiar_estado_proyecto(request, proyecto_id, estado):
    """
    Cambia el estado de un proyecto. Alterna entre los estados Activo y Pausado

    :param proyecto_id: ID del proyecto al cual se le quiere cambiar el estado
    :param estado: Estado que le será asignado al proyecto
    """

    if request.session.is_empty() or request.user.is_superuser:
        return render(request, 'proyecto/error_inicio_session.html', {})
    else:
        proyecto = Proyecto.objects.get(id=proyecto_id)

        # Hay algun sprint activo
        # Verificar si el sprint tiene us sin asignar

        '''uss = SprintBacklog.objects.get(sprint_id=sprint.id).userstory_set.all()

        us_libres = 0
        for us in uss:
            if not us.usuario_asignado:
                us_libres += 1'''

        if (not proyecto.sprint_activo == 0 or estado == CANCELADO) and not estado == PAUSADO:
            sprint = Sprint.objects.get(pk=proyecto.sprint_activo)

            sprint.set_estado(estado)
            us_sprint = UserStory.objects.filter(sprint_backlog=sprint.sprintbacklog_set.all()[0])
            if estado == ACTIVO:
                for us in us_sprint:
                    us.estado_proyecto = EN_PROCESO
                    us.save()
            elif estado == PAUSADO:
                for us in us_sprint:
                    us.estado_proyecto = NO_INICIADO
                    us.save()
            elif estado == CANCELADO:
                for us in us_sprint:
                    us.estado_proyecto = CANCELADO
                    us.save()
            if proyecto.estado == PENDIENTE:
                proyecto.fecha_inicio = datetime.date.today()
            proyecto.set_estado(estado)

        # No hay un sprint activo
        elif proyecto.sprint_activo == 0:
            # No hay sprints
            if proyecto.cant_sprints == 0:
                proyecto.set_sprint(-1)  # No hay sprints
            # Hay un sprint
            elif proyecto.cant_sprints == 1:
                sprint = Sprint.objects.all().filter(proyecto_id=proyecto_id).filter(estado__exact=PENDIENTE)
                if len(sprint) == 1:
                    return redirect('cambiar_estado_sprint', sprint[0].id, 'A')

            else:
                proyecto.set_sprint(-2)  # Hay mas de un sprint pendiente

        return redirect('ver_proyecto', proyecto_id)


def cancelar_proyecto(request, proyecto_id):
    """
    Cancela un proyecto pidiendo una justificacion.

    :param proyecto_id: ID del proyecto que se quiere cancelar
    """
    proyecto = Proyecto.objects.get(id=proyecto_id)
    if request.method == 'POST':
        form = CancelarProyectoForm(request.POST, instance=proyecto)
        if form.is_valid():
            proyecto = form.save(commit=False)

            hora = (proyecto.fecha_fin - proyecto.fecha_inicio).days
            hora /= 24
            hora = int(hora)

            miembros = proyecto.usuarios.all()

            for usuario in miembros:
                if not usuario.id == proyecto.scrum_id:
                    aux = int(hora) - usuario.horas_laborales
                    usuario.add_horas_laborales(aux)

            proyecto.estado = 'C'
            proyecto.save()
            sprints = Sprint.objects.filter(proyecto=proyecto)
            for sprint in sprints:
                if sprint.is_activo() or sprint.is_pendiente():
                    sprint.set_estado(CANCELADO)
                us_sprint = UserStory.objects.filter(sprint_backlog=sprint.sprintbacklog_set.all()[0])
                for us in us_sprint:
                    us.estado_proyecto = CANCELADO
                    us.save()
            return redirect('ver_proyecto', proyecto_id)
    form = CancelarProyectoForm(instance=proyecto)
    return render(request, 'proyecto/editar_proyecto.html', {'proy': proyecto,
                                                             'form_editar': form,
                                                             'cancelar': 1,
                                                             })


def aceptar_us(request, proyecto_id):
    """
    Sirve para verificar si un user story que haya finalizado el flujo sea aceptado o rechazado por el scrum
    si es aceptado, el estado del user story pasa Finalizado, si rechaza debe indicar en que fase volvera a colocar
    el user story.

    :param request: solicitud HTTP
    :param proyecto_id: id del proyecto al que pertenece el scrum
    :return: redirecciona ver_proyecto
    """

    if request.GET.get('aceptar'):
        us = UserStory.objects.get(pk=request.GET.get('aceptar'))
        us.estado_proyecto = 'F'
        usuario = Usuario.objects.get(user=request.user)
        Actividad.objects.create(user_story=us, descripcion="El User Story fue aceptado",
                                 fecha_cargado=datetime.date.today(), trabajo=0, autor=usuario)
        us.save()
        email_destino = us.usuario_asignado.user.email
        send_mail(f'User Story {us.nombre} del proyecto {us.tipo.proyecto.nombre} ha sido aceptado',
                  f'El User story {us.nombre} ha cumplido con todos los criterios de evaluación',
                  EMAIL_HOST_USER,
                  [email_destino],
                  fail_silently=True,
                  )
    return redirect('ver_proyecto', proyecto_id)


def elegir_fase(request, proyecto_id, pk):
    """
    Esta vista muestra las fases dentro del flujo al cual pertenece el user story y permite elegir en cual fase se
    colocará el user story para corregir el error o agregarle lo que le falta si esta incompleto

    :param request: solicitud HTTP
    :param proyecto_id: id del proyecto al que pertenece el scrum
    :param pk: id del user story selecccionado
    :return: ver_proyecto
    """
    us = UserStory.objects.get(pk=pk)
    if request.GET.get('elige_fase'):
        us.fase = Fase.objects.get(id=int(request.GET.get('elige_fase')))
        us.estado_fase = 1
        us.fin = False
        us.save()
    return redirect('rechazar_us', proyecto_id, pk)


def rechazar_us(request, proyecto_id, us_id):
    """
    Rechaza un User Story que ya llego al ultimo estado de la ultima fase de su flujo,
    teniendo que justificar ese rechazo

    :param proyecto_id: ID del proyecto al cual pertenece el User Story
    :param us_id: ID del User Story que se quiere rechazar
    """
    proyecto = Proyecto.objects.get(id=proyecto_id)
    us = UserStory.objects.get(id=us_id)
    usuario = Usuario.objects.get(user=request.user)
    if request.method == 'POST':
        form = RechazarUserStory(request.POST,us_id=us_id)
        if form.is_valid():
            act = form.save(commit=False)
            act.user_story = us
            act.fecha_cargado = datetime.date.today()
            act.trabajo = 0
            act.autor = usuario
            motivo_rechazo = act.descripcion
            act.descripcion = "User Story Rechazado: " + act.descripcion
            #modificar fase
            us.fase = Fase.objects.get(id=form.cleaned_data['fase'].id)
            us.estado_fase = 1
            us.fin = False
            us.save()
            act.save()
            #enviar correo al desarrollador con motivo y fase
            email_destino = us.usuario_asignado.user.email
            send_mail(f'User Story {us.nombre} del proyecto {proyecto.nombre} ha sido rechazado',
                      'Motivo: '+ motivo_rechazo +'\nSe ha movido a la Fase: ' + us.fase.nombre,
                      EMAIL_HOST_USER,
                      [email_destino],
                      fail_silently=True,
                      )
            return redirect('ver_proyecto', proyecto_id)
    form = RechazarUserStory(us_id=us_id)
    return render(request, 'proyecto/justificar_rechazo_us.html', {'proyecto_id': proyecto_id,
                                                                   'us': us,
                                                                   'form': form,
                                                                    })

def remover_miembro_proyecto(request, proyecto_id, username):
    """
    Remueve un usuario de un proyecto, eliminando el rol que este tiene dentro del mismo.
    Los permisos se aplican sobre una instancia específica de proyecto.
    :param request:
    :param proyecto_id: ID del proyecto al cual será agregado el usuario
    :param username: username del Usuario que será agregado al proyecto
    :param rolname: nombre del rol que se le designa al usuario dentro del proyecto
    """

    if request.session.is_empty() or request.user.is_superuser:
        return render(request, 'proyecto/error_inicio_session.html', {})
    else:
        proyecto = Proyecto.objects.get(id=proyecto_id)
        user = User.objects.get(username=username)
        usuario = Usuario.objects.get(user=user)

        nombre_grupo = 'proyecto' + str(proyecto_id) + str(username)
        grupo = Group.objects.filter(name__startswith=nombre_grupo)

        for g in grupo:
            if g:
                user.groups.remove(g)  # quitar grupo de los grupos del usuario
                g.delete()  # borrar objeto de grupo

        proyecto.usuarios.remove(usuario)

        return redirect('ver_proyecto', proyecto_id)


def reporte_Product_Backlog(request, proyecto_id):
    """Vista de reporte de Produck Backlog en el cual, se filtra los user story
    que pertencen al mismo proyecto"""
    pro = Proyecto.objects.filter(id=proyecto_id)
    pb = ProductBacklog.objects.filter(proyecto=pro[0])
    us = UserStory.objects.filter(product_backlog=pb[0])

    return Render.render('proyecto/reporte_PB.html', {'us': us})

def reporte_Horas_Trabajadas(request, proyecto_id):
    """Vista de reporte de las horas trabajadas por User Story, User y Sprint
    Se trae todos los user stories que estan dentro del proyecto y luego todos los usuarios del proyecto exepto al admin
    Luego se calcula horas trabajadas por todos los usuarios dependiendo de los user stories asignados a el"""
    pro=Proyecto.objects.get(id=proyecto_id)
    sprint = Sprint.objects.filter(proyecto=pro)
    pb=ProductBacklog.objects.filter(proyecto=pro)
    us=UserStory.objects.filter(product_backlog=pb[0])
    usuarios=pro.usuarios.filter(tipo='US')
    lista_horas_usuarios=[]
    lista_horas_sprint=[]
    total=0
    for sp in sprint:
        print(sp.nombre)
        lista_horas=TrabajoDiario.objects.filter(sprint=sp)
        for horas in lista_horas:
            total=total+horas.trabajo_realizado
        print(total)
        lista_horas_sprint.append(total)
        total=0
    for user in usuarios:
        for u in us:
            if u.usuario_asignado==user:
                total=total+u.trabajo_realizado
        lista_horas_usuarios.append(total)
        total=0

    return Render.render('proyecto/reporte_horas.html', {'sprint': sprint,
                                                 'us': us,
                                                 'usuarios': usuarios,
                                                 'lista_horas_usuarios': lista_horas_usuarios,
                                                 'lista_horas_sprint': lista_horas_sprint,
                                                'usuarios_total': zip(usuarios, lista_horas_usuarios),
                                                 'sprint_total': zip(sprint, lista_horas_sprint)})

def reporte_eficiencia(request, proyecto_id):
    pro=Proyecto.objects.get(id=proyecto_id)
    pb=ProductBacklog.objects.filter(proyecto=pro)
    us=UserStory.objects.filter(product_backlog=pb[0])
    usuarios=pro.usuarios.filter(tipo='US')
    lista=[]
    a=0
    for user in usuarios:
        u=us.filter(usuario_asignado=user)
        con=u.count()
        for n in u:
            if n.estado_proyecto=='F':
                a=a+1
        if con>0:
            total=(a/con)*100
            lista.append(total)
        else:
            lista.append(0)
        a=0

    return Render.render('proyecto/reporte_eficiencia.html', {'usuarios_eficiencia': zip(usuarios, lista)})






