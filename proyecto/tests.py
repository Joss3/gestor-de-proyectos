# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.urls import reverse
from django.test import TestCase, Client
from proyecto.models import Proyecto
from usuarios.models import Usuario
from django.contrib.auth.models import User
import datetime


class ProyectoTestCase(TestCase):
    def setUp(self):
        User.objects.create(username="Carlos_Mendez", email="carlosmendez@gmail.com")
        user = Usuario.objects.create(user=User.objects.get(username="Carlos_Mendez"), telefono="0981-234-567")
        Proyecto.objects.create(nombre="Proyecto 1900", fecha_inicio="2010-01-01", fecha_fin="2019-02-02", scrum=user)

    def test_nombre_proyecto(self):
        proy = Proyecto.objects.get(nombre="Proyecto 1900")
        self.assertEqual(proy.nombre, 'Proyecto 1900')

    def test_fecha_inicio_proyecto(self):
        proy = Proyecto.objects.get(nombre="Proyecto 1900")
        self.assertEqual(proy.fecha_inicio, datetime.date(2010, 1, 1))

    def test_fecha_fin_proyecto(self):
        proy = Proyecto.objects.get(nombre="Proyecto 1900")
        self.assertEqual(proy.fecha_fin, datetime.date(2019, 2, 2))

    def test_scrum_master(self):
        user = User.objects.get(username="Carlos_Mendez")
        scrum = Usuario.objects.get(user=user, telefono="0981-234-567")
        proy = Proyecto.objects.get(nombre="Proyecto 1900")
        self.assertEqual(proy.scrum, scrum)

    def test_carga_ver_proyecto_sin_login(self):
        proy = Proyecto.objects.get(nombre="Proyecto 1900")
        response = self.client.get(reverse('ver_proyecto', kwargs={'proyecto_id': proy.id}))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "No puede acceder a este sitio.")

    def test_crear_proyecto(self):
        proy = Proyecto.objects.get(nombre="Proyecto 1900")
        respuesta = self.client.get(reverse('ver_proyecto', kwargs={'proyecto_id': proy.id}))
        # print(respuesta)
        self.assertEqual(respuesta.status_code, 200)
