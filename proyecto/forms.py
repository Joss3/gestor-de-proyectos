import datetime
import django_filters
from django import forms
from django.conf import settings
from django.contrib.auth.models import User
from .models import Proyecto
from usuarios.models import Usuario
from roles_permisos.models import Rol
from actividad.models import Actividad
from flujo.models import Flujo, Fase
from user_story.models import UserStory
ESTADOS = (('A', 'Activo'), ('F', 'Finalizado'), ('C', 'Cancelado'), ('P', 'En Planeamiento'))


class DateInput(forms.DateInput):
    input_type = 'date'


class EditarProyectoForm(forms.ModelForm):
    """
        Formulario para editar un proyecto, teniendo como campos el nombre,
        la fecha de inicio, la fecha de finalizacion, el estado y los miembros
        que no son el scrum master
    """

    def __init__(self, *args, **kwargs):
        """
        Inicializa el formulario excluyendo al scrum master de la lista de
        miembros que se pueden agregar al proyecto
        :param kwargs: Atravez de args se pasa el objeto del scrum master
        """
        self.scrum = kwargs.pop('scrum')
        super(EditarProyectoForm, self).__init__(*args, **kwargs)  # populates the post

    class Meta:
        """
            Se define el formato y la estructura del formulario para editar proyecto
        """
        model = Proyecto
        fields = {'nombre', 'estado', 'sprint_activo'}
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'col-sm-12'}),
            'estado': forms.HiddenInput(),
            'usuarios': forms.CheckboxSelectMultiple(),
            'sprint_activo': forms.HiddenInput,
        }

    estado = forms.HiddenInput()
    field_order = ['nombre', 'estado']


class ProyectoForm(forms.ModelForm):
    """Se crea una lista de objetos de tipo Usuario y luego se selecciona un scrum master
        de esa lista de objetos"""
    """
    Formulario de creacion de proyectos.
    Se oculta el campo 'estado' pues se le asignara luego el estado inicial 'PENDIENTE'
    """

    queryset = Usuario.objects.all().exclude(user__is_superuser=True).exclude(user__username='AnonymousUser')
    queryset = queryset.order_by('user__first_name')

    scrum = forms.ModelChoiceField(queryset=queryset, widget=forms.RadioSelect(), empty_label=None)

    class Meta:
        """Se define los campos del formulario y el nombre de cada uno"""
        model = Proyecto
        fields = ('nombre', 'fecha_fin', 'estado', 'scrum', 'numero')
        widgets = {
            'estado': forms.HiddenInput(),
            'numero': forms.HiddenInput(),
        }

    def clean_fecha_fin(self):
        data = self.cleaned_data['fecha_fin']
        inicio = datetime.date.today()
        if data < inicio:
            raise forms.ValidationError('La fecha de fin debe ser mayor que la fecha de hoy')
        return data


class AddMiembroForm(forms.ModelForm):
    """
    Formulario de seleccion de miembros
    Permite agregar un usuario con un rol a un proyecto determinado
    """

    USER_SET = Usuario.objects.all().exclude(user__is_superuser=True).exclude(user__usuario__tipo='AD').exclude(user__username='AnonymousUser')
    ROL_SET = Rol.objects.all()

    usuario = forms.ModelMultipleChoiceField(queryset=USER_SET)
    roles = forms.ModelMultipleChoiceField(queryset=ROL_SET)


class FiltrarUsuarioProyecto(django_filters.FilterSet):
    """Campos de texto y fitros para realizar búsquedas de usuario"""
    username = django_filters.CharFilter(lookup_expr='icontains')
    first_name = django_filters.CharFilter(lookup_expr='icontains')
    last_name = django_filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name']


class CancelarProyectoForm(forms.ModelForm):
    """
        Formulario para cancelacion de un Proyecto, el unico
        campo que se tiene es la justificacion de la cancelacion
        del Proyecto a cancelar
    """
    class Meta:
        model = Proyecto
        fields = ['justificacion']
        widgets = {
            'justificacion': forms.Textarea(attrs={'rows': 2,'cols': 20}),
        }


class RechazarUserStory(forms.ModelForm):
    """
        Formulario para el rechazo de un User Story por el Scrum Master,
        esto genera un actividad de tiempo de trabajo cero y
        tiene solo el campo de descripcion para justificar el
        rechazo de este User Story
    """
    class Meta:
        model = Actividad
        fields = ['descripcion']

        widgets = {
            'descripcion': forms.Textarea(attrs={'rows': 2,'cols': 20}),
        }

        labels = {
            "descripcion": "Motivo del Rechazo",
        }

    def __init__(self, *args, **kwargs):
        """
        Con init podemos añadir un nuevo campo dinamicamente al formulario con un dato que recibe de la vista
        :param args: argumentos pasados al constructor
        :param kwargs: se le pasan datos tipo clave : valor como un diccionario, en el recibe un parametro
        """
        id_us = kwargs.pop('us_id', 0)
        super(RechazarUserStory, self).__init__(*args, **kwargs)

        us = UserStory.objects.get(id=id_us)
        fases = Fase.objects.filter(id_flujo=us.fase.id_flujo)

        self.fields['fase'] = forms.ModelChoiceField(queryset=fases,
                                                               widget=forms.RadioSelect(), empty_label=None)

        for field_name, field in self.fields.items():
            if field_name == 'descripcion':
                field.widget.attrs['class'] = 'form-control'

