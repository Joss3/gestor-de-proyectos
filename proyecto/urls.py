from django.urls import path, include
from proyecto import views
from roles_permisos.views import search_rol_proyecto
urlpatterns = [
    path('<int:proyecto_id>/', views.ver_proyecto, name='ver_proyecto'),
    path('<int:proyecto_id>/editar/', views.editar_proyecto, name='editar_proyecto'),
    path('crear/', views.proyecto_new, name='proyecto_new'),
    path('<int:proyecto_id>/user-stories/', include('user_story.urls'), name='user_story'),
    path('<int:proyecto_id>/search/', views.search_proy_miembro, name='search_proy'),
    path('<int:proyecto_id>/<str:username>/rol', search_rol_proyecto, name='search_rol_proyecto'),
    path('<int:proyecto_id>/<str:username>/<str:rolname>/rol', views.agregar_miembro_proyecto, name='agregar_miembro_proyecto'),
    path('<int:proyecto_id>/<str:username>/remover', views.remover_miembro_proyecto, name='remover_miembro_proyecto'),
    path('<int:proyecto_id>/<str:estado>/estado', views.cambiar_estado_proyecto, name='cambiar_estado_proyecto'),
    path('<int:proyecto_id>/cancelar/', views.cancelar_proyecto, name='cancelar_proyecto'),
    path('<int:proyecto_id>/tipo-us/', include('tipo_us.urls')),
    path('<int:proyecto_id>/kanban/', include('kanban.urls')),
    path('<int:proyecto_id>/aceptar/', views.aceptar_us, name='aceptar'),
    #path('<int:proyecto_id>/elegir_fase/<int:pk>/', views.elegir_fase, name='elegir_fase'),
    path('<int:proyecto_id>/rechazar_us/<int:us_id>/', views.rechazar_us, name='rechazar_us'),
    path('<int:proyecto_id>/reporte_PB/', views.reporte_Product_Backlog, name='reporte_Product_Backlog'),
    path('<int:proyecto_id>/reporte_horas/', views.reporte_Horas_Trabajadas, name='reporte_Horas_Trabajadas'),
    path('<int:proyecto_id>/reporte_eficiencia/', views.reporte_eficiencia, name='reporte_eficiencia'),
]
