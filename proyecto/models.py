from django.db import models
import datetime


class Proyecto(models.Model):

    ESTADO_CHOICES = (
        ('P', 'Pendiente'),
        ('S', 'Pausado'),
        ('A', 'Activo'),
        ('F', 'Finalizado'),
        ('C', 'Cancelado'),
    )
    """Lista de estados en los que se puede encontrar un proyecto"""

    nombre = models.CharField(max_length=100, unique=True)
    """El nombre del proyecto"""
    fecha_inicio = models.DateField(null=True, blank=True)
    """La fecha en que se crea el proyecto"""
    fecha_fin = models.DateField(null=True, help_text="Fecha estimada de finalizacion, en formato DD/MM/AAAA")
    """La fecha estimada de finalizacion del proyecto"""
    estado = models.CharField(max_length=1, choices=ESTADO_CHOICES, null=True, blank=True)
    """El estado actual del proyecto que puede ser Pendiente, Pausado, Activo, Finalizado y Cancelado"""
    usuarios = models.ManyToManyField('usuarios.Usuario', blank=True)
    """Los miembros que integran el proyecto"""
    scrum = models.ForeignKey('usuarios.Usuario', related_name='scrum', on_delete=models.SET_NULL, null=True, blank=False)
    """El usuario que fue asignado como Scrum Master del Proyecto"""
    numero = models.IntegerField(blank=True, default=0)  # Contador de sprints
    """Un contador de Sprints"""
    justificacion = models.CharField(max_length=1000, null=True, blank=True)
    """La justificacion que sirve para cuando se cancela el proyecto"""
    sprint_activo = models.IntegerField(null=False, default=0, blank=True)
    """El id el sprint en ejecucion, si no hay sprints ejecutandoce esta en cero"""
    cant_sprints = models.IntegerField(null=True, blank=True, default=0)
    """Cuantos Sprints actualmente hay en el Proyecto"""
    duracion = models.IntegerField(default=0, blank=True, null=True)
    """Cantidad de dias que durara el Proyecto calculando en base a las fechas de inicio y fin"""

    class Meta:
        """
        Se agregan permisos nuevos relacionados con los miembros de un proyecto específico
        """

        permissions = (
            ("asignar_miembro", "Asignar miembros a un proyecto"),
            ("ver_miembro", "Ver miembros de un proyecto"),
            ("remover_miembro", "Remover miembros de un proyecto"),
            ("agregar_cliente", "Agregar cliente al proyecto"),
            ("editar_cliente", "Editar cliente del proyecto"),
            ("crear_sprint", "Agregar sprints al proyecto"),
            ("editar_sprint", "Editar sprints del proyecto"),
            ("ver_sprint", "Ver sprints del proyecto"),
            ("asignar_miembro_sprint", "Asignar miembros al sprint"),
            ("ver_miembro_sprint", "Ver miembros de los sprints del proyecto"),
            ("importar_flujo_sprint", "Importar Flujo dentro de un sprint"),
            ("crear_us", "Crear User Story"),
            ("crear_tus", "Crear Tipo de User Story"),
            ("importar_tus", "Importar Tipo de User Story"),
        )
        """Lista de permisos relacionados con un proyecto especifico"""

    def __str__(self):
        return f' {self.nombre}'

    def is_scrum(self, usuario):
        """
        :param usuario: Objeto usuario
        :return: True si el usuario es scrum master, False en caso contrario
        Verifica si el usuario es scrum master dentro de un proyecto
        """
        if usuario == self.scrum:
            return True
        return False

    def set_estado(self, estado):
        """
        :param estado: Estado nuevo al cual se va a cambiar el proyectos
        Cambia el estado de un proyecto a un estado nuevo pasado como parámetro
        """
        self.estado = estado
        self.save()
        return

    def incrementar(self):
        """Incrementa el contador de sprints de un proyecto en una unidad"""
        self.numero += 1
        self.save()
        return

    def decrementar(self):
        """Decrementa el contador de sprints de un proyecto en una unidad"""
        self.numero -= 1
        self.save()
        return

    def set_sprint(self, sprint_id):
        """
        :param sprint_id: id del sprint que es iniciado
        Activa una bandera que indica que hay un sprint activo en un proyecto
        """
        self.sprint_activo = sprint_id
        self.save()
        return

    def is_finalizado(self):
        """
        :return: retorna True si el proyecto está finalizado, False en caso contrario
        Verifica si un proyecto está finalizado
        """
        if self.estado == 'F':
            return True
        return False

    def is_cancelado(self):
        """
        :return: retorna True si el proyecto está cancelado, False en caso contrario
        Verifica si un proyecto está cancelado
        """
        if self.estado == 'C':
            return True
        return False

    def is_activo(self):
        """
        :return: retorna True si el proyecto está activo, False en caso contrario
        Verifica si un proyecto está activo
        """
        if self.estado == 'A':
            return True
        return False

    def is_pausado(self):
        """
        :return: retorna True si el proyecto está pausado, False en caso contrario
        Verifica si un proyecto está pausado
        """
        if self.estado == 'S':
            return True
        return False

    def is_pendiente(self):
        """
        :return: retorna True si el proyecto está pendiente, False en caso contrario
        Verifica si un proyecto está pendiente
        """
        if self.estado == 'P':
            return True
        return False

    def set_null(self):
        """Desactiva la bandera de sprint activo dentro del proyecto"""
        self.sprint_activo = 0
        return

    def add_sprint(self):
        """Incrementa en una unidad la cantidad de sprints asignados al proyecto"""
        self.cant_sprints += 1
        self.save()
        return

    def del_sprint(self):
        """Decrementa en una unidad la cantidad de sprints asignados al proyecto"""
        self.cant_sprints -= 1
        self.save()
        return

    def set_duracion(self):
        """Establece la duración del proyecto basado en las fechas de inicio y fin del proyecto"""
        self.duracion = (self.fecha_fin-self.fecha_inicio).days
        self.save()
        return

    def set_fecha_inicio(self):
        """Asigna la fecha actual al campo de fecha de inicio del proeycto"""
        self.fecha_inicio = datetime.date.today()
        return
