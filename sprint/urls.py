from django.urls import path

from sprint.views import reporte_Sprint_Backlog
from .views import crear_sprint, search_sprint_miembro, agregar_miembro_sprint, ver_sprint, listar_sprints, get_data
from .views import cambiar_estado_sprint, editar_sprint, cancelar_sprint, finalizar_sprint, burndown_chart_sprint
from .views import remover_miembro_sprint, ver_reporte_us_pendientes
from user_story.views import listar_us, listar_us_sprint, listar_usuario_sprint, listar_us_usuario, desasignar_usuario_us
from user_story.views import asignar_usuario_us, listar_usuario_us
from sprint_backlog.views import cargar_us, quitar_us, reasignar_horas_a_trabajar
from sprint import views

urlpatterns = [
    path('<int:sprint_id>/', ver_sprint, name='ver_sprint'),
    path('<int:proyecto_id>/crear/', crear_sprint, name='crear_sprint'),
    path('<int:proyecto_id>/<int:sprint_id>/search/', search_sprint_miembro, name='search_sprint_miembro'),
    path('<int:proyecto_id>/listar/', listar_sprints, name='listar_sprints'),
    path('<int:sprint_id>/<str:estado>/estado', cambiar_estado_sprint, name='cambiar_estado_sprint'),
    path('<int:sprint_id>/cancelar', cancelar_sprint, name='cancelar_sprint'),
    path('<int:sprint_id>/finalizar', finalizar_sprint, name='finalizar_sprint'),
    path('<int:sprint_id>/editar/', editar_sprint, name='editar_sprint'),
    path('<int:sprint_id>/agregar_us/', listar_us, name='listar_us'),
    path('<int:sprint_id>/agregar_us/<int:us_id>/', cargar_us, name='cargar_us'),
    path('<int:sprint_id>/<str:username>/agregar_miembro/', agregar_miembro_sprint, name='agregar_miembro_sprint'),
    path('<int:sprint_id>/<str:username>/remover_miembro/', remover_miembro_sprint, name='remover_miembro_sprint'),

    path('<int:sprint_id>/listar_us_us/', listar_us_sprint, name='listar_us_sprint'),
    path('<int:sprint_id>/<int:us_id>/listar_usuario_us/', listar_us_usuario, name='listar_us_usuario'),

    path('<int:sprint_id>/<int:us_id>/quitar_us/', quitar_us, name='quitar_us'),

    path('<int:sprint_id>/listar_us_usuario/', listar_usuario_sprint, name='listar_usuario_sprint'),
    path('<int:sprint_id>/<str:username>/listar_us', listar_usuario_us, name='listar_usuario_us'),

    path('<int:sprint_id>/<str:username>/<int:us_id>/asignar_us', asignar_usuario_us, name='asignar_usuario_us'),
    path('<int:sprint_id>/<str:username>/<int:us_id>/desasignar_us/<int:quitar_sprint>', desasignar_usuario_us,
                                                                                         name='desasignar_usuario_us'),

    path('<int:sprint_id>/burndown_chart', burndown_chart_sprint, name='burndownChartSprint'),
    path('<int:sprint_id>/datos', get_data, name='get_data'),

    path('<int:sprint_id>/<int:us_id>/reasignar-trabajo/', reasignar_horas_a_trabajar, name='reasignar_horas_a_trabajar'),
    path('<int:sprint_id>/reporte_SB', views.reporte_Sprint_Backlog, name='reporte_SB'),

    path('<int:sprint_id>/informe-us-pendientes/', ver_reporte_us_pendientes, name='ver_reporte_us_pendientes')
]
