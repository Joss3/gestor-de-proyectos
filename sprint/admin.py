from django.contrib import admin
from sprint.models import Sprint, TrabajoDiario

# Register your models here.
admin.site.register([Sprint, TrabajoDiario])
