from django import forms
from usuarios.models import Usuario
from django.conf import settings
from roles_permisos.models import Rol
from .models import Sprint
import django_filters
from django.contrib.auth.models import User

ESTADOS = (('A', 'Activo'), ('F', 'Finalizado'), ('C', 'Cancelado'), ('P', 'En Planeamiento'))


class DateInput(forms.DateInput):
    input_type = 'date'


class CrearSprintForm(forms.ModelForm):
    """Formulario de creación de sprints"""
    class Meta:
        model = Sprint
        fields = ('duracion',)


class FiltrarUsuarioSprint(django_filters.FilterSet):
    """Campos y filtros para realizar búsquedas de usuario"""
    user__username = django_filters.CharFilter(field_name='user__username', lookup_expr='icontains')
    user__first_name = django_filters.CharFilter(field_name='user__first_name', lookup_expr='icontains')
    user__last_name = django_filters.CharFilter(field_name='user__last_name', lookup_expr='icontains')

    class Meta:
        model = Usuario
        fields = ['user__username', 'user__first_name', 'user__last_name']


class CancelarSprintForm(forms.ModelForm):
    """
        Formulario para cancelacion de un Sprint, el unico
        campo que se tiene es la justificacion de la cancelacion
        del Sprint a cancelar
    """
    class Meta:
        model = Sprint
        fields = ['justificacion']
        widgets = {
            'justificacion': forms.Textarea(attrs={'rows': 2,'cols': 20}),
        }