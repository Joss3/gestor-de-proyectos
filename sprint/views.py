from django.core.mail import send_mail, get_connection, EmailMessage
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse

from gestorProyectos.email_info import EMAIL_HOST_USER
from .forms import CrearSprintForm
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.models import User
from django.shortcuts import redirect
from proyecto.models import Proyecto
from sprint.models import Sprint, TrabajoDiario
from .forms import FiltrarUsuarioSprint
from usuarios.models import Usuario
from sprint_backlog.models import SprintBacklog
from .forms import CancelarSprintForm
import datetime
from django.http import JsonResponse
from user_story.models import UserStory
from django.contrib import messages
from .render import Render

"""Estados de Sprint y Proyecto"""
PENDIENTE = 'P'
"""Sprint en estado pendiente"""
ACTIVO = 'A'
"""Sprint en estado activo"""
FINALIZADO = 'F'
"""Sprint en estado finalizado"""
CANCELADO = 'C'
"""Sprint en estado cancelado"""
PAUSADO = 'S'
"""Sprint en estado pausado"""


NO_INICIADO ='N'
"""US en estado no iniciado"""
EN_PROCESO = 'P'
"""US en estado en proceso"""


def crear_sprint(request, proyecto_id):
    """
    Crea una nueva instancia de sprint, luego asigna los permisos del rol scrum_master al
    usuario designado con ese rol en el nuevo sprint.

    :type proyecto_id: int
    :param proyecto_id: id del proyecto al cual pertenecerá el sprint

    :return: retorna a la página de lista de sprints
    """

    if request.session.is_empty() or request.user.is_superuser:
        return render(request, 'proyecto/error_inicio_session.html', {})
    else:
        if request.method == "POST":
            try:
                print(request.POST)
                if "cancel" in request.POST:
                    return redirect('index')

                form1 = CrearSprintForm(request.POST)
                if form1.is_valid():

                    proyecto = Proyecto.objects.get(id=proyecto_id)

                    sprint = form1.save(commit=False)
                    sprint.estado = PENDIENTE
                    sprint.proyecto = proyecto

                    proyecto.incrementar()

                    sprint_nombre = 'Sprint ' + str(proyecto.numero)

                    sprint.nombre = sprint_nombre

                    sprint.save()
                    form1.save()

                    sprint = Sprint.objects.get(nombre=sprint_nombre, proyecto_id__exact=proyecto_id)

                    sprint.miembros.add(proyecto.scrum)

                    # Sprint backlog

                    backlog = SprintBacklog.objects.create(sprint_id=sprint.id)

                    sprint.proyecto.add_sprint()

                    sprint.save()

                    return redirect('listar_sprints', proyecto_id)
            except:
                messages.add_message(request, messages.ERROR, 'Error al crear el Sprint')

        else:
            form1 = CrearSprintForm()
            return render(request, 'sprint/crearSprint.html', {'form': form1, 'titulo': "Nuevo Sprint", 'proyecto_id': proyecto_id})


def search_sprint_miembro(request, proyecto_id, sprint_id):
    """
    Muestra los miembros del sprint

    :type proyecto_id: int
    :param proyecto_id: id del Proyecto al cual pertenece el Sprint

    :type sprint_id: int
    :param sprint_id: id del sprint del cual se quiere ver los miembros
    """
    sprint = Sprint.objects.get(pk=sprint_id)
    if request.session.is_empty() or request.user.is_superuser:
        return render(request, 'proyecto/error_inicio_session.html', {})
    elif sprint.estado != 'P':
        return redirect('ver_sprint', sprint_id)
    else:
        proyecto = Proyecto.objects.get(id=proyecto_id)
        sprint = Sprint.objects.get(pk=sprint_id)

        sprint_miembros = sprint.miembros.all()

        user_list = proyecto.usuarios.all()

        usuarios_id = []

        for usuario in user_list:
            if usuario not in sprint_miembros:
                if not usuario.is_in_sprint():
                    usuarios_id.append(usuario.id)

        usuarios = Usuario.objects.filter(id__in=usuarios_id)

        user_filter = FiltrarUsuarioSprint(request.GET, queryset=usuarios)  # form
        return render(request, 'sprint/miembros_sprint.html', {'filter': user_filter, 'sprint_id': sprint_id})


def agregar_miembro_sprint(request, sprint_id, username):
    """
    Agrega un usuario a un sprint

    :type sprint_id: int
    :param sprint_id: sprint al cual será asignado el usuario

    :type username: string
    :param username: username del usuario que será el nuevo miembro del sprint
    :return: redirecciona a la vista del sprint, con el usuario ya asignado
    """

    if request.session.is_empty() or request.user.is_superuser:
        return render(request, 'proyecto/error_inicio_session.html', {})
    else:
        sprint = Sprint.objects.get(id=sprint_id)
        user = User.objects.get(username=username)

        usuario = Usuario.objects.get(user=user)

        if usuario.is_in_sprint():
            return redirect('ver_sprint', sprint_id)

        usuario.set_en_sprint(sprint.proyecto_id)
        sprint.miembros.add(usuario)

        horas = sprint.duracion*usuario.horas_laborales  # Horas que trabajara en total el programador en el sprint

        usuario.add_horas_disponibles(horas)
        sprint.add_capacidad(horas)

        return redirect('ver_sprint', sprint_id)


def remover_miembro_sprint(request, sprint_id, username):
    """
    Remueve un usuario de un sprint

    :type sprint_id: int
    :param sprint_id: sprint al cual será removido el usuario

    :type username: string
    :param username: username del usuario que será removido del sprint

    :return: redirecciona a la vista del sprint, con el usuario removido del sprint
    """

    if request.session.is_empty() or request.user.is_superuser:
        return render(request, 'proyecto/error_inicio_session.html', {})
    else:
        sprint = Sprint.objects.get(id=sprint_id)
        user = User.objects.get(username=username)

        usuario = Usuario.objects.get(user=user)

        sprint.miembros.remove(usuario)

        horas = sprint.duracion*usuario.horas_laborales  # Horas que trabajara en total el programador en el sprint
        sprint.quitar_capacidad(horas)

        usuario.reset_horas_disponibles()
        usuario.set_no_sprint()

        uss = UserStory.objects.filter(sprint_backlog__sprint=sprint).filter(usuario_asignado=usuario)
        for us in uss:
            us.desasignar_usuario()
            us.reset_estado_fase()

        return redirect('ver_sprint', sprint_id)


def ver_sprint(request, sprint_id):
    """
    Recibe un solicitud de la página de ver sprints y el
    codigo del sprint del cual se quieren ver los detalles.
    Retorna una pagina con informacion sobre el sprint, y ofrece algunas opciones de configuracion para el sprint

    :type sprint_id: int
    :param sprint_id: id del sprint que se desea ver
    """

    if request.session.is_empty():
        return render(request, 'proyecto/error_inicio_session.html', {})
    else:
        sprint = Sprint.objects.get(pk=sprint_id)
        request.session['atras_us'] = 3
        request.session['atras_us_sprint_id'] = sprint_id
        usuarios = []
        for users in sprint.miembros.all():
            usuarios.append(users.user)

        user_stories = SprintBacklog.objects.get(sprint_id=sprint.id).userstory_set.all()

        us_libres = 0
        for us in user_stories:
            if not us.usuario_asignado:
                us_libres += 1

        page = request.GET.get('page', 1)
        paginator = Paginator(user_stories, 5)
        try:
            user_stories = paginator.page(page)
        except PageNotAnInteger:
            user_stories = paginator.page(1)
        except EmptyPage:
            user_stories = paginator.page(paginator.num_pages)
        us_pendientes = UserStory.objects.filter(fin=True, sprint_backlog__sprint_id=sprint_id, estado_proyecto='P')
        return render(request, 'sprint/ver_sprint.html', {'sprint': sprint,
                                                          'usuarios': usuarios,
                                                          'user_stories': user_stories,
                                                          'us_libres': us_libres,
                                                          'saldo': sprint.capacidad-sprint.trabajo_por_realizar,
                                                          'us_pendientes': us_pendientes,
                                                          'usuario_actual': Usuario.objects.get(user=request.user)})


def editar_sprint(request, sprint_id):
    """
    Permite cambiar la duración de un sprint

    :type sprint_id: int
    :param sprint_id: id del sprint que será editado
    :return: retorna a la página de visualización del sprint, con la duración modificada
    """

    if request.session.is_empty() or request.user.is_superuser or not Sprint.objects.get(pk=sprint_id).is_pendiente():
        return render(request, 'proyecto/error_inicio_session.html', {})
    else:
        sprint = Sprint.objects.get(pk=sprint_id)
        if not request.method == 'POST':
            sprint.guardar_duracion()
        usuario = request.user
        objeto_permiso = User.objects.get(username=usuario).get_all_permissions()  # tendra roles predeterminados
        if request.method == 'POST':
            form = CrearSprintForm(request.POST, instance=sprint)
            if form.is_valid():
                sprint = form.save(commit=False)
                sprint.save()

                sprint = Sprint.objects.get(pk=sprint_id)

                dias = sprint.duracion - sprint.duracion_anterior

                capacidad = 0
                usuarios = sprint.miembros.all()

                for usuario in usuarios:
                    if not sprint.proyecto.scrum.user.username == usuario.user.username:
                        horas = dias*usuario.horas_laborales
                        usuario.add_horas_disponibles(horas)
                        capacidad += usuario.horas_laborales*sprint.duracion

                sprint.reset_capacidad()
                sprint.add_capacidad(capacidad)

                return redirect('ver_sprint', sprint_id)
            return HttpResponseRedirect(reverse('ver_sprint', kwargs={'sprint_id': sprint_id}))
        form_editar_sprint = CrearSprintForm(None, instance=sprint)
        return render(request, 'sprint/crearSprint.html', {'form': form_editar_sprint,
                                                           'titulo': 'Editar sprint',
                                                           'proyecto_id': sprint.proyecto_id,
                                                           'objeto_permiso': objeto_permiso,
                                                           'sprint_id': sprint_id})


def listar_sprints(request, proyecto_id):
    """
    Lista con buscador de proyectos.

    :type proyecto_id: int
    :param proyecto_id: id del proyecto
    """

    if request.session.is_empty():
        return render(request, 'proyecto/error_inicio_session.html', {})
    else:
        proyecto = Proyecto.objects.get(id=proyecto_id)
        atras = request.session['atras_sprint']
        sprint_queryset = Sprint.objects.all().filter(proyecto=proyecto).order_by('-id')

        return render(request, 'sprint/lista_sprint.html', {'sprints': sprint_queryset,
                                                            'proyecto_id': proyecto_id,
                                                            'proyecto': proyecto,
                                                            'proyecto_nombre': proyecto.nombre,
                                                            'atras': atras})


def cambiar_estado_sprint(request, sprint_id, estado):
    """
    Cambia el estado de un sprint, cambiando también el estado del proyecto de acuerdo al estado recibido como parámetro

    :type sprint_id: int
    :param sprint_id: id del sprint al cual se le cambiará el estado

    :type estado: string
    :param estado: nuevo estado del sprint
    :return: retorna a la página principal del sprint
    """

    sprint = Sprint.objects.get(id=sprint_id)

    if estado == ACTIVO:

        sprint = Sprint.objects.get(pk=sprint_id)
        num_miembros = sprint.miembros.count()
        uss = SprintBacklog.objects.get(sprint_id=sprint.id).userstory_set.all()

        us_libres = 0
        for us in uss:
            if not us.usuario_asignado:
                us_libres += 1

        if not us_libres == 0 or num_miembros <= 1:
            sprint.proyecto.set_estado(PAUSADO)
            return redirect('ver_sprint', sprint_id)
        else:
            if sprint.proyecto.sprint_activo == sprint_id or sprint.proyecto.sprint_activo == 0:
                sprint.proyecto.set_estado(ACTIVO)
                sprint.proyecto.set_fecha_inicio()
                us_sprint = UserStory.objects.filter(sprint_backlog=sprint.sprintbacklog_set.all()[0])
                for us in us_sprint:
                    us.estado_proyecto = EN_PROCESO
                    us.save()

                if sprint.is_pendiente():
                    sprint.set_fecha()
                    sprint.proyecto.set_sprint(sprint_id)
                    sprint.iniciar_dia_actual()

                    i = 0
                    while i < sprint.duracion:
                        TrabajoDiario.objects.create(sprint=sprint)
                        i += 1

                # enviar correo a los desarrolladores que inicio el sprint
                miembros = sprint.miembros.all()
                destinos = [miembro.user.email for miembro in miembros]
                send_mail(f'El Sprint {sprint.nombre} del proyecto {sprint.proyecto.nombre} ha sido iniciado',
                        f'Se ha iniciado el sprint\nCon los siguientes User Stories: {[us.nombre for us in us_sprint]} ',
                        EMAIL_HOST_USER,
                        destinos,
                        fail_silently=True,
                        )


            else:
                return redirect('ver_sprint', sprint_id)

    elif estado == PAUSADO and sprint.is_activo():
        sprint.proyecto.set_estado(PAUSADO)

    elif estado == FINALIZADO:
        return redirect('ver_sprint', sprint_id)

    sprint.set_estado(estado)

    return redirect('ver_sprint', sprint_id)


def cancelar_sprint(request, sprint_id):
    """
    Cancela un sprint. Se debe ingresar una justificacion para poder cancelar el sprint.
    Se le devuelven las horas que se tomaron de los usuarios para los US

    :type sprint_id: int
    :param sprint_id: id del sprint que se quiere cancelar

    :return: vuelve a la página de listar sprints
    """
    sprint = Sprint.objects.get(id=sprint_id)
    if request.method == 'POST':
        form = CancelarSprintForm(request.POST, instance=sprint)
        if form.is_valid():
            sprint = form.save(commit=False)
            sprint.proyecto.set_sprint(0)
            sprint.proyecto.set_estado(PAUSADO)
            sprint.proyecto.del_sprint()

            miembros = sprint.miembros.all()

            for usuario in miembros:
                usuario.set_no_sprint()
                usuario.reset_horas_disponibles()

            us_sprint = UserStory.objects.filter(sprint_backlog=sprint.sprintbacklog_set.all()[0])
            for us in us_sprint:
                us.reset_estado_fase()
                us.estado_proyecto = NO_INICIADO
                us.desasignar_usuario()
                us.reset_sprint_backlog()
                us.save()

            sprint.set_estado(CANCELADO)
            sprint.save()
            return redirect('listar_sprints', sprint.proyecto_id)
    form = CancelarSprintForm(instance=sprint)
    return render(request, 'sprint/crearSprint.html', {'form': form,
                                                       'titulo': "Cancelar sprint",
                                                       'proyecto_id': sprint.proyecto_id,
                                                       'sprint_id': sprint_id})


def finalizar_sprint(request, sprint_id):
    """
    Finaliza un sprint.

    :type sprint_id: int
    :param sprint_id: id del sprint que se quiere cancelar.

    :return: vuelve a la página de listar sprints
    """
    sprint = Sprint.objects.get(id=sprint_id)
    us_pendientes = UserStory.objects.filter(fin=True, sprint_backlog__sprint_id=sprint_id, estado_proyecto='P')
    if not us_pendientes:
        sprint.proyecto.set_sprint(0)
        sprint.proyecto.set_estado(PAUSADO)
        sprint.proyecto.del_sprint()

        miembros = sprint.miembros.all()

        for usuario in miembros:
            usuario.set_no_sprint()
            usuario.reset_horas_disponibles()

        us_sprint = UserStory.objects.filter(sprint_backlog=sprint.sprintbacklog_set.all()[0])
        for us in us_sprint:
            if us.estado_fase != 3 or us.fase.indice != us.flujo.cant_fases:
                us.reset_estado_fase()
                us.estado_proyecto = NO_INICIADO  # Marcar para que pase a primer lugar en el backlog
                us.reset_sprint_backlog()
                us.usuario_asignado = None
            else:
                us.estado_proyecto = 'F'
            us.save()
        sprint.set_estado(FINALIZADO)

    return redirect('listar_sprints', sprint.proyecto_id)


def burndown_chart_sprint(request, sprint_id):
    """
    Obtiene un sprint determinado y redirige a la página que contiene el burndown chart

    :type sprint_id: int
    :param sprint_id: id del sprint
    :return: redirige a la página con el burndown chart
    """

    if request.session.is_empty():
        return render(request, 'proyecto/error_inicio_session.html', {})
    else:
        sprint = Sprint.objects.get(pk=sprint_id)
        return render(request, 'sprint/burndownChartSprint.html', {"sprint": sprint})


def get_data(request, sprint_id):
    """
    Prepara los datos necesarios para generar el gráfico burndown chart

    :type sprint_id: int
    :param sprint_id: id del sprint del cual se obtendrán los datos para el gráfico

    :return: respuesta JSON
    """

    sprint = Sprint.objects.get(pk=sprint_id)

    sprint.set_dia_actual(int(sprint.get_cant_dias()) + 1)

    trabajos = TrabajoDiario.objects.all().filter(sprint=sprint).order_by('id')

    incremento = sprint.trabajo_por_realizar / sprint.duracion
    trabajo_total = sprint.trabajo_por_realizar

    for trabajo in trabajos:
        trabajo_total -= incremento
        trabajo.set_trabajo_esperado(round(trabajo_total))

    inicio = -1
    default_data = [sprint.trabajo_por_realizar]
    planificacion_data = [sprint.trabajo_por_realizar]
    labels = ['']
    for trabajo in trabajos:

        # se guarda el id del primer dia
        if inicio == -1:
            inicio = trabajo.id - 1

        numero_dia = trabajo.id - inicio
        labels.append("Día " + str(numero_dia))  # Contador de dias

        if numero_dia <= sprint.dia_actual:
            default_data.append(sprint.trabajo_por_realizar-trabajo.trabajo_realizado)

        planificacion_data.append(trabajo.trabajo_esperado)

    data = {
        "labels": labels,
        "default": default_data,
        "data": planificacion_data,
    }
    return JsonResponse(data)


def reporte_Sprint_Backlog(request, sprint_id):
    """Vista de reporte de Sprint Backlog en el cual, se filtra los sprint
        que pertencen al mismo proyecto"""
    sprint = Sprint.objects.filter(id=sprint_id)
    sp= SprintBacklog.objects.filter(sprint=sprint[0])
    us=UserStory.objects.filter(sprint_backlog=sp[0])
    return Render.render('sprint/reporte_SB.html', {'us': us})


def ver_reporte_us_pendientes(request, sprint_id):
    """
    Vista que permite visualizar los US con prioridad maxima en el sprint junto con su
    encargado y sus horas trabajadas
    :type sprint_id: int
    :param sprint_id: id del sprint que se quiere ver el informe
    :return: retorna a la página de visualización del informe de los US de sprint que quedaron pendientes en el Product Backlog
    """
    us_no_terminados = SprintBacklog.objects.get(sprint_id=sprint_id).userstory_set.filter(iniciado=True).order_by("-prioridad_final")
    return Render.render('sprint/informe_us_pendientes.html', {'us_no_terminados': us_no_terminados})