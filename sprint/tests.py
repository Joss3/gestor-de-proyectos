from django.test import TestCase
from django.test import TestCase, Client
from user_story.models import UserStory
from django.contrib.auth.models import User
from usuarios.models import Usuario
from proyecto.models import Proyecto
from sprint.models import Sprint
from sprint.forms import CrearSprintForm, CancelarSprintForm
from sprint_backlog.models import SprintBacklog
from tipo_us.models import Tipo_US, CampoPersonalizado, Tipo_Campo
from flujo.models import Flujo, Fase
from product_backlog.models import ProductBacklog
from user_story.forms import CrearUserStoryForm
import datetime


class SprintTestCase(TestCase):
    def setUp(self):
        user = User.objects.create(username='pedro', email='pedro@test.com')
        user.set_password('00000000')
        user.save()
        User.objects.create(username='pablo', email='pablo@test.com')
        User.objects.create(username='juan', email='juan@test.com')
        usuarios = Usuario.objects.all().exclude(user__username='AnonymousUser').exclude(user=user)
        proy = Proyecto.objects.create(nombre='Proyecto 1', fecha_inicio='2018-01-01', estado='P',
                                       scrum=Usuario.objects.get(user=user))
        proy.usuarios.set(usuarios)
        proy.save()

    def test_form_crear_sprint(self):
        c = Client()
        c.login(username='pedro', password='00000000')
        user = ['pablo', 'pedro']
        users = Usuario.objects.filter(user__username__in=user)
        proyecto = Proyecto.objects.get(nombre='Proyecto 1')
        sprint = Sprint.objects.create(duracion=5)
        sprint.estado = 'P'
        sprint.proyecto = proyecto
        proyecto.incrementar()
        sprint.nombre = 'Sprint ' + str(proyecto.numero)
        sprint.miembros.add(users[0])
        sprint.miembros.add(users[1])
        backlog = SprintBacklog.objects.create(sprint_id=sprint.id)
        sprint.proyecto.add_sprint()
        sprint.save()

        self.assertTrue(Sprint.objects.get(nombre='Sprint 1', proyecto=proyecto))

        self.client.logout()

    '''def test_iniciar_sprint_vacio(self):
        self.test_form_crear_sprint()
        c = Client()
        c.login(username='pedro', password='00000000')
        proy = Proyecto.objects.get(nombre='Proyecto 1')
        sprint = Sprint.objects.get(nombre='Sprint 1', proyecto=proy)
        response = self.client.post('sprint/' + str(sprint.id) + '/' + str('A') + '/estado', follow=True)

        self.assertEquals(sprint.estado, 'P', 'El sprint no se encuentra en estado pausado')
        self.assertEquals(sprint.fecha_inicio, None, 'Error en la fecha de inicio')
        self.assertEquals(sprint.fecha_fin, None, 'Error en la fecha fin')

    def test_iniciar_sprint_con_us(self):
        self.test_form_crear_sprint()
        c = Client()
        c.login(username='pedro', password='00000000')
        usuario = Usuario.objects.get(user__username='juan')
        proy = Proyecto.objects.get(nombre='Proyecto 1')
        sprint = Sprint.objects.get(nombre='Sprint 1', proyecto=proy)
        tipo = Tipo_US.objects.create(nombre='tipo', proyecto=proy)
        flujo = Flujo.objects.create(nombre='Flujo 1', tipo=tipo, cant_fases=1)
        fase = Fase.objects.create(nombre='Fase 1', id_flujo=flujo, indice=1)
        us = UserStory.objects.create(nombre='US1', descripcion='descripcion',
                                      criterios_aceptacion='criterios_aceptacion', trabajo_por_realizar=10,
                                      prioridad=10, valor_negocio=10, valor_tecnico=10, tipo=tipo,
                                      usuario_asignado=usuario)

        calculo_prioridad = (us.prioridad + 2 * us.valor_negocio + 2 * us.valor_tecnico) / 5
        us.set_prioridad_final(calculo_prioridad)
        us.reset_trabajo_realizado()
        us.set_flujo(flujo)
        us.reset_estado_fase()
        us.set_fase(fase)

        # Asignar US
        backlog = SprintBacklog.objects.get(sprint_id=sprint.id)
        us.set_sprint_backlog(backlog)

        response = self.client.post('sprint/' + str(sprint.id) + '/' + str('A') + '/estado', follow=True)

        self.assertEquals(sprint.estado, 'A', 'El sprint NO se encuentra en estado Activo')'''

    def test_form_cancelar_sprint(self):
        c = Client()
        c.login(username='pedro', password='00000000')
        proy = Proyecto.objects.get(nombre='Proyecto 1')
        data = {'justificacion': 'SE CANCELA PARA PROBAR'}
        form = CancelarSprintForm(data=data, instance=proy)
        self.assertTrue(form.is_valid())
        form.save()
        self.assertEqual(proy.justificacion, 'SE CANCELA PARA PROBAR')

        self.client.logout()
