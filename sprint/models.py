from django.db import models
import datetime
from datetime import timedelta
from usuarios.models import Usuario
from proyecto.models import Proyecto
from django.core.validators import MaxValueValidator, MinValueValidator


class Sprint(models.Model):

    ESTADOS_SPRINT = (
        ('P', 'Pendiente'),
        ('A', 'Activo'),
        ('S', 'Pausado'),
        ('F', 'Finalizado'),
        ('C', 'Cancelado'),
    )
    """
    Lista de estados de sprint
    """

    nombre = models.CharField(max_length=100, null=True, default='', blank=True)
    duracion = models.PositiveIntegerField(validators=[MinValueValidator(1)])
    fecha_inicio = models.DateField(null=True, blank=True)
    fecha_fin = models.DateField(null=True, blank=True)
    miembros = models.ManyToManyField('usuarios.Usuario', blank=True)
    estado = models.CharField(max_length=1, choices=ESTADOS_SPRINT, null=True, blank=True)
    proyecto = models.ForeignKey(Proyecto, on_delete=models.CASCADE, null=True, blank=True)
    justificacion = models.CharField(max_length=300, null=True, default='', blank=True)
    capacidad = models.IntegerField(validators=[MinValueValidator(1)], default=0)  # Hs de los programadores
    trabajo_por_realizar = models.IntegerField(validators=[MinValueValidator(1)], default=0)  # Hs de los US
    dia_actual = models.IntegerField(default=0)
    duracion_anterior = models.IntegerField(default=0)  # Backup de la duracion cuando se edita un sprint

    class Meta:

        permissions = (
            ("asignar_miembro_sprint", "Asignar miembros a un sprint"),
            ("ver_miembro_sprint", "Ver miembros de un sprint"),
            ("remover_miembro_sprint", "Remover miembros de un sprint"),
            ("cambiar_estado_sprint", "Cambiar estado de un sprint. De ACTIVO a PENDIENTE y viceversa"),
            ("cancelar_sprint", "Cancelar un sprint"),
        )
        """
            Permisos relacionados con los miembros de un proyecto específico
        """

    def set_estado(self, estado):
        """
        :type estado:
        :param estado:
        Cambia el estado de un sprint a un estado definido como parámetro
        """
        self.estado = estado
        self.save()
        return

    def set_fecha(self):
        """Calcula y asigna la fecha de inicio fin de un sprint a partir de la fecha actual y la duración del sprint"""
        self.fecha_inicio = datetime.date.today()
        self.fecha_fin = self.fecha_inicio + timedelta(days=self.duracion)
        self.save()
        return

    def get_cant_dias(self):
        """
        :return: retorna la cantidad dias
        Retorna la cantidad de días que han pasado desde el inicio del sprint
        """
        fecha = datetime.date.today()
        return (fecha - self.fecha_inicio).days

    def is_finalizado(self):
        """
        :return: Retorna True si el sprint ha finalizado, False en caso contrario
        Verifica si un sprint está finalizado o no
        """
        if self.estado == 'F':
            return True
        return False

    def is_cancelado(self):
        """
        :return: Retorna True si el sprint se ha cancelado, False en caso contrario
        Verifica si un sprint está cancelado o no"""
        if self.estado == 'C':
            return True
        return False

    def is_activo(self):
        """
        :return: Retorna True si el sprint está activo, False en caso contrario
        Verifica si un sprint está activo o no"""
        if self.estado == 'A':
            return True
        return False

    def is_pausado(self):
        """
        :return: Retorna True si el sprint está pausado, False en caso contrario
        Verifica si un sprint está pausado o no"""
        if self.estado == 'S':
            return True
        return False

    def is_pendiente(self):
        """
        :return: Retorna True si el sprint está pendiente, False en caso contrario
        Verifica si un sprint está pendiente o no"""
        if self.estado == 'P':
            return True
        return False

    def add_capacidad(self, horas):
        """Incrementa la capacidad de un sprint en una cantidad de horas recibida como parámetro"""
        self.capacidad += horas
        self.save()
        return

    def quitar_capacidad(self, horas):
        """Decrementa la capacidad de un sprint en una cantidad de horas recibida como parámetro"""
        self.capacidad -= horas
        self.save()
        return

    def reset_capacidad(self):
        """Establece la capacidad de un sprint a cero"""
        self.capacidad = 0
        self.save()
        return

    def add_trabajo(self, horas):
        """Incrementa el campo de trabajo por realizar de un sprint en una cantidad de horas recibida como parámetro"""
        self.trabajo_por_realizar += horas
        self.save()
        return

    def quitar_trabajo(self, horas):
        """Decrementa el campo de trabajo por realizar de un sprint en una cantidad de horas recibida como parámetro"""
        self.trabajo_por_realizar -= horas
        self.save()
        return

    def iniciar_dia_actual(self):
        """Establece el campo día actual a 1"""
        self.dia_actual = 1
        self.save()
        return

    def set_dia_actual(self, dia):
        """Establece el campo día actual a un día recibido como parámetro"""
        self.dia_actual = dia
        self.save()
        return

    def guardar_duracion(self):
        """Guarda la duración anterior de un sprint cuando de ha editado la duración de un proyecto, para calcular la nueva capacidad"""
        self.duracion_anterior = self.duracion
        self.save()
        return


class TrabajoDiario(models.Model):
    """
    Almacena el trabajo que se realizo en un dia especifico dentro de un sprint
    """

    sprint = models.ForeignKey(Sprint, on_delete=models.CASCADE, null=True, blank=True)
    """Sprint dentro del cual se ha realizado el trabajo"""
    trabajo_realizado = models.IntegerField(default=0)
    """Cantidad de horas trabajadas en un dia determinado"""
    trabajo_esperado = models.IntegerField(default=0)
    """Cantidad de trabajo restante hasta un dia determinado"""

    def add_trabajo_realizado(self, horas):
        """
        :type horas: int
        :param horas: cantidad de horas
        Establece el campo de trabajo realizado a una cantidad de horas recibidas como parámetro
        """
        self.trabajo_realizado += horas
        self.save()
        return

    def set_trabajo_esperado(self, horas):
        """
        :type horas: int
        :param horas: cantidad de horas
        Establece el campo de trabajo esperado a una cantidad de horas recibidas como parámetro"""
        self.trabajo_esperado = horas
        self.save()
        return

