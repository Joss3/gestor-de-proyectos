import os


def populate():

    print('Cargando base de datos...')
    print('----------------------\n')

    # PERMISOS

    print('\nCreando permisos...')
    print('--------------------\n')

    add_permiso('view_proyecto', 'Ver proyecto')
    add_permiso('change_proyecto', 'Modificar proyecto')
    add_permiso('asignar_miembro', 'Asignar miembros a un proyecto')
    add_permiso('ver_miembro', 'Ver miembros de un proyecto')
    add_permiso('remover_miembro', 'Remover miembros de un proyecto')
    add_permiso('agregar_cliente', 'Agregar cliente al proyecto')
    add_permiso('editar_cliente', 'Editar cliente del proyecto')
    add_permiso('crear_sprint', 'Agregar sprints al proyecto')
    add_permiso('editar_sprint', 'Editar sprints del proyecto')
    add_permiso('ver_sprint', 'Ver sprints del proyecto')
    add_permiso('asignar_miembro_sprint', 'Asignar miembros al sprint')
    add_permiso('ver_miembro_sprint', 'Ver miembros de los sprints del proyecto')
    add_permiso('importar_flujo_sprint', 'Importar Flujo dentro de un sprint')
    add_permiso('crear_us', 'Crear User Story')
    add_permiso('crear_tus', 'Crear Tipo de User Story')
    add_permiso('importar_tus', 'Importar Tipo de User Story')

    # ROLES

    print('\nCreando roles...')
    print('--------------------\n')

    perms = []
    perm = Permiso.objects.all()

    for p in perm:
        perms.append(p)

    crear_rol('scrum_master', perms)

    perms = []

    perms.append(Permiso.objects.get(nombre='view_proyecto'))
    perms.append(Permiso.objects.get(nombre='ver_miembro'))
    perms.append(Permiso.objects.get(nombre='ver_sprint'))

    crear_rol('desarrollador', perms)

    # USUARIOS

    print('\nCreando usuarios...')
    print('--------------------\n')

    create_super_user('super', 'super@gmail.com', 'super')

    crear_usuario('00000000', 'admin', 'Pedro Dario', 'Perez Martinez', 'admin@gmail.com', '0982 123 456', 'Av. Mcal. López Nº 5696', 'AD')
    crear_usuario('00000000', 'jose', 'Jose Andres', 'Martinez Perez', 'jose@outlook.com', '0987-557-253', 'Vencedores del Chaco 150 y Cacique Lambaré', 'US')
    crear_usuario('00000000', 'antonio', 'Antonio Mateo', 'Cruz Calvo', 'antonio@outlook.com', '0986-300-610', 'Boquerón e/ Miller y Gondra', 'US')
    crear_usuario('00000000', 'martin', 'Martín', 'Gomez', 'martin@gmail.com', '0967-227-501', 'Av. Cacique Lambaré c/ Dr. L. M. Argaña', 'US')
    crear_usuario('00000000', 'nicolas', 'Nicolás Daniel', 'Ramos Flores', 'nicolas@outlook.com', '0981-281-776', 'Dr. Río Boo 469 c/ Chaco Boreal', 'US')
    crear_usuario('00000000', 'francisco', 'Francisco Javier', 'Mora Roman', 'francisco@hotmail.com', '0984-288-174', 'Av. Cacique Lambaré esq. Ayoreos, Lambaré', 'US')
    crear_usuario('00000000', 'mateo', 'Mateo', 'Alonso Cabrera', 'mateo@hotmail.com', '0968-127-652', 'Av. Hernán Cortez 589, Lambaré', 'US')
    crear_usuario('00000000', 'ana', 'Ana Andrea', 'Martinez Lopez', 'ana@gmail.com', '0981-343-785', 'Gral. Díaz 471', 'US')
    crear_usuario('00000000', 'anamaria', 'Ana María', 'Cano Nuñez', 'anamaria@gmail.com', '0985-886-345', 'Manuel Dominguez 344 e/ Iturbe y Caballero', 'US')
    crear_usuario('00000000', 'victoria', 'Victoria Monserratt', 'Muñoz Ramos', 'victoria@hotmail.com', '0962-186-001', 'Av. Artigas 2681 c/ Dematei', 'US')
    crear_usuario('00000000', 'alicia', 'Alicia María', 'Ramirez Navarro', 'alicia@gmail.com', '0982-676-074', 'Av. Eusebio Ayala 1192', 'US')
    crear_usuario('00000000', 'teresa', 'Teresa', 'Soler Santos', 'teresa@outlook.com', '0983-286-608', 'Ruta Mcal. Estigarribia 1536', 'US')
    crear_usuario('00000000', 'irene', 'Irene', 'Aguilar Prieto', 'irene@gmail.com', '0988-688-066', 'Monseñor Pantaleón Garcia N° 182 c/ Ind. Nacional', 'US')
    crear_usuario('00000000', 'lucia', 'Lucía', 'Marin Ferrer', 'lucia@gmail.com', '0973-276-646', 'Yegros 956 c/ Tte. Fariña', 'US')

    # PROYECTO

    print('\nCreando proyectos...')
    print('--------------------\n')

    crear_proyecto('Sistema de Gestion', datetime.datetime.strptime('10/06/2019', '%d/%m/%Y').date(),
                   datetime.datetime.strptime('10/07/2019', '%d/%m/%Y').date(), 'nicolas')

    crear_proyecto('Sistema de Contabilidad', datetime.datetime.strptime('1/06/2019', '%d/%m/%Y').date(),
                   datetime.datetime.strptime('30/08/2019', '%d/%m/%Y').date(), 'jose')

    crear_proyecto('Pagina web del Ministerio de Salud', datetime.datetime.strptime('22/05/2019', '%d/%m/%Y').date(),
                   datetime.datetime.strptime('05/06/2019', '%d/%m/%Y').date(), 'ana')

    # MIEMBROS PROYECTO

    print('\nAgregar miembros a proyecto...')
    print('--------------------\n')

    agregar_usuario_proyecto('Sistema de Gestion', 'antonio', 'desarrollador')
    agregar_usuario_proyecto('Sistema de Gestion', 'martin', 'desarrollador')
    agregar_usuario_proyecto('Sistema de Gestion', 'anamaria', 'desarrollador')

    agregar_usuario_proyecto('Sistema de Contabilidad', 'teresa', 'desarrollador')
    agregar_usuario_proyecto('Sistema de Contabilidad', 'irene', 'desarrollador')
    agregar_usuario_proyecto('Sistema de Contabilidad', 'mateo', 'desarrollador')

    agregar_usuario_proyecto('Pagina web del Ministerio de Salud', 'lucia', 'desarrollador')
    agregar_usuario_proyecto('Pagina web del Ministerio de Salud', 'victoria', 'desarrollador')

    # TUS

    print('\nCreando TUS...')
    print('--------------------\n')
    crear_tus('Tipo1', 1)
    crear_tus('Tipo2', 2)
    crear_tus('Tipo3', 3)

    # FLUJOS

    print('\nCreando flujos...')
    print('--------------------\n')
    crear_flujo('Flujo1', 1, 3)
    crear_flujo('Flujo2', 1, 2)
    crear_flujo('FlujoA', 2, 4)
    crear_flujo('FlujoX', 3, 3)

    # FASES

    print('\nCreando fases...')
    print('--------------------\n')
    crear_fase('FaseA', 1, 1)
    crear_fase('FaseB', 1, 2)
    crear_fase('FaseC', 1, 3)
    crear_fase('Fase1', 2, 1)
    crear_fase('Fase2', 2, 2)
    crear_fase('F1', 3, 1)
    crear_fase('F2', 3, 2)
    crear_fase('F3', 3, 3)
    crear_fase('F4', 3, 4)
    crear_fase('FaseX', 4, 1)
    crear_fase('FaseY', 4, 2)
    crear_fase('FaseZ', 4, 3)

    # US

    print('\nCreando US...')
    print('--------------------\n')

    '''crear_us(nombre, descripcion, criterios_aceptacion, trabajo_por_realizar, prioridad, valor_negocio, valor_tecnico,
             tipo_id, proyecto_id, flujo_id, fase_id)'''

    crear_us('Ambiente de desarrollo y produccion', 'Descripcion 1', 'Criterios de aceptacion 1', 15, 10, 4, 9, 1, 1, 1, 1)
    crear_us('Script Base de Datos', 'Descripcion 2', 'Criterios de aceptacion 2', 15, 5, 4, 5, 1, 1, 2, 4)
    crear_us('Modulo base de usuarios y permisos', 'Descripcion 3', 'Criterios de aceptacion 3', 20, 8, 6, 8, 1, 1, 2, 4)
    crear_us('Login', 'Descripcion 4', 'Criterios de aceptacion 4', 25, 5, 5, 8, 1, 1, 1, 1)

    # SPRINTS

    print('\nCreando sprints...')
    print('--------------------\n')

    crear_sprint('Sistema de Gestion', 14)
    crear_sprint('Sistema de Contabilidad', 7)
    crear_sprint('Pagina web del Ministerio de Salud', 10)

    # AGREGAR USUARIO SPRINTS

    print('\nCreando sprints...')
    print('--------------------\n')

    agregar_usuario_sprint('Sistema de Gestion', 'Sprint 1', 'antonio')
    agregar_usuario_sprint('Sistema de Gestion', 'Sprint 1', 'martin')
    agregar_usuario_sprint('Sistema de Gestion', 'Sprint 1', 'anamaria')
    agregar_usuario_sprint('Sistema de Gestion', 'Sprint 1', 'nicolas')

    agregar_usuario_sprint('Sistema de Contabilidad', 'Sprint 1', 'teresa')
    agregar_usuario_sprint('Sistema de Contabilidad', 'Sprint 1', 'irene')
    agregar_usuario_sprint('Sistema de Contabilidad', 'Sprint 1', 'mateo')
    agregar_usuario_sprint('Sistema de Contabilidad', 'Sprint 1', 'jose')

    agregar_usuario_sprint('Pagina web del Ministerio de Salud', 'Sprint 1', 'victoria')
    agregar_usuario_sprint('Pagina web del Ministerio de Salud', 'Sprint 1', 'lucia')
    agregar_usuario_sprint('Pagina web del Ministerio de Salud', 'Sprint 1', 'ana')

    # AGREGAR US A SPRINTS

    print('\nCreando sprints...')
    print('--------------------\n')

    importar_us_sprint("Sistema de Gestion", 'Sprint 1', 1, 'antonio')
    importar_us_sprint("Sistema de Gestion", 'Sprint 1', 2, 'martin')
    importar_us_sprint("Sistema de Gestion", 'Sprint 1', 3, 'martin')
    importar_us_sprint("Sistema de Gestion", 'Sprint 1', 4, 'anamaria')

    # INICIAR SPRINT

    print('\nIniciando sprints...')
    print('--------------------\n')

    iniciar_sprint('Sistema de Gestion', 'Sprint 1')

    # Activar US

    print('\nActivando us...')
    print('--------------------\n')

    cambiar_estado_us(1)
    cambiar_estado_us(2)
    cambiar_estado_us(4)

    # ACTIVIDADES

    print('\nRegistrando actividades...')
    print('--------------------\n')

    # (us_id, descripcion, fecha_cargado, trabajo, username, id)

    crear_actividad(1, 'Creacion de estructura', 2, 2, 'antonio', 1)
    crear_actividad(4, 'Estructura y documentacion', 2, 4, 'anamaria', 2)
    crear_actividad(2, 'Funcion principal', 1, 2, 'martin', 3)
    crear_actividad(1, 'Documentacion inicial', 1, 1, 'antonio', 4)


def add_permiso(nombre, descripcion):
    """
    Crea un objeto del tipo permiso dentro del sistema
    :param nombre: Nombre del permiso
    :param descripcion: Descripción del permiso
    """
    p, created = Permiso.objects.get_or_create(nombre=nombre,
                                               descripcion=descripcion)
    print('- Permiso: {0}, Created: {1}'.format(str(p), str(created)))
    return


def crear_rol(nombre, lista_permisos):
    """
    Crea roles dentro del sistema
    :param lista_permisos: Lista de permisos que contendrá el sistema
    :param nombre: Nombre del rol
    """

    if not Rol.objects.filter(nombre=nombre).exists():

        rol = Rol.objects.create(nombre=nombre)

        for p in lista_permisos:
            rol.permisos.add(p)
            rol.save()

        created = True
        print('- Rol: {0}, Created: {1}'.format(str(rol), str(created)))
    else:
        print('Ya existe un rol con el nombre ' + nombre + '!!!')

    return


def crear_usuario(password, username, first_name, last_name, email, telefono, direccion, tipo):
    """
    Crea un usuario dentro del sistema
    :param password: Contraseña del usuario
    :param username: Username del usuario
    :param first_name: Nombre/s del usuario
    :param last_name: Apellido/s del usuario
    :param email: Dirección de correo electronico del usuario
    :param telefono: Número de teléfono del usuario
    :param direccion: Dirección del usuario
    :param tipo: Tipo de usuario que se va a crear. AD y US, que son Administrador y Usuario respectivamente
    """

    if not User.objects.filter(username=username).exists():

        user = User.objects.create_user(username, email, password)

        user.last_login = datetime.date.today()
        user.is_superuser = False
        user.first_name = first_name
        user.last_name = last_name
        user.is_staff = True
        user.is_active = True
        user.date_joined = datetime.date.today()
        user.save()

        usuario = Usuario.objects.get(pk=user.id)

        if usuario:
            print(usuario)
            usuario.set_telefono(telefono)
            usuario.set_direccion(direccion)
            if tipo == 'AD':
                usuario.tipo = 'AD'

                # Asignacion de permisos de administrador

                # Permiso para crear proyectos
                content_type = ContentType.objects.get_for_model(Proyecto)

                permiso = Permission.objects.get(codename='add_proyecto', content_type=content_type)
                usuario.user.user_permissions.add(permiso)
                usuario.save()

                # Permiso para crear y ver usuarios

                content_type = ContentType.objects.get_for_model(Usuario)
                permiso = Permission.objects.get(codename='add_usuario', content_type=content_type)
                usuario.user.user_permissions.add(permiso)
                usuario.save()

                permiso = Permission.objects.get(codename='view_usuario', content_type=content_type)
                usuario.user.user_permissions.add(permiso)
                usuario.save()

                permiso = Permission.objects.get(codename='change_usuario', content_type=content_type)
                usuario.user.user_permissions.add(permiso)
                usuario.save()

                # Permiso para crear roles

                content_type = ContentType.objects.get_for_model(Rol)
                permiso = Permission.objects.get(codename='add_rol', content_type=content_type)
                usuario.user.user_permissions.add(permiso)
                usuario.save()

                permiso = Permission.objects.get(codename='change_rol', content_type=content_type)
                usuario.user.user_permissions.add(permiso)
                usuario.save()

                permiso = Permission.objects.get(codename='view_rol', content_type=content_type)
                usuario.user.user_permissions.add(permiso)
                usuario.save()

        created = True

        print('- Usuario: {0}, Created: {1}'.format(str(user), str(created)))

    else:
        print('- El usuario ' + username + ' ya existe!!!')

    return


def crear_proyecto(nombre, fecha_inicio, fecha_fin, scrum_username):

    if not Proyecto.objects.filter(nombre=nombre).exists():

        scrum = Usuario.objects.get(user__username=scrum_username)

        proyecto = Proyecto.objects.create(nombre=nombre, fecha_inicio=fecha_inicio, fecha_fin=fecha_fin,
                                           estado='P', scrum=scrum)

        # Se trae el rol scrum_master
        rol = Rol.objects.get(nombre='scrum_master')

        # Se crea un nuevo grupo con un nombre determinado por el usuario y el proyecto
        nombre_rol = 'scrum' + str(nombre) + str(scrum.user.username)

        try:
            nuevogrupo = Group.objects.create(name=nombre_rol)

            # Asignacion de permisos

            permisos = rol.permisos.all()

            for perm in permisos:
                assign_perm(perm.nombre, nuevogrupo, proyecto)

            scrum.user.groups.add(nuevogrupo)

            proyecto.usuarios.add(scrum)

            proyecto.set_duracion()

            proyecto.save()
            nuevogrupo.save()
            scrum.save()

        except:
            print("Error al asignar los permisos")
            Group.objects.get(name=nombre_rol).delete()
            proyecto.delete()

            return

        # Creacion y asignacion de Product Backlog
        ProductBacklog.objects.create(proyecto_id=proyecto.id)

        created = True

        print('- Proyecto: {0}, Created: {1}'.format(str(proyecto), str(created)))

    else:
        print('Ya existe un proyecto llamado ' + nombre + '!!!')

    return


def agregar_usuario_proyecto(proyecto_nombre, username, nombre_rol):

    proyecto = Proyecto.objects.get(nombre=proyecto_nombre)
    user = User.objects.get(username=username)
    usuario = Usuario.objects.get(user=user)
    rol = Rol.objects.get(nombre=nombre_rol)

    # Se crea un nuevo grupo con un nombre determinado por los nombres del proyecto, del usuario y del rol
    nombre_rol = 'proyecto' + str(proyecto.id) + str(username) + str(nombre_rol)

    if not Group.objects.filter(name=nombre_rol).exists():

        nuevogrupo = Group.objects.create(name=nombre_rol)

        # Asignacion de permisos

        permisos = rol.permisos.all()

        for perm in permisos:
            assign_perm(perm.nombre, nuevogrupo, proyecto)

        user.groups.add(nuevogrupo)
        proyecto.usuarios.add(usuario)

        print('- Usuario: {0}, Agregado a proyecto: {1}'.format(str(usuario.user.username), str(True)))
    else:
        print('- Usuario: {0}, Agregado a proyecto: {1}'.format(str(usuario.user.username), str(False)))

    return


def crear_sprint(proyecto_nombre, duracion):

    proyecto = Proyecto.objects.get(nombre=proyecto_nombre)

    if proyecto.cant_sprints == 2 and proyecto.sprint_activo > 0 or proyecto.cant_sprints == 0:

        sprint = Sprint.objects.create(duracion=duracion)

        sprint.estado = 'P'
        sprint.proyecto = proyecto

        proyecto.incrementar()

        sprint_nombre = 'Sprint ' + str(proyecto.numero)

        sprint.nombre = sprint_nombre

        sprint.miembros.add(proyecto.scrum)

        # Sprint backlog

        backlog = SprintBacklog.objects.create(sprint_id=sprint.id)

        sprint.proyecto.add_sprint()

        sprint.save()

        print('- Sprint: {0}, Proyecto: {1}, Creado: {2}'.format(str(sprint.nombre), str(sprint.proyecto.nombre),
                                                                 str(True)))
    else:
        print('- Ya se alcanzó el límite de sprints en el proyecto {0}. Creado: {1}'.format(str(proyecto_nombre),
                                                                                            str(False)))
    return


def crear_tus(nombre, proyecto_id):
    proyecto = Proyecto.objects.get(pk=proyecto_id)
    if not Tipo_US.objects.filter(nombre=nombre).exists():
        tus = Tipo_US.objects.create(nombre=nombre, proyecto=proyecto)

        print('- Tus: {0}, Created: {1}'.format(str(tus), str(True)))
    else:
        print('Ya existe un tus con el nombre ' + nombre + '!!!')


def crear_flujo(nombre, tipo_id, cant):
    tipo = Tipo_US.objects.get(pk=tipo_id)
    if not Flujo.objects.filter(nombre=nombre).exists():
        flujo = Flujo.objects.create(nombre=nombre, tipo=tipo, cant_fases=cant)

        print('- Flujo: {0}, Created: {1}'.format(str(flujo), str(True)))
    else:
        print('Ya existe un flujo con el nombre ' + nombre + '!!!')


def crear_fase(nombre, flujo_id, indice):

    if not Fase.objects.filter(nombre=nombre).exists():
        flujo = Flujo.objects.get(pk=flujo_id)
        fase = Fase.objects.create(nombre=nombre, id_flujo=flujo, indice=indice)
        print('- Fase: {0}, Created: {1}'.format(str(fase), str(True)))
    else:
        print('Ya existe una fase con el nombre ' + nombre + '!!!')


def crear_us(nombre, descripcion, criterios_aceptacion, trabajo_por_realizar, prioridad, valor_negocio, valor_tecnico,
             tipo_id, proyecto_id, flujo_id, fase_id):
    proy = Proyecto.objects.get(pk=proyecto_id)
    tipo = Tipo_US.objects.get(pk=tipo_id)
    flujo = Flujo.objects.get(pk=flujo_id)
    fase = Fase.objects.get(pk=fase_id)

    if not UserStory.objects.filter(nombre=nombre).exists():

        us = UserStory.objects.create(nombre=nombre, descripcion=descripcion, criterios_aceptacion=criterios_aceptacion,
                                      trabajo_por_realizar=trabajo_por_realizar, prioridad=prioridad,
                                      valor_negocio=valor_negocio, valor_tecnico=valor_tecnico, tipo=tipo)

        backlog = ProductBacklog.objects.get(proyecto_id=proy.id)
        us.set_product_backlog(backlog)
        calculo_prioridad = (us.prioridad + 2 * us.valor_negocio + 2 * us.valor_tecnico) / 5
        us.set_prioridad_final(calculo_prioridad)
        us.reset_trabajo_realizado()
        us.set_flujo(flujo)
        us.reset_estado_fase()
        us.set_fase(fase)

        print('- US: {0}, Created: {1}'.format(str(us), str(True)))
    else:
        print('Ya existe un US con el nombre ' + nombre + '!!!')


def agregar_usuario_sprint(proyecto_nombre, sprint_nombre, username):

    if Sprint.objects.filter(proyecto__nombre=proyecto_nombre, nombre=sprint_nombre).exists():

        sprint = Sprint.objects.get(proyecto__nombre=proyecto_nombre, nombre=sprint_nombre)
        usuario = Usuario.objects.get(user__username=username)

        miembros = sprint.miembros.all()

        if usuario not in miembros:
            usuario.set_en_sprint(sprint.proyecto_id)
            sprint.miembros.add(usuario)
            horas = sprint.duracion * usuario.horas_laborales  # horas por dia
            usuario.add_horas_disponibles(horas)
            sprint.add_capacidad(horas)

            print('- Usuario: {0}, Agregado a Sprint: {1}'.format(str(usuario.user.username), str(True)))
        else:
            print("El usuario ya está en el sprint indicado!!!")
    else:
        print("El sprint indicado no existe!!!")
    return


def importar_us_sprint(proyecto_nombre, sprint_nombre, us_id, username):

    if Sprint.objects.filter(proyecto__nombre=proyecto_nombre, nombre=sprint_nombre).exists():

        sprint = Sprint.objects.get(proyecto__nombre=proyecto_nombre, nombre=sprint_nombre)
        usuario = Usuario.objects.get(user__username=username)
        us = UserStory.objects.get(id=us_id)
        sprint_backlog = SprintBacklog.objects.get(sprint_id=sprint.id)

        if not us.sprint_backlog == sprint_backlog:
            us.set_sprint_backlog(sprint_backlog)
            sprint.add_trabajo(us.trabajo_por_realizar)

            # asignar a usuario
            us.asignar_usuario(usuario)
            usuario.restar_horas_disponibles(us.trabajo_por_realizar)

            print('- US: {0}, Importado a Sprint: {1}, Asignado a usuario: {2}'.format(str(usuario.user.username),
                                                                                       str(sprint.nombre),
                                                                                       str(username)))
        else:
            print("El US ya está en el sprint indicado!!!")
    else:
        print("El sprint indicado no existe!!!")
    return


def iniciar_sprint(proyecto_nombre, sprint_nombre):

    if Sprint.objects.filter(proyecto__nombre=proyecto_nombre, nombre=sprint_nombre).exists():

        sprint = Sprint.objects.get(proyecto__nombre=proyecto_nombre, nombre=sprint_nombre)

        uss = SprintBacklog.objects.get(sprint_id=sprint.id).userstory_set.all()

        us_libres = 0
        for us in uss:
            if not us.usuario_asignado:
                us_libres += 1

        if sprint.proyecto.sprint_activo == sprint.id or sprint.proyecto.sprint_activo == 0:
            sprint.proyecto.set_estado('A')
            sprint.proyecto.set_fecha_inicio()
            us_sprint = UserStory.objects.filter(sprint_backlog=sprint.sprintbacklog_set.all()[0])

            for us in us_sprint:
                us.estado_proyecto = 'P'
                us.save()

            if sprint.is_pendiente():
                sprint.set_fecha()
                sprint.fecha_inicio = sprint.fecha_inicio - timedelta(days=2)
                sprint.fecha_fin = sprint.fecha_fin - timedelta(days=2)
                sprint.proyecto.set_sprint(sprint.id)
                sprint.iniciar_dia_actual()

                i = sprint.duracion
                while i > 0:
                    TrabajoDiario.objects.create(sprint=sprint)
                    i -= 1

        sprint.set_estado('A')
        print('- Sprint: {0}, Proyecto: {1}, Iniciado: {2} '.format(str(sprint.nombre), str(sprint.proyecto.nombre),
                                                                    str(True)))
    else:
        print("El sprint indicado no existe!!!")
    return


def crear_actividad(us_id, descripcion, fecha_cargado, trabajo, username, id):

    if not Actividad.objects.filter(id=id).exists():
        us = UserStory.objects.get(id=us_id)
        usuario = Usuario.objects.get(user__username=username)
        act = Actividad.objects.create(descripcion=descripcion, trabajo=trabajo)
        act.user_story = us
        act.fecha_cargado = datetime.date.today() - timedelta(days=fecha_cargado)
        act.autor = usuario
        act.save()
        act = Actividad.objects.get(id=act.id)
        us.actividad_agregada = True
        us.add_trabajo_realizado(act.trabajo)
        horas = act.trabajo
        us.iniciado = True
        sprint = Sprint.objects.get(pk=us.sprint_backlog.sprint.id)

        # Lista de horas trabajadas por dia, para el burndownchart
        trabajos = TrabajoDiario.objects.all().filter(sprint=sprint).order_by('id')
        trabajoss = []
        for trabajo in trabajos:
            trabajoss.append(trabajo)

        dia = (act.fecha_cargado - sprint.fecha_inicio).days

        trabajo = trabajoss[dia]

        trabajo.add_trabajo_realizado(horas)

        id_inicial = trabajoss[0].id
        dia_actual = sprint.get_cant_dias()

        id_actual = id_inicial + dia_actual - fecha_cargado

        for trabajo in trabajoss:
            if trabajo.id > id_actual:
                trabajo.add_trabajo_realizado(horas)
        print('- Actividad: {0}, Registrada correctamente: {1}'.format(str(usuario.user.username), str(True)))
    else:
        print('Esta actividad ya esta registrada !!!')


def cambiar_estado_us(us_id):
    us = UserStory.objects.get(id=us_id)
    us.estado_fase = 2
    us.save()


def create_super_user(username, email, password):
    """
    Crea un super usuario de django
    Si por alguna razón no se puede crear el super user, se contiene el error
    """
    try:
        u = User.objects.create_superuser(username, email, password)
        return u
    except IntegrityError:
        pass


if __name__ == '__main__':
    print(('\n' + ('=' * 80) + '\n'))
    import django
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'gestorProyectos.settings')
    django.setup()
    from roles_permisos.models import Permiso, Rol
    from django.contrib.auth.models import User
    from usuarios.models import Usuario
    from django.contrib.contenttypes.models import ContentType
    from django.contrib.auth.models import Permission
    from django.db import IntegrityError
    from proyecto.models import Proyecto
    import datetime
    from django.contrib.auth.models import Group
    from guardian.shortcuts import assign_perm
    from product_backlog.models import ProductBacklog
    from sprint_backlog.models import SprintBacklog
    from tipo_us.models import Tipo_US
    from sprint.models import Sprint, TrabajoDiario
    from flujo.models import Flujo, Fase
    from user_story.models import UserStory
    from actividad.models import Actividad
    import datetime
    from datetime import timedelta
    populate()  # Call the populate function
