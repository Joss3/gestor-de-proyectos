from django.db import models

# Create your models here.


class Cliente(models.Model):

    nombre = models.CharField(max_length=100)
    """Nombre del cliente"""
    from proyecto.models import Proyecto
    proyecto = models.ForeignKey(Proyecto, on_delete=models.CASCADE, blank=True, null=True)
    """Indica el proyecto al cual está relacionado el cliente"""
    empresa = models.CharField(max_length=100)
    """Empresa a la cual representa el cliente"""
    telefono = models.CharField(max_length=100)
    """Número telefonico del cliente"""

    def __str__(self):
        return f' {self.nombre}'

    def set_proyecto(self, proyecto):
        """
        :param proyecto: Objeto proyecto al cual se asigna el cliente
        Asigna un cliente a un proyecto
        """
        self.proyecto = proyecto
        self.save()
        return
