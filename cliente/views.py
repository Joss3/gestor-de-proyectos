from django.shortcuts import render
from django.contrib.auth.models import User
from django.shortcuts import render, redirect, get_object_or_404
from .forms import CrearCliente, EditarCliente
from django.http import HttpResponseRedirect
from django.urls import reverse
from cliente.models import Cliente
from proyecto.models import Proyecto


def crear_cliente(request, proyecto_id):
    """
    :param request: Objeto que contiene metadatos sobre la solicitud
    :param proyecto_id: ID del proyecto al cual estara asignado el cliente
    :return: pagina principal del proyecto

    Esta funcion toma los datos introducidos en el formulario CrearCliente y crea un nuevo
    """
    usuario = request.user
    objeto_permiso = User.objects.get(username=usuario).get_all_permissions() # cambiar, usar desde el template y preguntar a request.user

    if request.method == "POST":

        form1 = CrearCliente(request.POST)

        if form1.is_valid():

            proyecto = Proyecto.objects.get(id=proyecto_id)

            cliente = form1.save(commit=False)

            cliente.set_proyecto(proyecto)

            cliente.save()

        return redirect('ver_proyecto', proyecto_id)
    else:
        form1 = CrearCliente()
    return render(request, 'cliente/agregar_cliente.html', {'form': form1, 'titulo': 'Crear Cliente',
                                                            'objeto_permiso': objeto_permiso,
                                                            'proyecto_id': proyecto_id})


def editar_cliente(request, proyecto_id):
    """
    Funcion para editar el campo cliente existente dentro de un proyecto
    :param proyecto_id: ID del proyecto donde se encuentra el cliente
    :return: pagina principal de proyecto, con el campo cliente editado
    """

    cliente = Cliente.objects.get(proyecto_id__exact=proyecto_id)
    usuario = request.user
    objeto_permiso = User.objects.get(username=usuario).get_all_permissions()  # tendra roles predeterminados
    if request.method == 'POST':
        form = EditarCliente(request.POST, instance=cliente)
        if form.is_valid():
            cliente = form.save(commit=False)
            cliente.save()
            return redirect('ver_proyecto', proyecto_id)
        return HttpResponseRedirect(reverse('ver_proyecto', kwargs={'proyecto_id': proyecto_id}))
    form_editar_cliente = EditarCliente(None, instance=cliente)
    return render(request, 'cliente/agregar_cliente.html', {'form': form_editar_cliente,
                                                            'titulo': 'Editar cliente',
                                                            'objeto_permiso': objeto_permiso,
                                                            'proyecto_id': proyecto_id})


def eliminar_cliente(request, proyecto_id):
    """
    Resetea el campo cliente dentro de un proyecto especifico
    :param proyecto_id: Id del proyecto con el campo cliente
    :return: pagina principal del proyecto con el campo reseteado
    """
    Cliente.objects.get(proyecto_id__exact=proyecto_id).delete()
    return redirect('ver_proyecto', proyecto_id)
