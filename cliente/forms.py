from django import forms
from cliente.models import Cliente


class CrearCliente(forms.ModelForm):

    """
    Formulario de creación de Clientes
    """

    class Meta:
        model = Cliente
        fields = ('proyecto', 'empresa', 'nombre', 'telefono')
        widgets = {'proyecto': forms.HiddenInput()}


class EditarCliente(forms.ModelForm):
    """
    Formulario de modificacion de Clientes
    """

    class Meta:
        model = Cliente
        fields = ('proyecto', 'empresa', 'nombre', 'telefono')
        widgets = {'proyecto': forms.HiddenInput()}
