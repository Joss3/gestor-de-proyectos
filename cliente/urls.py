from django.contrib import admin
from django.urls import include, path
from .views import crear_cliente, editar_cliente, eliminar_cliente

urlpatterns = [
    path('<int:proyecto_id>/crear/', crear_cliente, name='crear_cliente'),
    path('<int:proyecto_id>/editar', editar_cliente, name='editar_cliente'),
    path('<int:proyecto_id>/eliminar', eliminar_cliente, name='eliminar_cliente'),
]
