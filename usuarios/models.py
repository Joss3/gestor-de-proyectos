from django.db import models
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User
from roles_permisos.models import Rol
from django.contrib.auth.models import User, Permission


from django.dispatch import receiver
from django.db.models.signals import post_save


class Usuario(models.Model):
    """
    Modelo personalizado de usuario personalizado que sirve para cargar datos adicionales al modelo User
    """

    ADMIN = 'AD'
    USER = 'US'

    TIPO_CHOICES = (
        (ADMIN, 'Administrador'),
        (USER, 'Usuario'),
    )

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    telefono = models.CharField(max_length=100, null=True, default='')
    direccion = models.CharField(max_length=100, null=True, default='')
    horas_laborales = models.IntegerField(default=8)  # Hs por dia del programador
    horas_disponibles = models.IntegerField(default=0)  # Hs por sprint del programador
    tipo = models.CharField(max_length=2, choices=TIPO_CHOICES, default=USER)
    en_sprint = models.IntegerField(default=0) # guarda el id del proyecto al cual pertenece el sprint donde el usuario es miembro

    def __str__(self):
        return f' {self.user.username}'

    def has_perm(self, perm, obj):
        aux = self.user.has_perm(perm, obj)
        return aux

    def is_admin(self):
        if self.tipo == 'AD':
            return True
        return False

    def set_admin(self):
        self.tipo = 'AD'
        self.save()
        return

    def set_telefono(self, telefono):
        self.telefono = telefono
        self.save()
        return

    def set_direccion(self, direccion):
        self.direccion = direccion
        self.save()
        return

    def is_in_sprint(self):
        if self.en_sprint == 0:
            return False
        return True

    def set_en_sprint(self, sprint_id):
        self.en_sprint = sprint_id
        self.save()
        return

    def set_no_sprint(self):
        self.en_sprint = 0
        self.save()
        return

    def add_horas_laborales(self, horas):
        self.horas_laborales += horas
        self.save()
        return

    def add_horas_disponibles(self, horas):
        self.horas_disponibles += horas
        self.save()
        return

    def restar_horas_disponibles(self, horas):
        self.horas_disponibles -= horas
        self.save()
        return

    def reset_horas_disponibles(self):
        self.horas_disponibles = 0
        self.save()
        return

    def add_telefono(self, telefono):
        self.telefono = telefono
        self.save()
        return

    def add_direccion(self, direccion):
        self.direccion = direccion
        self.save()
        return


@receiver(post_save, sender = User)
def crear_usuario(sender, instance, created, **kwargs):
    """
    :param sender: modelo que envia la señal
    :param instance: valor que se revive
    :param created: si el evento es una creación
    :param kwargs: necesario en una señal, con argumentos o sin ellos.
    Señal de  post_save
    """
    if created and instance.is_anonymous == False:
        Usuario.objects.create(user=instance)
        user = User.objects.get(pk=instance.pk)

        permiso = Permission.objects.get(codename='change_usuario')
        user.user_permissions.add(permiso)
