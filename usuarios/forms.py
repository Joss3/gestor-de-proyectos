from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .models import Usuario
from django.forms import ModelForm
from django import forms


class CrearUserForm(ModelForm):
    """Formulario de creación de usuarios"""

    class Meta:
        model = User
        fields = ['username', 'email']


class EditarUsuarioForm(ModelForm):

    """
    Formulario para editar la clase Usuario
    """

    class Meta:
        model = Usuario
        fields = ['telefono', 'direccion']

        widgets = {
            'telefono': forms.TextInput(),
            'direccion': forms.TextInput(),
        }


class EditarUserForm(forms.ModelForm):
    """
    Formulario de edicion de usuario
    """
    def __init__(self, *args, **kwargs):
        """
        Cambios de widgets en el formulario
        """
        super(EditarUserForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'

    class Meta:
        """
        Clase que sirve para especificar los los campos del modelo User a ser
        utilizados
        """
        model = User
        fields = ['first_name', 'last_name', 'email', 'is_active']
