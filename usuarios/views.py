from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from .models import Usuario
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect, get_object_or_404
from django.utils.decorators import method_decorator
from django.views.generic import CreateView, UpdateView, ListView, DetailView
from django.urls.base import reverse_lazy
from django import forms
from .forms import CrearUserForm, EditarUserForm, EditarUsuarioForm
from .models import Usuario
from django.forms.models import inlineformset_factory
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth.models import Permission
from proyecto.models import Proyecto
from django.contrib.contenttypes.models import ContentType
from roles_permisos.models import Rol
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from django.shortcuts import render, redirect
from django.contrib import messages
from django.core.mail import send_mail
from gestorProyectos.email_info import EMAIL_HOST


ADMINISTRADOR = 'AD'


class CrearUserView(CreateView):
    """
        Vista de creación de usuarios del modelo User y el formulario CrearUserForm
    """
    model = User
    template_name = "usuario/registrar.html"
    form_class = CrearUserForm

    def form_valid(self, form):
        """
        :param form: Username , email
        Valida el formulario y asigna la contraseña predeterminada
        """
        usuario = form.save(commit=False)
        passw = "00000000" #User.objects.make_random_password()
        # print(passw)
        usuario.set_password(passw)
        username = usuario.username
        email = usuario.email
        usuario = usuario.save()

        subject = 'Le damos la bienvenida al sistema SGP'
        message = 'Saludos ' + username + '\nLe confirmamos la creación de su cuenta de usuario en el sistema ' \
                                          'SGP.\nPara hacer login el en sistema, y comenzar a trabajar, introduzca ' \
                                          'su nombre de usuario y la contraseña por defecto la cual es: ' \
                                          '00000000\n\nAnte cualquier eventualidad no dude en comunicarse con ' \
                                          'nosotros.\nAtentamente, Equipo de SGP'
        from_email = EMAIL_HOST
        to_list = [email]
        fail_silently = True

        send_mail(subject, message, from_email, to_list, fail_silently)

        return redirect('listarusuario')


class CrearAdminView(CreateView):
    """
        Vista de creación de usuarios administradores del modelo User y el formulario CrearUserForm
    """
    model = User
    template_name = "usuario/registrar.html"
    form_class = CrearUserForm

    def form_valid(self, form):
        """
        :param form: Username , email
        Valida el formulario y asigna la contraseña predeterminada
        """
        usuario = form.save(commit=False)
        passw = "00000000"
        # print(passw)
        usuario.set_password(passw)

        aux = usuario.username

        usuario.save()

        #Asignacion de permisos de administrador

        usuario = User.objects.get(username=aux)
        usuario = Usuario.objects.get(user=usuario)

        usuario.tipo = ADMINISTRADOR
        usuario.save()

        # Permiso para crear proyectos
        content_type = ContentType.objects.get_for_model(Proyecto)

        permiso = Permission.objects.get(codename='add_proyecto', content_type=content_type)
        usuario.user.user_permissions.add(permiso)

        # Permiso para crear y ver usuarios

        content_type = ContentType.objects.get_for_model(Usuario)
        permiso = Permission.objects.get(codename='add_usuario', content_type=content_type)
        usuario.user.user_permissions.add(permiso)

        permiso = Permission.objects.get(codename='view_usuario', content_type=content_type)
        usuario.user.user_permissions.add(permiso)

        permiso = Permission.objects.get(codename='change_usuario', content_type=content_type)
        usuario.user.user_permissions.add(permiso)

        # Permiso para crear roles

        content_type = ContentType.objects.get_for_model(Rol)
        permiso = Permission.objects.get(codename='add_rol', content_type=content_type)
        usuario.user.user_permissions.add(permiso)

        permiso = Permission.objects.get(codename='change_rol', content_type=content_type)
        usuario.user.user_permissions.add(permiso)

        permiso = Permission.objects.get(codename='view_rol', content_type=content_type)
        usuario.user.user_permissions.add(permiso)

        # Agregar mas permisos si hace falta

        return redirect('index')


@login_required()
def ListarUserView(request):
    """
    Vista para listar todos los usuarios registrados con acceso a crear usuarios y modificar
    """
    usuarios = Usuario.objects.all()
    user = request.user
    usuario = Usuario.objects.get(user=user)

    objeto_permiso = User.objects.get(username=usuario.user.username).get_all_permissions()  # tendra roles predeterminados

    if usuario.user.has_perm('usuarios.view_usuario'):
        return render(request, 'usuario/listarUsuario.html', {'objeto_usuario': usuarios,
                                                              'user': usuario, 'objeto_permiso': objeto_permiso}
                      )
    else:
        return render(request, 'usuario/acceso_denegado.html', {})


def get_or_none(classmodel, **kwargs):
    try:
        return classmodel.objects.get(**kwargs)
    except classmodel.DoesNotExist:
        return None


@login_required()
def EditarUserView(request, id_User):
    """
    Vista de edición de usuario, esta vista controla que se puedan modificar los datos del modelo User y del modelo Usuario correspondiente, redirecciona a la lista de usuarios
    """
    user = User.objects.get(id=id_User)

    usuario = Usuario.objects.get(user=user)

    objeto_permiso = User.objects.get(username=request.user).get_all_permissions()  # tendra roles predeterminados

    if request.method == 'POST':

        form = EditarUserForm(request.POST, instance=user)
        usuarioForm = EditarUsuarioForm(request.POST, instance=usuario)
        if form.is_valid():
            # usuario = form.save(commit=False)
            form.save()
            usuarioForm.save()
            if request.user.has_perm('view_usuario'):
                return redirect('listarusuario')
            else:
                return redirect('index')

    else:

        form = EditarUserForm(instance=user)
        usuarioForm = EditarUsuarioForm(instance=usuario)

    return render(request, 'usuario/editarusuario.html', {'form': form, 'form2': usuarioForm, 'objeto_permiso': objeto_permiso})


def cambiar_password(request, id_User):
    """
    :type id_User: int
    :param id_User: id del usuario al cual se le cambiará la contraseña
    Permite cambiar la contraseña de un usuario
    """
    user = User.objects.get(id=id_User)
    usuario = Usuario.objects.get(user=user)
    if request.method == 'POST':
        form = PasswordChangeForm(user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Your password was successfully updated!')

            if request.user.has_perm('view_usuario'):
                return redirect('listarusuario')
            else:
                return redirect('index')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordChangeForm(user)
    return render(request, 'usuario/cambiar_password.html', {'form': form, 'usuario': usuario})


class detalle_usuario(DetailView):
    """Vista que muestra datos relacionados con un usuario específico"""
    model = User
    template_name = "usuario/detalle.html"

    def get_context_data(self, **kwargs):

        # Llamamos ala implementacion primero del  context
        context = super(detalle_usuario, self).get_context_data(**kwargs)
        usuario = Usuario.objects.get(user_id=self.kwargs.get('pk'))
        context['usuario'] = usuario

        return context

