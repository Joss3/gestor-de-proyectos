from django.urls import path, include
from .views import *


urlpatterns = [
    path('registrar/', login_required(CrearUserView.as_view()), name='crearusuario'),
    path('registrarAdmin/', CrearAdminView.as_view(), name='crearadmin'),
    path('listar/', ListarUserView, name='listarusuario'),
    path('modificar/<int:id_User>/', EditarUserView, name='modificar_usuario'),
    path('modificar/<int:id_User>/password/', cambiar_password, name='cambiar_password'),
    path('<int:pk>/', detalle_usuario.as_view(), name='detalles'),

]
