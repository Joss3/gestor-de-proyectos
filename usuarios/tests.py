from django.test import TestCase
from django.urls import reverse, reverse_lazy
import unittest
# Create your tests here.
from django.contrib.auth.models import User
from .models import Usuario


class UsuarioTest(TestCase):

    """Prueba unitaria de usuarios en, en el se prueban creación, listado y modificacion"""

    def setUp(self):
       User.objects.create_superuser(password='test', username='test', email='email@email.com').save()

    def test_crearusuario(self):

        """Prueba de creación de usuario en la vista de crear usuario"""

        self.assertTrue(self.client.login(username='test', password='test') )

        response = self.client.post('/usuario/registrar/',{'username':'usuario', 'email':'email@email.com'}, follow=True)

        self.assertEqual(response.status_code, 200)
        usuario = User.objects.get(username='usuario')
        self.assertEquals(usuario.email, 'email@email.com')
        self.assertRedirects(response, '/usuario/listar/')
        self.assertTemplateUsed(response, 'usuario/listarUsuario.html')
        #self.assertContains(response, '')
        # print( t.name for t in response.templates )

        self.client.logout()

    def test_listar(self):
        """Verifica que el status_code y el template sean los correctos"""
        User.objects.create_user(username='usuario1', password='contraseña').save()
        User.objects.create_user(username='usuario2',password='contraseña').save()

        self.client.login(username='test', password='test')
        response = self.client.get(reverse('listarusuario'), follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'usuario/listarUsuario.html')
        self.client.logout()

    def test_modificarusuario(self):
        """test de modificacion de usuario que utiliza un superuser para poder acceder,
            debido a que no se manejan permisos en este test
        """

        User.objects.create_user(password='contraseña', username='usuario').save()
        self.client.login(username='test', password='test')
        usuario = User.objects.get(username='usuario')

        response = self.client.get(reverse('modificar_usuario', args=(usuario.id, )), follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'usuario/editarusuario.html')

        self.client.logout()

