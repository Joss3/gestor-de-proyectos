from django.db import models
from proyecto.models import Proyecto


class ProductBacklog(models.Model):
    """
    El modelo del Product Backlog que tiene cada proyecto en donde se agrupan todos los User Stories de ese proyecto.
    """
    proyecto = models.ForeignKey(Proyecto, on_delete=models.CASCADE, null=True, blank=True)
    """El unico campo que tiene es el proyecto a cual pertenece este Product Backlog"""
