from django.test import TestCase
from django.contrib.auth.models import User
from usuarios.models import Usuario
from proyecto.models import Proyecto
from product_backlog.models import ProductBacklog


class ProductBacklogTest(TestCase):
    def setUp(self):
        user = User.objects.create(username='pedro', email='pedro@test.com')
        usuarios = Usuario.objects.all().exclude(user__username='AnonymousUser').exclude(user=user)
        proy = Proyecto.objects.create(nombre='Proyecto 1', fecha_inicio='2018-01-01', estado='P',
                                       scrum=Usuario.objects.get(user=user))
        proy.usuarios.set(usuarios)
        proy.save()

    def test_creacion_product_backlog(self):
        proy = Proyecto.objects.get(nombre='Proyecto 1')
        pb = ProductBacklog.objects.create(proyecto_id=proy.id)
        self.assertEqual(pb, proy.productbacklog_set.all()[0])
