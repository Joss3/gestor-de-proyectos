from django.shortcuts import render
from flujo.models import Flujo
from user_story.models import UserStory
from sprint.models import Sprint
from tipo_us.models import Tipo_US
from sprint_backlog.models import SprintBacklog
from django.shortcuts import redirect
from user_story.forms import AsignarUsuarioForm
from user_story.forms import ReasignarTrabajoForm
from proyecto.models import Proyecto


def cargar_us(request, sprint_id, us_id):
    """
    Asigna un US al sprint backlog de un sprint específico recibido como parámetro
    :type sprint_id: int
    :param sprint_id: id del sprint al cual se importará el US

    :type us_id: int
    :param us_id: id del US que se importará al sprint
    :return: página principal del sprint en caso de éxito, página de listado de sprints en caso de error
    """
    if request.session.is_empty() or request.user.is_superuser:
        return render(request, 'proyecto/error_inicio_session.html', {})
    else:
        us = UserStory.objects.get(pk=us_id)
        sprint = Sprint.objects.get(pk=sprint_id)
        sprint_bl = SprintBacklog.objects.get(sprint_id=sprint_id)
        us.set_sprint_backlog(sprint_bl)
        sprint.add_trabajo(us.trabajo_por_realizar)

        miembros = sprint.miembros.count()

        if us.trabajo_por_realizar < 0:
            return redirect('reasignar_horas_a_trabajar', sprint_id, us_id)
        if miembros == 1:
            return redirect('ver_sprint', sprint_id)
        else:
            return redirect('listar_us_usuario', sprint_id, us_id)


def quitar_us(request, sprint_id, us_id):
    """
        Remueve un US del sprint backlog de un sprint específico recibido como parámetro
        :type sprint_id: int
        :param sprint_id: id del sprint

        :type us_id: int
        :param us_id: id del US que se desasignará del sprint
        :return: página principal del sprint en caso de éxito, página de listado de sprints en caso de error
        """
    if request.session.is_empty() or request.user.is_superuser:
        return render(request, 'proyecto/error_inicio_session.html', {})
    else:
        us = UserStory.objects.get(pk=us_id)
        sprint = Sprint.objects.get(pk=sprint_id)
        us.reset_sprint_backlog()
        sprint.quitar_trabajo(us.trabajo_por_realizar)

        return redirect('ver_sprint', sprint_id)


def reasignar_horas_a_trabajar(request, sprint_id, us_id):

    us = UserStory.objects.get(id=us_id)
    sprint = Sprint.objects.get(pk=sprint_id)
    miembros = sprint.miembros.count()
    if request.method == 'POST':
        form = ReasignarTrabajoForm(request.POST, instance=us)
        if form.is_valid():
            form.save()
            if miembros == 1:
                return redirect('ver_sprint', sprint_id)
            else:
                return redirect('listar_us_usuario', sprint_id, us_id)
    form = ReasignarTrabajoForm(instance=us)
    return render(request, 'user_story/reasignar_horas_us.html', {'form': form, 'us': us})
