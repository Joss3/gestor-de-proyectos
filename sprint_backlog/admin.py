from django.contrib import admin
from sprint_backlog.models import SprintBacklog

admin.site.register([SprintBacklog])
