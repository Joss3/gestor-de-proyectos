from django.db import models
from sprint.models import Sprint


class SprintBacklog(models.Model):
    """
    El modelo del Sprint Backlog que tiene cada sprint en donde se agrupan todos los User Stories de ese sprint.
    """
    sprint = models.ForeignKey(Sprint, on_delete=models.CASCADE, null=True, blank=True)
    """El unico campo que tiene es el sprint a cual pertenece este Sprint Backlog"""
