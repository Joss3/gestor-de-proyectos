from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import render
from django.utils.decorators import method_decorator

from .forms import *
from .models import *
from django.views.generic import CreateView,UpdateView, ListView,DeleteView, DetailView
from django.shortcuts import reverse, get_object_or_404
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect
from flujo.models import *
from user_story.models import *
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
# Create your views here.


@method_decorator(login_required, name='dispatch')
class CrearTUSView(SuccessMessageMixin, CreateView):
    """
    Vista de creación de tipo de user story, contiene un enlace para agregar campos personalizados
    guardar y cancelar, solo el scrum master del proyecto puede acceder a esta vista
    :parameter proyecto_id: indica el id del proyecto donde se crea el tipo de user story
    :return al presionar guardar, retornara la vista de crear flujos
    :return al presionar cancelar volvera a la vista anterior

    """
    model = Tipo_US
    template_name = "tipous/crear.html"
    form_class = Tipo_USForm
    success_message = " %(nombre)s se ha creado exitosamente"

    #def get_success_message(self, cleaned_data):
    def get_form_kwargs(self):
        """
        con este metodo podemos pasarle parametros como kwargs al formulario
        :return kwargs con el valor añadido
        """
        kwargs = super(CrearTUSView, self).get_form_kwargs()

        # La variable que queremos pasar al formulario
        kwargs.update({'proyecto_id': self.kwargs.get('proyecto_id')})

        return kwargs

    def get_success_url(self):
        """
        Sirve para indicar la ruta de redireccion que tomara la vista
        :return reverse('crear_flujo', kwargs={'proyecto_id': self.kwargs.get('proyecto_id'), 'pk': self.object.id})
        """
        return reverse('crear_flujo', kwargs={'proyecto_id': self.kwargs.get('proyecto_id'), 'pk': self.object.id})

    def get_context_data(self, **kwargs):
        """
        Cargamos en el contexto los argumentos recividos para poder usarlos en la plantilla
        :param kwargs: argumentos del contexto
        :return context
        """
        # Llamamos ala implementacion primero del  context
        context = super(CrearTUSView, self).get_context_data(**kwargs)
        # Agregamos el id a la plantilla
        context['proyecto_id'] = self.kwargs.get('proyecto_id')
        return context

    def form_valid(self, form):
        """
        con form_valid poder hacer ciertas operaciones con los datos del formulario antes de guardarlo
        :param form: formulario con los datos POSTeados y validos
        :return super().form_valid(form) guarda los datos del formulario y redirecciona al valor especificado
        con get_success_url()
        """
        tipo_nuevo = form.save()
        proyecto = Proyecto.objects.get(pk = self.kwargs.get('proyecto_id'))
        tipo_nuevo.proyecto = proyecto
        #cargamos el tipo en tipo_campo
        campos_por_tipo = Tipo_Campo.objects.filter(tipo__isnull = True)
        for campo in campos_por_tipo:
            if campo.campo.proyecto_or == proyecto.id:
                campo.tipo = tipo_nuevo
                campo.save()

        return super().form_valid(form)

# este no está en Modificar


@method_decorator(login_required, name='dispatch')
class ModificarTUSView(UpdateView):

    model = Tipo_US
    template_name = "tipous/editar.html"
    form_class = Tipo_USForm

    # success_url = '/'


    def form_valid(self, form, **kwargs):
        # return HttpResponseRedirect('/proyecto/'+ self.proyecto_id)
        return HttpResponseRedirect(reverse('ver_proyecto', kwargs={'proyecto_id': self.kwargs.get('proyecto_id')}))

    def get_context_data(self, **kwargs):
        # Llamamos ala implementacion primero del  context
        context = super(ModificarTUSView, self).get_context_data(**kwargs)
        # Agregamos el id a la plantilla
        context['proyecto_id'] = self.kwargs.get('proyecto_id')
        context['pk'] = self.kwargs.get('pk')
        return context


@login_required()
def Modificar(request,proyecto_id, pk):
    """
    vista de modificacion de tipo de user story
    esta vista no se utiliza ya que los tipos no se pueden modificar una vez creados
    :param request: datos de la vista
    :param proyecto_id: id del proyecto al cual pertenece el tipo
    :param pk: id del tipo a modificar
    """
    tipo_us = Tipo_US.objects.get(pk=pk)
    if request.method == 'POST':

        form = Tipo_USForm(request.POST, instance = tipo_us)

        if form.is_valid():
            # lista de tcv del tipo de us actual
            lista_tcv = Tipo_Campo.objects.filter(tipo = tipo_us)

            #query = CampoPersonalizado.objects.filter(tipo_us=tipo_us)
            #print(lista_tcv)

            form.save()
            return HttpResponseRedirect(reverse('tipoushome', kwargs={'proyecto_id': proyecto_id } ))
    else:
        form = Tipo_USForm(instance=tipo_us)
    return render(request, "tipous/editar.html", {'form': form, 'proyecto_id':proyecto_id, 'pk':pk })


@method_decorator(login_required, name='dispatch')
class CrearCampoPersonalizadoView(CreateView):
    """
    Vista de creación de nuevo campo personalizado, esta vista se llama desde la vista de creación de tipo
    permite crear varios campos personalizados con sus tipos correspondientes
    al presionar cancelar vuelve a la vista de creación de tipo de user story
    """
    model = CampoPersonalizado
    template_name = "tipous/crear_campo.html"
    form_class = CampoPersonalizadoForm

    def get_success_url(self):
        """
        url de redirección cuando los datos son válidos
        :return /proyecto/proyecto_id/tipo-us/crear/crear_campo/
        """
        return reverse('crearcampopersonalizado', kwargs={'proyecto_id': self.kwargs.get('proyecto_id')})

    def get_context_data(self, **kwargs):
        """
        En este metodo se añaden nuevos datos al contexto de la plantilla
        se le añade el id del proyecto
        :param kwargs: datos pasados por la url ej:proyecto_id
        :return context el nuevo contexto con los nuevos datos.
        """
        # Llamamos ala implementacion primero del  context
        context = super(CrearCampoPersonalizadoView, self).get_context_data(**kwargs)
        # Agregamos el id a la plantilla
        context['proyecto_id'] = self.kwargs.get('proyecto_id')
        #context['pk'] = self.kwargs.get('pk')
        return context

    def form_valid(self, form):
        """
        Con este metodo se añade el proyecto al campo creado y se carga la tabla tipo_campo
        :param form: formulario con los datos POSTeados y validos
        :return super().form_valid(form) guarda y redirecciona a la url especificada en get_success_url()
        """
        nombre_campo = form.cleaned_data.get('nombre_campo')

        # print(nombre_campo)
        #tipopk = self.kwargs.get('pk')

        #tipo_objeto = Tipo_US.objects.get(pk = tipopk)
        newcampo = form.save()
        newcampo.proyecto_or = self.kwargs.get('proyecto_id')
        tcv = Tipo_Campo(campo=newcampo)
        tcv.save()
        return super().form_valid(form)


@method_decorator(login_required, name='dispatch')
class ListaTUSView(ListView):

    """
    Esta vista sirve para lista los tipos de user story
    """
    model = Tipo_US
    context_object_name = 'tipos_campos'
    template_name = "tipous/tipo_us_list.html"

    def get_success_url(self):
        """
        url de redirección cuando los datos son válidos
        :return /proyecto/proyecto_id/
        """
        return reverse('ver_proyecto', kwargs={'proyecto_id': self.kwargs.get('proyecto_id')})

    def get_queryset(self):
        """
        sirve para elegir los datos que se le pasaran a la plantilla como queryset
        se filtra por proyecto_id
        :return queryset
        """
        return Tipo_US.objects.filter(proyecto_id = self.kwargs.get('proyecto_id'))

    def get_context_data(self, **kwargs):
        """
        En este metodo se añaden nuevos datos al contexto de la plantilla
        se le añade el id del proyecto
        :param kwargs: datos pasados por la url ej:proyecto_id
        :return context el nuevo contexto con los nuevos datos.
        """
        # Llamamos ala implementacion primero del  context
        context = super(ListaTUSView, self).get_context_data(**kwargs)
        # Agregamos el id a la plantilla
        context['proyecto_id'] = self.kwargs.get('proyecto_id')
        context['importar'] = False
        us=UserStory.objects.all()
        context['us']=us
        return context


@method_decorator(login_required, name='dispatch')
class DetalleTUSView(DetailView):
    """
    sirve para obtener los datos del tipo que se envia como solicitud
    """
    model = Tipo_US
    template_name = "tipous/detalle.html"

    def get_context_data(self, **kwargs):
        """
        se añaden los datos que se pasaran como contexto a la plantilla
        proyecto_id, pk:id del tipo a mostrar, tipo: objeto tipo
        :param kwargs: [], argumentos pasados al metodo o la clase
        :return: context contexto con los datos añadidos
        """
        # Llamamos ala implementacion primero del  context
        context = super(DetalleTUSView, self).get_context_data(**kwargs)
        #tipo = Tipo_US.objects.get(pk = self.kwargs.get('pk'))
        # Agregamos el id a la plantilla
        context['proyecto_id'] = self.kwargs.get('proyecto_id')
        context['pk'] = self.kwargs.get('pk')
        # context['tipo'] = tipo

        return context


@login_required()
def ImportarTUSView(request, proyecto_id, pk):
    """
    Vista que permite modificar los datos del tipo de user story a importar,
    permite seleccionar los flujos que se van a importar como así tambien los
    campos personalizados.
    :param request: datos de la vista de la solicitud
    :param proyecto_id: id del proyecto a donde se importara el tipo de user story
    :param pk: id del tipo de user story que se quiere importar
    :return redirecciona a la lista de tipos de user story que se pueden importar
    """

    tipo = Tipo_US.objects.get(pk = pk)
    tipo2 = tipo
    if request.method == 'POST':

        form = Importar_TipoUSForm(request.POST, instance=tipo)

        if form.is_valid():
            try:
                # Aqui se clona el tipo de user story
                if clonable(tipo, proyecto_id):#clonable verifica que no tenga ya el tipo
                    tipo.pk = None
                    nombre_nuevo = form.cleaned_data.get('nombre')

                    if nombre_nuevo == tipo.nombre:
                        #print(nombre_nuevo)
                        tipo.nombre = nombre_nuevo + f'({proyecto_id})'
                        #print(tipo.nombre)
                    else:
                        tipo.nombre = nombre_nuevo

                    tipo.proyecto = Proyecto.objects.get(pk=proyecto_id)
                    tipo.save()

                    #se copian los flujos
                    flujos_por_tipous = form.cleaned_data.get('flujos')

                    for flujo in flujos_por_tipous:
                        #print(flujo)
                        nuevo_flujo = Flujo(nombre=flujo.nombre + f'({proyecto_id})', tipo= tipo2)
                        nuevo_flujo.save()
                        fases_por_flujo = Fase.objects.filter(id_flujo=flujo.id)
                        #print(fases_por_flujo)
                        #se copian las fases
                        for fase in fases_por_flujo:
                            fase_nuevo = Fase(nombre=fase.nombre + f'({proyecto_id})',
                                              id_flujo=nuevo_flujo.id, indice=fase.indice)
                            #print(fase)
                            fase_nuevo.save()

                    # clonamos las filas de la tabla tipo_campo_valor
                    campos_por_tipous = form.cleaned_data.get('campos')
                    #print(campos_por_tipous)
                    for tcv in campos_por_tipous:

                        tcv.pk = None
                        tcv.tipo = tipo2
                        tcv.save()

                    messages.add_message(request, messages.SUCCESS, 'Se ha importado exitosamente')
                else:
                    messages.add_message(request, messages.ERROR,
                                         'Ya existe este tipo en el proyecto')
            except:
                messages.add_message(request, messages.ERROR,
                                     'Ya existe este tipo en la base de datos o ya tiene el campo')
                print('ya existe este tipo en la base de datos o ya tiene el campo')

            return HttpResponseRedirect(reverse_lazy('listaimportar', kwargs={'proyecto_id': proyecto_id}))
    else:

        form = Importar_TipoUSForm(instance=tipo)

    return render(request, "tipous/importar.html", {'form': form, 'proyecto_id': proyecto_id, 'tipo': tipo2})


def clonable(tipo, proyecto_id):
    """
    Verifica que el tipo de user story no sea uno ya existente
    :param tipo: el tipo que se quiere importar
    :param proyecto_id: el id del proyecto al cual se quiere importar
    :return True si no está existe, False si ya existe
    """
    tipos = Tipo_US.objects.filter(proyecto_id=proyecto_id)
    for t in tipos:
        if t.nombre in tipo.nombre:
            return False
    return True


@method_decorator(login_required, name='dispatch')
class ListaTUSImportarView(ListView):
    """
    vista que muestra todos los tipos de user story de todos los proyecto que se pueden importar
    por cada user story hay un enlace para importar dicho tipo, esta vista redireccionara a la vista
    de importación pasa realizar cambios y guardar los cambios.
    """

    model = Tipo_US
    context_object_name = 'tipos_campos'
    template_name = "tipous/tipo_us_list.html"

    def get_success_url(self):
        """
            url de redirección cuando los datos son válidos
           :return /proyecto/proyecto_id/
        """
        return reverse('ver_proyecto', kwargs={'proyecto_id': self.kwargs.get('proyecto_id')})

    def get_context_data(self, **kwargs):
        """
        se añaden datos al contexto
        :param kwargs: [], argumentos pasados
        :return context
        """
        # Llamamos ala implementacion primero del  context
        context = super(ListaTUSImportarView, self).get_context_data(**kwargs)
        # Agregamos el id a la plantilla
        context['proyecto_id'] = self.kwargs.get('proyecto_id')

        return context
