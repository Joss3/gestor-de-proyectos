from django.test import TestCase
from django.urls import reverse, reverse_lazy
import unittest
# Create your tests here.
from django.contrib.auth.models import User
from proyecto.models import Proyecto
from usuarios.models import Usuario
from user_story.models import UserStory
from tipo_us.models import Tipo_US, CampoPersonalizado,Tipo_Campo
from flujo.models import Flujo,Fase


class TipoUSTest(TestCase):
    """Prueba unitaria de usuarios en, en el se prueban creación, listado y modificacion"""

    def setUp(self):
        user = User.objects.create(username='fran', email='fran@test.com')
        user.set_password('fran')
        user.save()
        User.objects.create(username='pablo', email='pablo@test.com')
        User.objects.create(username='juan', email='juan@test.com')
        usuarios = Usuario.objects.all().exclude(user__username='AnonymousUser').exclude(user=user)
        proy = Proyecto.objects.create(nombre='Proyecto 1', fecha_inicio='2018-01-01', estado='P',
                                       scrum=Usuario.objects.get(user=user))
        proy.usuarios.set(usuarios)
        proy.save()
        encargado = Usuario.objects.get(user=user)

        Tipo_US.objects.create(nombre='tipo', proyecto=proy).save()
        Tipo_US.objects.create(nombre='tipo1', proyecto=proy).save()
        Tipo_US.objects.create(nombre='tipo2', proyecto=proy).save()

        #proyecto2
        proy2 = Proyecto.objects.create(nombre='Proyecto 2', fecha_inicio='2018-01-01', estado='P',
                                        scrum=Usuario.objects.get(user=user))
        proy2.usuarios.set(usuarios)
        proy2.save()

        tip = Tipo_US.objects.create(nombre='tipo3', proyecto=proy2)
        tip.save()

        campo = CampoPersonalizado.objects.create(nombre_campo='campo1', tipo_dato='TEXT', proyecto_or=proy2.id)
        campo.save()

        fl = Flujo.objects.create(nombre='flujo1', tipo=tip)
        fl.save()
        tc = Tipo_Campo.objects.create(tipo=tip, campo=campo)

        Fase.objects.create(nombre='fase1', id_flujo=fl).save()


        #Proyecto.objects.create(nombre='proyecto',)

    def test_CrearTUSView(self):

        self.assertTrue(self.client.login(username='fran', password='fran'))

        proy = Proyecto.objects.get(nombre='Proyecto 1')

        #self.assertEqual(proy.id, 1)

        response = self.client.get(reverse_lazy('creartipous', kwargs={'proyecto_id':proy.id}),
                                    follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'tipous/crear.html')

        response = self.client.post(reverse_lazy('creartipous', kwargs={'proyecto_id':proy.id}),{'nombre':'test'},
                                    follow=True)

        self.assertEqual(response.status_code, 200)
        #self.assertTemplateUsed(response, 'flujo/crear_flujo.html')
        tipo = Tipo_US.objects.get(nombre='test')
        self.assertEquals(tipo.nombre, 'test')
        self.assertRedirects(response, reverse('crear_flujo', kwargs={'proyecto_id': proy.id, 'pk': tipo.id}))

        # self.assertContains(response, '')
        #for t in response.templates:
           # print(t.name )

        self.client.logout()

    def test_crear_Campos_personalizados(self):

        self.assertTrue(self.client.login(username='fran', password='fran'))

        proy = Proyecto.objects.get(nombre='Proyecto 1')
        tipo = Tipo_US.objects.get(nombre='tipo')

        response = self.client.get(reverse_lazy('crearcampopersonalizado',kwargs={'proyecto_id': proy.id}),follow=True)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'tipous/crear_campo.html')

        response = self.client.post(reverse_lazy('crearcampopersonalizado',kwargs={'proyecto_id': proy.id}),
                                    {'nombre_campo': 'campo','tipo_dato':'TEXT'},follow=True)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'tipous/crear_campo.html')
        campo = CampoPersonalizado.objects.get(nombre_campo='campo')
        self.assertEquals(campo.nombre_campo, 'campo')
        self.assertRedirects(response, reverse('crearcampopersonalizado', kwargs={'proyecto_id': proy.id}))

        self.client.logout()

    def test_listartipous(self):
        self.assertTrue(self.client.login(username='fran', password='fran'))

        proy = Proyecto.objects.get(nombre='Proyecto 1')

        response = self.client.get(reverse('tipoushome', kwargs={'proyecto_id':proy.id}), follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'tipous/tipo_us_list.html')
        self.client.logout()

    def test_importar_tipous(self):

        self.assertTrue(self.client.login(username='fran', password='fran'))

        proy1 = Proyecto.objects.get(nombre='Proyecto 1')
        proy2 = Proyecto.objects.get(nombre='Proyecto 2')

        tipos = Tipo_US.objects.filter(proyecto=proy2)
        tipop = tipos[0]
        #print(tipop)
        flujos = Flujo.objects.filter(tipo=tipop)
        #print(flujos)
        campo = CampoPersonalizado.objects.get(nombre_campo='campo1')
        cpt = Tipo_Campo.objects.get(tipo=tipop, campo=campo)

       # print(cpt)

        response = self.client.get(reverse('importartipous', kwargs={'proyecto_id': proy1.id, 'pk': tipop.id}), follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'tipous/importar.html')

        response2 = self.client.post(reverse('importartipous', kwargs={'proyecto_id': proy1.id, 'pk': tipop.id}),
                                    {'nombre':'tipo3','flujos':(flujos[0]),'campos':(cpt,)}, follow=True)

        self.assertEqual(response.status_code, 200)


        #print(response2.redirect_chain)
        self.assertTemplateUsed(response2, 'tipous/importar.html')



        self.client.logout()

    """def test_modificarusuario(self):
        ""test de modificacion de usuario que utiliza un superuser para poder acceder,
            debido a que no se manejan permisos en este test
        ""

        User.objects.create_user(password='contraseña', username='usuario').save()
        self.client.login(username='test', password='test')
        usuario = User.objects.get(username='usuario')

        response = self.client.get(reverse('modificar_usuario', args=(usuario.id,)), follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'usuario/editarusuario.html')

        self.client.logout()


    """