from django.db import models
from proyecto.models import Proyecto
from sprint.models import Sprint


class CampoPersonalizado(models.Model):
    """
    CampoPersonalizado es el modelo que almacena los campos personalizados de los tipos de user story
    """
    nombre_campo = models.CharField(max_length=100)
    """Indica el nombre que va a tener el field o campo"""
    tipo_dato = models.CharField(max_length=100, default='TEXT')
    """Indica el tipo de dato que tendra el campo por defecto es texto(TEXT) o CharField"""
    proyecto_or = models.IntegerField(null=True)
    """Indica el proyecto en el que fue creado"""


class Tipo_US(models.Model):

    nombre = models.CharField(max_length=100, unique=True)
    """Indica el nombre del tipo de user story"""
    proyecto = models.ForeignKey(Proyecto, on_delete=models.CASCADE, null=True)
    """Proyecto al cual pertenece el tipo de User Story"""
    # sprint = models.ForeignKey(Sprint, on_delete=models.CASCADE, null=True, blank=True, default=None)

    def __str__(self):
        return f'{self.nombre}'

    #def set_sprint(self, sprint_id):
     #   self.sprint = Sprint.objects.get(pk=sprint_id)
      #  self.save()
       # return


class Tipo_Campo(models.Model):
    """
    Este modelo contiene al tipo y al campo personalizado de dicho tipo lo cual representa una relación de muchos a muchos de tipos con campos.
    """
    tipo = models.ForeignKey(Tipo_US, on_delete= models.CASCADE, null=True)
    """Clave foranea del tipo de user story"""
    campo = models.ForeignKey(CampoPersonalizado, on_delete=models.CASCADE)
    """Clave foranea del campo personalizado del tipo"""

    def __str__(self):
        return f'{self.campo.nombre_campo}'


class US_Tipo_Campo_Valor(models.Model):
    """
    Este modelo sirve para poder relacionar el user story con el tipo y los campos con sus respectivos valores
    """
    us = models.ForeignKey('user_story.UserStory', on_delete=models.CASCADE, null=True)
    """clave foranea del modelo  user_story"""
    tipo_campo = models.ForeignKey(Tipo_Campo, on_delete=models.CASCADE)
    """clave foranea del modelo Tipo_Campo para obtener los campos del tipo de user story"""
    valor = models.CharField(max_length=100)
    """indica el valor para un determinado campo en el us"""

