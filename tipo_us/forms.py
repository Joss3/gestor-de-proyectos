from django import forms
from .models import *
from flujo.models import Flujo


class Tipo_USForm(forms.ModelForm):
    """
    Formulario de creación de tipo de user story
    """
    class Meta:
        model = Tipo_US
        fields = '__all__'
        exclude = ('proyecto', 'sprint')

    def __init__(self, *args, **kwargs):

        proyecto_id = kwargs.pop('proyecto_id')
        """
        constructor del formulario, en el se construyen los campos adicionales dinamicamente obteniendo un
        queryset de CampoPersonalizado
        :param args: argumentos pasados al constructor
        :param kwargs: se le pasan datos tipo clave : valor como un diccionario, en el recibe la instancia
        """
        super(Tipo_USForm, self).__init__(*args, **kwargs)

        campos_por_tipo = Tipo_Campo.objects.filter(tipo__isnull = True)

        if campos_por_tipo != None:
            for tcv in campos_por_tipo:
                # genera los campos extras
                campo = tcv.campo #trae el campo de la lista de campos por tipo de user story
                if campo.proyecto_or == proyecto_id:
                    index = '%s' % campo.nombre_campo
                    #valor = campo.imput_campo

                    if(campo.tipo_dato == 'TEXT'):
                        self.fields[index] = forms.CharField(required= False)
                        self.fields[index].widget.attrs['readonly'] = 'readonly'
                    elif(campo.tipo_dato == 'INT'):
                        self.fields[index] = forms.IntegerField(required= False)
                        self.fields[index].widget.attrs['readonly'] = 'readonly'
                    elif(campo.tipo_dato == 'BOOL'):
                        self.fields[index] = forms.BooleanField(required= False)
                        self.fields[index].widget.attrs['readonly'] = 'readonly'

        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'


class CampoPersonalizadoForm(forms.ModelForm):
    """
    Formulario del modelo CampoPersonalizado donde se requiere el nombre del nuevo campo
    para este formulario se especifican tres tipos de datos como choice_set TEXT,INT y BOOL

    modelo : CampoPersonalizado
    fiedls :  nombre_campo, tipo_dato
    """

    class Meta:
        TIPO_DATO = {
            'INT': 'IntegerField',
            'TEXT': 'CharField',
        }
        TIPO_CHOICES = (
            ('TEXT', 'TEXT'),
            ('INT', 'INT'),
            ('BOOL', 'BOOL'),
        )
        model = CampoPersonalizado
        fields = ['nombre_campo','tipo_dato']

        widgets = {
            'nombre_campo' : forms.TextInput(attrs= {'class':'form-control'}, ),
            'tipo_dato': forms.Select(choices=TIPO_CHOICES,attrs={'class':'form-control'}),
        }


class Importar_TipoUSForm(forms.ModelForm):
    """
       Formulario de importación de tipo de user story
       modelo: Tipo_US
    """
    class Meta:
        model = Tipo_US
        fields = '__all__'
        exclude = ('proyecto','sprint')

    def __init__(self, *args, **kwargs):
        """
        constructor del formulario, en el se construyen los campos adicionales dinamicamente obteniendo un
        queryset de CampoPersonalizado
        :param args: argumentos pasados al constructor
        :param kwargs: se le pasan datos tipo clave : valor como un diccionario, en el recibe la instancia
        """
        super(Importar_TipoUSForm, self).__init__(*args, **kwargs)
        # self.fields['proyecto'].initial =

        flujos = Flujo.objects.filter(tipo= kwargs['instance']) # aqui se traen los flujos que corresponder al tipo
        self.fields['flujos'] = forms.ModelMultipleChoiceField(queryset=flujos,
                                                               widget= forms.CheckboxSelectMultiple())

        campos_por_tipo = Tipo_Campo.objects.filter(tipo=kwargs['instance'])
        self.fields['campos'] = forms.ModelMultipleChoiceField(queryset=campos_por_tipo,
                                                               widget = forms.CheckboxSelectMultiple(),
                                                               required= False)

        for field_name, field in self.fields.items():
            if field_name != 'campos' and field_name != 'flujos':
                field.widget.attrs['class'] = 'form-control'


