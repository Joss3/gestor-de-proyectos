from django.urls import path, include
from .views import *


urlpatterns = [
    path('',ListaTUSView.as_view(), name='tipoushome'),
    path('importar/',ListaTUSImportarView.as_view(), name='listaimportar'),
    path('crear/',CrearTUSView.as_view(), name='creartipous'),
    path('crear/crear_campo/', CrearCampoPersonalizadoView.as_view(), name='crearcampopersonalizado'),
    path('<int:pk>/', DetalleTUSView.as_view(), name='detalletipous'),
    path('<int:pk>/modificar/', Modificar, name='modificartipous'),
    path('importar/<int:pk>/', ImportarTUSView, name='importartipous'),
    #path('eliminar/<int:pk>',EliminarTUSView, name='eliminartipous'),
    #path('modificar/<int:pk>/eliminar_campo/',EliminarCampoPersonalizadoView.as_view(), name='eliminarcampopersonalizado'),
    path('<int:pk>/flujo/', include('flujo.urls'), name='redireccionarflujo')
]
