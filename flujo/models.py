from django.db import models
from tipo_us.models import Tipo_US


class Flujo(models.Model):

    nombre = models.CharField(max_length=100, unique=True)
    """Nombre del flujo"""
    tipo = models.ForeignKey(Tipo_US, on_delete=models.CASCADE, null=True, blank=False)
    """Una clave foranea del Tipo de User Story al cual el flujo pertenece"""
    cant_fases = models.IntegerField(default=0)
    """La cantidad de fases que tiene el flujo"""

    def __str__(self):
        return f' {self.nombre}'


class Fase(models.Model):

    nombre = models.CharField(max_length=100, unique=True)
    """El nombre de la fase"""
    id_flujo = models.ForeignKey('flujo.Flujo', related_name='flujo', on_delete=models.SET_NULL, null=True, blank=False)
    """Una clave foranea del flujo al cual pertenece esta fase"""
    indice = models.IntegerField(null=True, blank=True)
    """Como las fases son secuenciales y no se pueden pasar a la ultima fase porque si, este atributo
    sirve para indicar en que posicion de la secuencialidad de fases del flujo se encuentra"""

    def __str__(self):
        return f' {self.nombre}'

