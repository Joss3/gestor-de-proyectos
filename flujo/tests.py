from django.test import TestCase, Client
from user_story.models import UserStory
from django.contrib.auth.models import User
from usuarios.models import Usuario
from proyecto.models import Proyecto
from tipo_us.models import Tipo_US
from flujo.models import Flujo, Fase

class FlujoTestCase(TestCase):
    def setUp(self):
        user = User.objects.create(username='pedro', email='pedro@test.com')
        user.set_password('00000000')
        user.save()
        User.objects.create(username='pablo', email='pablo@test.com')
        User.objects.create(username='juan', email='juan@test.com')
        usuarios = Usuario.objects.all().exclude(user__username='AnonymousUser').exclude(user=user)
        proy = Proyecto.objects.create(nombre='Proyecto 1', fecha_inicio='2018-01-01', estado='P',
                                       scrum=Usuario.objects.get(user=user))
        proy.usuarios.set(usuarios)
        proy.save()
        encargado = Usuario.objects.get(user=user)
        UserStory.objects.create(nombre='US2', descripcion='PRUEBA',
                                 criterios_aceptacion='PRUEBA', trabajo_por_realizar=10,
                                 valor_negocio=10, valor_tecnico=10, usuario_asignado=encargado)
        tipo=Tipo_US.objects.create(nombre='Tipo_1', proyecto=proy)
        tipo.save()
        flujo=Flujo.objects.create(nombre='Flujo1', tipo=tipo)
        flujo.save()
        fase=Fase.objects.create(nombre='Fase1', id_flujo=flujo, indice=1)
        fase.save()
    def test_nombre_flujo(self):
        flu = Flujo.objects.get(nombre="Flujo1")
        self.assertEqual(flu.nombre, 'Flujo1')
    def test_nombre_fase(self):
        fa = Fase.objects.get(nombre="Fase1")
        self.assertEqual(fa.nombre, 'Fase1')