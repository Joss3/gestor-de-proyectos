from django.urls import path
from . import views

urlpatterns = [
    path('<int:flujo_id>/', views.ver_fase, name='ver_fase'),
    path('ver/', views.ver_flujo, name='ver_flujo'),
    path('crear/', views.Crear_Flujo, name='crear_flujo'),
    path('<int:pk>/modificar/', views.Modificar_Flujo, name='modificar_flujo'),
    path('<int:pk>/eliminar/', views.Eliminar_Flujo, name='eliminar_flujo'),
    # path('<int:pk>/modificar/', views.Modificar_Flujo3, name='modificar_flujo3'),
    #path('crear/fase/', views.Crear_Fase2, name='crear_fase2'),
    #path('crear/fase/', views.Fase3, name='fase3'),
]
