from django import forms
from flujo.models import Flujo, Fase
from django.forms import formset_factory, inlineformset_factory
from django.forms import modelformset_factory


class FlujoForm(forms.ModelForm):

    class Meta:
        """Se define el modelo a utilizar y el campo del nombre"""
        model = Flujo
        fields = ('nombre', 'tipo')
        widgets = {'tipo': forms.HiddenInput()}

    def __init__(self, *args, **kwargs):
        super(self.__class__, self).__init__(*args, **kwargs)
        # asi vuelves tus campos no requeridos
        self.fields['tipo'].required = False


class FaseForm(forms.ModelForm):

    class Meta:
        """Se define el modelo a utilizar, los campos del formulario y el nombre de cada uno"""
        """Los campos de estado e id_flujo estan ocultos porque se cargan despues de la validacion del formulario"""
        model = Fase
        fields = ('nombre', 'id_flujo', 'indice',)
        widgets = {'id_flujo': forms.HiddenInput()}
        widgets = {'indice': forms.HiddenInput()}
    id_flujo = forms.IntegerField(required=False)


class FaseForm2(forms.Form):
    nombre = forms.CharField(
        label='Nombre de la fase',
        widget=forms.TextInput(attrs={
            'class': 'form-control',
            'placeholder': 'Ingrese el nombre de la fase aqui'
        })
    )
    id_flujo= forms.IntegerField(required=False)
    indice = forms.IntegerField(required=False)

FaseFormset = formset_factory(FaseForm2, extra=1)
#FaseFormset = inlineformset_factory(Flujo, Fase, fields=('nombre', ))


class FlujoForm2(forms.Form):
    nombre = forms.CharField()
    fase = FaseFormset()

    class Meta:
        """Se define el modelo a utilizar y el campo del nombre"""
        fields = ('nombre', 'tipo')
        widgets = {'tipo': forms.HiddenInput()}


class FaseEditarForm2(forms.ModelForm):
    class Meta:
        model = Fase
        fields = ('nombre', 'id_flujo', 'indice')
        widgets = {'id_flujo': forms.HiddenInput(), 'indice': forms.HiddenInput()}

    nombre = forms.CharField(
        label='Nombre de la fase',
        widget=forms.TextInput(attrs={
            'class': 'form-control',
            'placeholder': 'Ingrese el nombre de la fase aqui'
        })
    )

    id_flujo= forms.IntegerField(required=False)
    indice = forms.IntegerField(required=False)


#FaseEditarFormset = modelformset_factory(Fase, form=FaseEditarForm2, extra=1)


FaseEditarFormset = inlineformset_factory(Flujo, Fase, fields=('nombre', ))


class FlujoEditarForm2(forms.ModelForm):
    nombre = forms.CharField()
    fase = FaseEditarFormset()

    class Meta:
        """Se define el modelo a utilizar y el campo del nombre"""
        model=Flujo
        fields = ('nombre', 'tipo')
        widgets = {'tipo': forms.HiddenInput()}

    def __init__(self, *args, **kwargs):
        # asi vuelves tus campos no requeridos
        self.fa = kwargs.pop('pk')
        print("id del flujo:", self.fa)
        super(self.__class__, self).__init__(*args, **kwargs)
        self.fields['tipo'].required = False
        #self.fase.queryset= Fase.objects.filter(id_flujo=self.fa)
        #ho = Fase.objects.filter(id_flujo=self.fa)
        #for t in ho:
            #self.fase=FaseEditarFormset(instance=t)


class FaseEditarForm3(forms.ModelForm):
    class Meta:
        model=Fase
        fields=('nombre', 'id_flujo', 'indice')
        widgets = {'id_flujo': forms.HiddenInput(), 'indice': forms.HiddenInput()}

    nombre = forms.CharField(
            label='Nombre de la fase',
             widget=forms.TextInput(attrs={
                 'class': 'form-control',
                 'placeholder': 'Ingrese el nombre de la fase aqui'
        })
        )

    id_flujo= forms.IntegerField(required=False)
    indice = forms.IntegerField(required=False)


class FlujoEditarForm3(forms.ModelForm):
   nombre= forms.CharField()
   fase= FaseEditarFormset()

   class Meta:
       """Se define el modelo a utilizar y el campo del nombre"""
       model=Flujo
       fields = ('nombre', 'tipo')
       widgets = {'tipo': forms.HiddenInput()}

   def __init__(self, *args, **kwargs):
       # asi vuelves tus campos no requeridos
       self.fa=kwargs.pop('pk')
      # print("id del flujo:", self.fa)
       super(self.__class__, self).__init__(*args, **kwargs)
       self.fields['tipo'].required = False
       ho= Fase.objects.filter(id_flujo=self.fa)
       for t in ho:
           self.fase=FaseEditarFormset(instance=t)