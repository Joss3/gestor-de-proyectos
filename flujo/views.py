from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from .models import Flujo, Fase
from django.shortcuts import redirect
from .forms import FlujoForm, FaseForm
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from tipo_us.models import Tipo_US
from sprint.models import Sprint
from .forms import FaseFormset, FlujoForm2, FaseForm2, FlujoEditarForm2, FaseEditarForm2, FaseEditarFormset, FlujoEditarForm3, FaseEditarForm3
from django.forms import modelformset_factory
from django.http import HttpResponseRedirect, HttpResponse
from django.db import IntegrityError
from django.http import JsonResponse


def Crear_Flujo(request, proyecto_id, pk):
    """
    Recibe una solicitud de la pagina de crear la fase , se asigna una referencia del formulario del flujo,
    luego se guarda el pk del flujo creado, cuando se finaliza la creacion se guardan los
    datos del flujo en la base de datos y luego redirecciona a la vista Crear_fase para crear las fases

    :param request:
    :param proyecto_id: El id del proyecto al cual se le quiere agregar este flujo que se esta creando
    :param pk: La clave primaria del Tipo de US al cual se quiere agregar este flujo
    :return:
    """
    if request.POST:
        request.session['tipo_id'] = pk
        request.session['pro'] = proyecto_id
        form = FlujoForm2(request.POST)
        form.fase_instances = FaseFormset(request.POST)
        if form.is_valid():
            flujo = Flujo()
            flujo.nombre= form.cleaned_data['nombre']
            tipo = Tipo_US.objects.get(pk=pk)
            flujo.tipo=tipo
            request.session['indice'] = 1
            nombre=flujo.nombre
            print(nombre)
            flujo.cant_fases=0
            flujo.save()
        if form.fase_instances.cleaned_data is not None:
            for item in form.fase_instances:
                indice = request.session['indice']
                fase = Fase()
                fase.nombre = item['nombre'].data
                fase.id_flujo = flujo
                fase.indice = indice
                request.session['indice'] = request.session['indice'] + 1
                flujo.cant_fases = fase.indice
                try:
                    fase.save()
                except IntegrityError as e:
                    creados = Fase.objects.filter(id_flujo=flujo.id)
                    for i in creados:
                        i.delete()
                    flujo.delete()
                    return HttpResponse("ERROR: Nombre de fases iguales")
            flujo.save()
            return HttpResponseRedirect(reverse('crear_flujo', kwargs={'proyecto_id':proyecto_id, 'pk': pk}))
    else:
        form = FlujoForm2()
        return render(request, 'flujo/new.html', {'form': form})


def Modificar_Flujo(request, pk):
    """
    Vista que permite la modificacion de un flujo mientras que este no sea utilizado por ningun US

    :param request:
    :param pk: El id del flujo que se quiere modificar
    :return:
    """
    creado = get_object_or_404(Flujo, pk=pk)
    fase_creada = Fase.objects.filter(id_flujo=pk)
    print("flujo: ", creado)
    if request.POST:
        request.session['tipo_id'] = pk
        form = FlujoEditarForm2(request.POST, instance=creado, pk=pk)
        #fa = FaseEditarForm2(request.POST, instance=fase_creada)
        if form.is_valid():
            request.session['indice'] = 1
            f = form.save()
            f.save()
            form.fase_instances = FaseEditarFormset(request.POST, instance=creado)
            if form.fase_instances.is_valid():
                for item in form.fase_instances:
                    indice = request.session['indice']
                    fase = item.save()
                    fase.nombre = item['nombre'].data
                    fase.id_flujo = creado
                    fase.indice = indice
                    request.session['indice'] = request.session['indice'] + 1
                fase.save()
            '''if fa.is_valid():
                indice = request.session['indice']
                fe=fa.save()
                fe.indice=indice
                request.session['indice'] = request.session['indice'] + 1
                fe.save()'''
            return redirect('index')
        else:
            print("no se valido bien")
            return redirect('index')
    else:
        form = FlujoEditarForm2(instance=creado, pk=pk)
        fass= FaseEditarFormset(instance=creado)
        return render(request, 'flujo/modificar_flujo.html', {'form': form, 'fass':fass})


def Eliminar_Flujo(request, pk):
    """
    Recibe como parametro el pk del flujo y luego elimina las fases asociadas al flujo
    y luego al flujo en si

    :param pk: La clave primaria del flujo el cual se quiere eliminar
    :return:
    """
    flujo = Flujo.objects.filter(id=pk)
    fases = Fase.objects.filter(id_flujo=pk)
    for f in fases:
        f.delete()
    flujo.delete()
    return HttpResponse("Se han eliminado")


def Modificar_Flujo3(request, pk):
    creado = get_object_or_404(Flujo, pk=pk)
    print("vista3")
    #print("flujo: ", creado)
    if request.POST:
        request.session['tipo_id'] = pk
        form = FlujoEditarForm3(request.POST, instance=creado, pk=pk)
        fase_creada=Fase.objects.filter(id_flujo=pk)
        # fa = FaseEditarForm2(request.POST, instance=fase_creada)
        if form.is_valid():
            request.session['indice'] = 1
            f = form.save(commit=False)
            nombre = f.pk
            fl = Flujo.objects.get(pk=nombre)
            f.save()
            return redirect('index')
        else:
            print("no se valido bien")
            return redirect('index')
    else:
        form = FlujoEditarForm3(instance=creado, pk=pk)
        que=Fase.objects.filter(id_flujo=pk)

        return render(request, 'flujo/modificar-flujo2.html', {'form': form, 'que':que})


def ver_flujo(request, pk):
    """
    En esta vista muestra los flujos creados, en el cual se obtiene todos los flujos creados hasta ahora

    :param pk: El id del flujo que se quiere ver a
    """
    flujos = Flujo.objects.all()
    page = request.GET.get('page', 1)
    paginator = Paginator(flujos, 5)
    try:
        flujos = paginator.page(page)
    except PageNotAnInteger:
        flujos = paginator.page(1)
    except EmptyPage:
        flujos = paginator.page(paginator.num_pages)
    return render(request, 'flujo/inicio.html',{"flujos": flujos})


def ver_fase(request, flujo_id):
    """
    Vista para visualizar todas las fases del flujo del cual se pasa su id como argumento

    :param request:
    :param flujo_id: El id del flujo del cual se quiere ver todas las fases
    :return:
    """
    if request.session.is_empty():
        return render(request, 'proyecto/error_inicio_session.html', {})
    else:
        proy = Fase.objects.filter(id_flujo=flujo_id)

        return render(request, 'flujo/ver_fase.html',
                      {'proy': proy})


def listar_flujo(request, sprint_id):
    """

    :param request:
    :param sprint_id:
    :return:
    """
    if request.session.is_empty():
        return render(request, 'proyecto/error_inicio_session.html', {})
    else:
        sprint = Sprint.objects.get(pk=sprint_id)

        flujo_list = []

        if sprint:

            flujos = Flujo.objects.all()

            for flujo in flujos:

                if flujo.tipo.proyecto_id == sprint.proyecto_id:
                    flujo_list.append(flujo)

        return render(request, 'flujo/importar.html', {'sprint': sprint, 'flujos': flujo_list})
