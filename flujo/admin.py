from django.contrib import admin
from .models import Flujo, Fase
# Register your models here.
admin.site.register(Flujo)
admin.site.register(Fase)
