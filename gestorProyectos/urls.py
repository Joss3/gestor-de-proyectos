from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('admin/doc/', include('django.contrib.admindocs.urls')),
    path('admin/', admin.site.urls, name = 'home'),
    path('proyecto/', include('proyecto.urls')),
    path('usuario/', include('usuarios.urls')),
    path('', include('login.urls')),
    path('rol/', include('roles_permisos.urls')),
    path('cliente/', include('cliente.urls')),
    path('sprint/', include('sprint.urls')),
    path('flujo/', include('flujo.urls')),
]
