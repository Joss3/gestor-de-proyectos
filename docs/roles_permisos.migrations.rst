roles\_permisos.migrations package
==================================

Submodules
----------

roles\_permisos.migrations.0001\_initial module
-----------------------------------------------

.. automodule:: roles_permisos.migrations.0001_initial
    :members:
    :undoc-members:
    :show-inheritance:

roles\_permisos.migrations.0002\_permiso\_rol module
----------------------------------------------------

.. automodule:: roles_permisos.migrations.0002_permiso_rol
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: roles_permisos.migrations
    :members:
    :undoc-members:
    :show-inheritance:
