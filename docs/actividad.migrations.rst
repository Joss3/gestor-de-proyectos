actividad.migrations package
============================

Submodules
----------

actividad.migrations.0001\_initial module
-----------------------------------------

.. automodule:: actividad.migrations.0001_initial
    :members:
    :undoc-members:
    :show-inheritance:

actividad.migrations.0002\_actividad module
-------------------------------------------

.. automodule:: actividad.migrations.0002_actividad
    :members:
    :undoc-members:
    :show-inheritance:

actividad.migrations.0003\_actividad\_user\_story module
--------------------------------------------------------

.. automodule:: actividad.migrations.0003_actividad_user_story
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: actividad.migrations
    :members:
    :undoc-members:
    :show-inheritance:
