cliente.migrations package
==========================

Submodules
----------

cliente.migrations.0001\_initial module
---------------------------------------

.. automodule:: cliente.migrations.0001_initial
    :members:
    :undoc-members:
    :show-inheritance:

cliente.migrations.0002\_cliente module
---------------------------------------

.. automodule:: cliente.migrations.0002_cliente
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: cliente.migrations
    :members:
    :undoc-members:
    :show-inheritance:
