sprint.migrations package
=========================

Submodules
----------

sprint.migrations.0001\_initial module
--------------------------------------

.. automodule:: sprint.migrations.0001_initial
    :members:
    :undoc-members:
    :show-inheritance:

sprint.migrations.0002\_sprint module
-------------------------------------

.. automodule:: sprint.migrations.0002_sprint
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: sprint.migrations
    :members:
    :undoc-members:
    :show-inheritance:
