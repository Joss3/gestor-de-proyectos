user\_story.migrations package
==============================

Submodules
----------

user\_story.migrations.0001\_initial module
-------------------------------------------

.. automodule:: user_story.migrations.0001_initial
    :members:
    :undoc-members:
    :show-inheritance:

user\_story.migrations.0002\_userstory module
---------------------------------------------

.. automodule:: user_story.migrations.0002_userstory
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: user_story.migrations
    :members:
    :undoc-members:
    :show-inheritance:
