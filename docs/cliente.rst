cliente package
===============

Submodules
----------

cliente.forms module
--------------------

.. automodule:: cliente.forms
    :members:
    :undoc-members:
    :show-inheritance:

cliente.models module
---------------------

.. automodule:: cliente.models
    :members:
    :undoc-members:
    :show-inheritance:

cliente.views module
--------------------

.. automodule:: cliente.views
    :members:
    :undoc-members:
    :show-inheritance:
