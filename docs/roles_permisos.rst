roles\_permisos package
=======================

Submodules
----------

roles\_permisos.forms module
----------------------------

.. automodule:: roles_permisos.forms
    :members:
    :undoc-members:
    :show-inheritance:

roles\_permisos.models module
-----------------------------

.. automodule:: roles_permisos.models
    :members:
    :undoc-members:
    :show-inheritance:

roles\_permisos.views module
----------------------------

.. automodule:: roles_permisos.views
    :members:
    :undoc-members:
    :show-inheritance:
