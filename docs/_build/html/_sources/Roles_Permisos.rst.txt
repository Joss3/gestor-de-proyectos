Roles\_Permisos package
=======================

Submodules
----------

Roles\_Permisos.admin module
----------------------------

.. automodule:: Roles_Permisos.admin
    :members:
    :undoc-members:
    :show-inheritance:

Roles\_Permisos.apps module
---------------------------

.. automodule:: Roles_Permisos.apps
    :members:
    :undoc-members:
    :show-inheritance:

Roles\_Permisos.forms module
----------------------------

.. automodule:: Roles_Permisos.forms
    :members:
    :undoc-members:
    :show-inheritance:

Roles\_Permisos.models module
-----------------------------

.. automodule:: Roles_Permisos.models
    :members:
    :undoc-members:
    :show-inheritance:

Roles\_Permisos.tests module
----------------------------

.. automodule:: Roles_Permisos.tests
    :members:
    :undoc-members:
    :show-inheritance:

Roles\_Permisos.urls module
---------------------------

.. automodule:: Roles_Permisos.urls
    :members:
    :undoc-members:
    :show-inheritance:

Roles\_Permisos.views module
----------------------------

.. automodule:: Roles_Permisos.views
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: Roles_Permisos
    :members:
    :undoc-members:
    :show-inheritance:
