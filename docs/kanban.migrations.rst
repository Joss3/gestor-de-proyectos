kanban.migrations package
=========================

Submodules
----------

kanban.migrations.0001\_initial module
--------------------------------------

.. automodule:: kanban.migrations.0001_initial
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: kanban.migrations
    :members:
    :undoc-members:
    :show-inheritance:
