proyecto package
================

Submodules
----------

proyecto.forms module
---------------------

.. automodule:: proyecto.forms
    :members:
    :undoc-members:
    :show-inheritance:

proyecto.models module
----------------------

.. automodule:: proyecto.models
    :members:
    :undoc-members:
    :show-inheritance:

proyecto.views module
---------------------

.. automodule:: proyecto.views
    :members:
    :undoc-members:
    :show-inheritance:
