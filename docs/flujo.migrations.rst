flujo.migrations package
========================

Submodules
----------

flujo.migrations.0001\_initial module
-------------------------------------

.. automodule:: flujo.migrations.0001_initial
    :members:
    :undoc-members:
    :show-inheritance:

flujo.migrations.0002\_fase\_flujo module
-----------------------------------------

.. automodule:: flujo.migrations.0002_fase_flujo
    :members:
    :undoc-members:
    :show-inheritance:

flujo.migrations.0003\_flujo\_tipo module
-----------------------------------------

.. automodule:: flujo.migrations.0003_flujo_tipo
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: flujo.migrations
    :members:
    :undoc-members:
    :show-inheritance:
