usuarios.migrations package
===========================

Submodules
----------

usuarios.migrations.0001\_initial module
----------------------------------------

.. automodule:: usuarios.migrations.0001_initial
    :members:
    :undoc-members:
    :show-inheritance:

usuarios.migrations.0002\_usuario module
----------------------------------------

.. automodule:: usuarios.migrations.0002_usuario
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: usuarios.migrations
    :members:
    :undoc-members:
    :show-inheritance:
