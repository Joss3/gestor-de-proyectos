kanban package
==============

Submodules
----------

kanban.models module
--------------------

.. automodule:: kanban.models
    :members:
    :undoc-members:
    :show-inheritance:

kanban.views module
-------------------

.. automodule:: kanban.views
    :members:
    :undoc-members:
    :show-inheritance:
