tipo\_us.migrations package
===========================

Submodules
----------

tipo\_us.migrations.0001\_initial module
----------------------------------------

.. automodule:: tipo_us.migrations.0001_initial
    :members:
    :undoc-members:
    :show-inheritance:

tipo\_us.migrations.0002\_campopersonalizado\_tipo\_campo\_tipo\_us\_us\_tipo\_campo\_valor module
--------------------------------------------------------------------------------------------------

.. automodule:: tipo_us.migrations.0002_campopersonalizado_tipo_campo_tipo_us_us_tipo_campo_valor
    :members:
    :undoc-members:
    :show-inheritance:

tipo\_us.migrations.0003\_auto\_20190511\_0744 module
-----------------------------------------------------

.. automodule:: tipo_us.migrations.0003_auto_20190511_0744
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: tipo_us.migrations
    :members:
    :undoc-members:
    :show-inheritance:
