actividad package
=================

Submodules
----------

actividad.forms module
----------------------

.. automodule:: actividad.forms
    :members:
    :undoc-members:
    :show-inheritance:

actividad.models module
-----------------------

.. automodule:: actividad.models
    :members:
    :undoc-members:
    :show-inheritance:

actividad.views module
----------------------

.. automodule:: actividad.views
    :members:
    :undoc-members:
    :show-inheritance:
