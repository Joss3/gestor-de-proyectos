gestorProyectos package
=======================

Submodules
----------

gestorProyectos.desarrollo\-settings module
-------------------------------------------

.. automodule:: gestorProyectos.desarrollo-settings
    :members:
    :undoc-members:
    :show-inheritance:

gestorProyectos.settings module
-------------------------------

.. automodule:: gestorProyectos.settings
    :members:
    :undoc-members:
    :show-inheritance:

gestorProyectos.urls module
---------------------------

.. automodule:: gestorProyectos.urls
    :members:
    :undoc-members:
    :show-inheritance:

gestorProyectos.wsgi module
---------------------------

.. automodule:: gestorProyectos.wsgi
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: gestorProyectos
    :members:
    :undoc-members:
    :show-inheritance:
