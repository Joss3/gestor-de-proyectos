sprint\_backlog.migrations package
==================================

Submodules
----------

sprint\_backlog.migrations.0001\_initial module
-----------------------------------------------

.. automodule:: sprint_backlog.migrations.0001_initial
    :members:
    :undoc-members:
    :show-inheritance:

sprint\_backlog.migrations.0002\_sprintbacklog module
-----------------------------------------------------

.. automodule:: sprint_backlog.migrations.0002_sprintbacklog
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: sprint_backlog.migrations
    :members:
    :undoc-members:
    :show-inheritance:
